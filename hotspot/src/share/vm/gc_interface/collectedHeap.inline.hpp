/*
 * Copyright (c) 2001, 2013, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 *
 */

#ifndef SHARE_VM_GC_INTERFACE_COLLECTEDHEAP_INLINE_HPP
#define SHARE_VM_GC_INTERFACE_COLLECTEDHEAP_INLINE_HPP

#include "gc_interface/allocTracer.hpp"
#include "gc_interface/collectedHeap.hpp"
#include "memory/threadLocalAllocBuffer.inline.hpp"
#include "memory/universe.hpp"
#include "oops/arrayOop.hpp"
#include "prims/jvmtiExport.hpp"
#include "runtime/sharedRuntime.hpp"
#include "runtime/thread.inline.hpp"
#include "services/lowMemoryDetector.hpp"
#include "utilities/copy.hpp"

// Inline allocation implementations.

void CollectedHeap::post_allocation_setup_common(KlassHandle klass,
                                                 HeapWord* obj) {
  post_allocation_setup_no_klass_install(klass, obj);
  post_allocation_install_obj_klass(klass, oop(obj));
}

void CollectedHeap::post_allocation_setup_no_klass_install(KlassHandle klass,
                                                           HeapWord* objPtr) {
  oop obj = (oop)objPtr;

  assert(obj != NULL, "NULL object pointer");
  if (UseBiasedLocking && (klass() != NULL)) {
    obj->set_mark(klass->prototype_header());
  } else {
    // May be bootstrapping
    obj->set_mark(markOopDesc::prototype());
    obj->init_ids(); //initialize
  }
}

void CollectedHeap::post_allocation_install_obj_klass(KlassHandle klass,
                                                   oop obj) {
  // These asserts are kind of complicated because of klassKlass
  // and the beginning of the world.
  assert(klass() != NULL || !Universe::is_fully_initialized(), "NULL klass");
  assert(klass() == NULL || klass()->is_klass(), "not a klass");
  assert(obj != NULL, "NULL object pointer");
  obj->set_klass(klass());
  assert(!Universe::is_fully_initialized() || obj->klass() != NULL,
         "missing klass");
}

// Support for jvmti and dtrace
inline void post_allocation_notify(KlassHandle klass, oop obj) {

  //if(VerboseDebug)   tty->print_cr("notify post allocation %p, %s", obj, klass()->signature_name()); 

  // support low memory notifications (no-op if not enabled)
  LowMemoryDetector::detect_low_memory_for_collected_pools();

  // support for JVMTI VMObjectAlloc event (no-op if not enabled)
  JvmtiExport::vm_object_alloc_event_collector(obj);

  if (DTraceAllocProbes) {
    // support for Dtrace object alloc event (no-op most of the time)
    if (klass() != NULL && klass()->name() != NULL) {
      SharedRuntime::dtrace_object_alloc(obj);
    }
  }
}

void CollectedHeap::post_allocation_setup_obj(KlassHandle klass,
                                              HeapWord* obj) {
  //if (VerboseDebug) tty->print_cr("post_allocation set up for obj=%p class %s", obj, klass()->external_name());
  post_allocation_setup_common(klass, obj);
  assert(Universe::is_bootstrapping() ||
         !((oop)obj)->is_array(), "must not be an array");
  // notify jvmti and dtrace
  post_allocation_notify(klass, (oop)obj);
}

void CollectedHeap::post_allocation_setup_array(KlassHandle klass,
                                                HeapWord* obj,
                                                int length) {
  // Set array length before setting the _klass field
  // in post_allocation_setup_common() because the klass field
  // indicates that the object is parsable by concurrent GC.
  //if (VerboseDebug) tty->print_cr("post_alloc setup array %p lenght=%d %s", obj,length, klass()->external_name());
  assert(length >= 0, "length should be non-negative");
  ((arrayOop)obj)->set_length(length);
  post_allocation_setup_common(klass, obj);
  assert(((oop)obj)->is_array(), "must be an array");
  // notify jvmti and dtrace (must be after length is set for dtrace)
  post_allocation_notify(klass, (oop)obj);
}

HeapWord* CollectedHeap::common_mem_allocate_noinit(KlassHandle klass, size_t size, TRAPS) {

  // Clear unhandled oops for memory allocation.  Memory allocation might
  // not take out a lock if from tlab, so clear here.
  CHECK_UNHANDLED_OOPS_ONLY(THREAD->clear_unhandled_oops();)

  if (HAS_PENDING_EXCEPTION) {
    NOT_PRODUCT(guarantee(false, "Should not allocate with exception pending"));
    return NULL;  // caller does a CHECK_0 too
  }

  HeapWord* result = NULL;
  if (UseTLAB) {
    result = allocate_from_tlab(klass, THREAD, size);
    if (result != NULL) {
      assert(!HAS_PENDING_EXCEPTION,
             "Unexpected exception, will result in uninitialized storage");
      return result;
    }
  }
  bool gc_overhead_limit_was_exceeded = false;
  result = Universe::heap()->mem_allocate(size,
                                          &gc_overhead_limit_was_exceeded);
  if (result != NULL) {
    NOT_PRODUCT(Universe::heap()->
      check_for_non_bad_heap_word_value(result, size));
    assert(!HAS_PENDING_EXCEPTION,
           "Unexpected exception, will result in uninitialized storage");
    THREAD->incr_allocated_bytes(size * HeapWordSize);

    AllocTracer::send_allocation_outside_tlab_event(klass, size * HeapWordSize);

    // ASRMM code begin, author: Lu Fang, 04/06/2016.
    PRINT_YAK_DEBUG(DebugYakAllocation, "Yak allocation in mem at %p, size is %d, type is %s",
        result, size, klass()->internal_name());
    // ASRMM cdoe end.

    return result;
  }


  if (!gc_overhead_limit_was_exceeded) {
    // -XX:+HeapDumpOnOutOfMemoryError and -XX:OnOutOfMemoryError support
    report_java_out_of_memory("Java heap space");

    if (JvmtiExport::should_post_resource_exhausted()) {
      JvmtiExport::post_resource_exhausted(
        JVMTI_RESOURCE_EXHAUSTED_OOM_ERROR | JVMTI_RESOURCE_EXHAUSTED_JAVA_HEAP,
        "Java heap space");
    }

    THROW_OOP_0(Universe::out_of_memory_error_java_heap());
  } else {
    // -XX:+HeapDumpOnOutOfMemoryError and -XX:OnOutOfMemoryError support
    report_java_out_of_memory("GC overhead limit exceeded");

    if (JvmtiExport::should_post_resource_exhausted()) {
      JvmtiExport::post_resource_exhausted(
        JVMTI_RESOURCE_EXHAUSTED_OOM_ERROR | JVMTI_RESOURCE_EXHAUSTED_JAVA_HEAP,
        "GC overhead limit exceeded");
    }

    THROW_OOP_0(Universe::out_of_memory_error_gc_overhead_limit());
  }
}

HeapWord* CollectedHeap::common_mem_allocate_init(KlassHandle klass, size_t size, TRAPS) {
  //if (VerboseDebug)tty->print_cr("common_init %s", klass()->external_name());
  HeapWord* obj = common_mem_allocate_noinit(klass, size, CHECK_NULL);
  init_obj(obj, size);
  return obj;
}

HeapWord* CollectedHeap::allocate_from_tlab(KlassHandle klass, Thread* thread, size_t size) {
  assert(UseTLAB, "should use UseTLAB");

  // Khanh for ASRMM
  //if (RegionAllocate) {
  //  if (thread->is_region_manager_active()) {
  //    tty->print_cr("=========> region manager is active");
  //  } else {
  //    tty->print_cr("=========> region manager is inactive");
  //  }
  //}
  if (RegionAllocate && thread->is_region_manager_active()) {
    bool bypass = false; // whether or not we bypass allocating a type of object

    //bypass = !klass()->allocate_in_region(); Filtering class for GraphChi - not used anymore due to the huge size of Remset (too few in region and too many in heap)

    if (!AllocInstanceMirrorObjectInRegion && klass()->oop_is_instanceMirror()) {
      bypass = true;
      if(DebugYak){
       	tty->print_cr("Instance Mirror Object - klass name = %s - Bypassing region allocation" , klass()->signature_name());
      }
    }
    
    if (!bypass && !AllocReferenceObjectInRegion && klass()->oop_is_instanceRef()) {
      bypass = true;
      if(DebugYak){
	      tty->print_cr("Ref Object - klass name = %s - Bypassing region allocation" , klass()->signature_name());
      }
    }
    
    if (!bypass && klass()->oop_is_instanceClassLoader()) {
      if(DebugYak){
        tty->print_cr("InstanceClassLoader name = %s - NOT Bypassing the allocation, still allocating in region" , klass()->signature_name());
      }
      assert (false, "Check this case - instance Class Loader");
    }
    // else 
    


    if (!bypass && size >= (size_t)LargeObjectThreshold){ 
      // if (bypass) bypass=false; // we still allocate typeArray
      if (!AllocLargeObjectInRegion) bypass =true; 
      if (DebugLargeObjectAllocationInRegion) {
        tty->print_cr("Big Obj Allocation of size %d >= PageSize/2  klass=%s - %s bypassing" , size,  klass()->signature_name(), bypass ? "" :"not");
      }
    }

    if (!bypass) {
      HeapWord* obj = thread->region_manager()->allocate(size);
      assert (obj != NULL, "obj not allowed to be returned NULL from region manager");
      if (VerboseDebugAllocation) tty->print_cr("region-allocate obj at %p, klass name: %s", obj, klass()->signature_name());
      PRINT_YAK_DEBUG(DebugYakAllocation, "Yak allocation in region at %p, size is %d, type is %s, thread is %d, region is %d",
          obj, size, klass()->internal_name(),
          thread->region_manager()->get_thread_id(), thread->region_manager()->get_current_region_id());
      // obj has been properly marked
      return obj;
    }
  
  }

  //  VerboseDebug =false;
  HeapWord* obj = thread->tlab().allocate(size);
  
  if (obj == NULL) {
    obj = allocate_from_tlab_slow(klass, thread, size);
  }

  if (obj != NULL) {
    ((oop)obj)->init_ids();
  }
  //  if(VerboseDebug && Universe::is_fully_initialized()) tty->print_cr("Class name = %s thread=%p obj=%p" , klass()->signature_name(),thread,obj);
  if (VerboseDebugAllocation && obj!=NULL && klass()!=NULL) tty->print_cr("heap-allocate obj at %p, klass name: %s", obj, klass()->signature_name());
  if (DebugYak) {
    if (obj != NULL) {
      PRINT_YAK_DEBUG(DebugYakAllocation, "Yak allocation in tlab at %p, size is %d, type is %s",
          obj, size, klass()->internal_name());
    } else {
      PRINT_YAK_DEBUG(DebugYakAllocation, "Yak allocation from tlab is failed, size is %d, type is %s",
          size, klass()->internal_name());
    }
  }
  return obj;
}

void CollectedHeap::init_obj(HeapWord* obj, size_t size) {
  //if (VerboseDebug)  tty->print_cr("start init_obj %p",obj);
  assert(obj != NULL, "cannot initialize NULL object");
  const size_t hs = oopDesc::header_size();
  assert(size >= hs, "unexpected object size");
  //if (VerboseDebug)  tty->print_cr("header size = %d", hs);
  ((oop)obj)->set_klass_gap(0);
  Copy::fill_to_aligned_words(obj + hs, size - hs);
  //  if (VerboseDebug )  tty->print_cr("done init %p", obj);
}

oop CollectedHeap::obj_allocate(KlassHandle klass, int size, TRAPS) {
  ResourceMark rm;
  if (VerboseDebugAllocation)  tty->print_cr("obj_allocate %d %s - thread=%p, id=%d, region_manager_active=%d", size, klass()->external_name(),THREAD,THREAD->get_id(),THREAD->is_region_manager_active());
  debug_only(check_for_valid_allocation_state());
  assert(!Universe::heap()->is_gc_active(), "Allocation during gc not allowed");
  assert(size >= 0, "int won't convert to size_t");
  HeapWord* obj = common_mem_allocate_init(klass, size, CHECK_NULL);
  post_allocation_setup_obj(klass, obj);
  NOT_PRODUCT(Universe::heap()->check_for_bad_heap_word_value(obj, size));
  if (VerboseDebugAllocation)  tty->print_cr("obj_allocated %p: %d %s - thread=%p, id=%d, region_manager_active=%d", obj, size, klass()->external_name(),THREAD,THREAD->get_id(),THREAD->is_region_manager_active());
  return (oop)obj;
}

oop CollectedHeap::array_allocate(KlassHandle klass,
                                  int size,
                                  int length,
                                  TRAPS) {
  ResourceMark rm;
  if (VerboseDebugAllocation)  tty->print_cr("array_allocate %d %s - thread=%p, id=%d, region_manager_active=%d", size, klass()->external_name(),THREAD,THREAD->get_id(),THREAD->is_region_manager_active());
  //if (VerboseDebug) tty->print_cr("array_allocate size=%d length=%d %s", size, length, klass()->external_name());
  debug_only(check_for_valid_allocation_state());
  assert(!Universe::heap()->is_gc_active(), "Allocation during gc not allowed");
  assert(size >= 0, "int won't convert to size_t");
  HeapWord* obj = common_mem_allocate_init(klass, size, CHECK_NULL);
  post_allocation_setup_array(klass, obj, length);
  NOT_PRODUCT(Universe::heap()->check_for_bad_heap_word_value(obj, size));
  if (VerboseDebugAllocation)  tty->print_cr("array_allocated %p: %d %s - thread=%p, id=%d, region_manager_active=%d", obj, size, klass()->external_name(),THREAD,THREAD->get_id(),THREAD->is_region_manager_active());
  return (oop)obj;
}

oop CollectedHeap::array_allocate_nozero(KlassHandle klass,
                                         int size,
                                         int length,
                                         TRAPS) {
  ResourceMark rm;
  if (VerboseDebugAllocation)  tty->print_cr("array_allocate_nozero %d %s - thread=%p, id=%d, region_manager_active=%d", size, klass()->external_name(),THREAD,THREAD->get_id(),THREAD->is_region_manager_active());
  //if (VerboseDebug) tty->print_cr("array_allocate_no_zero size=%d length=%d %s", size, length, klass()->external_name());
  debug_only(check_for_valid_allocation_state());
  assert(!Universe::heap()->is_gc_active(), "Allocation during gc not allowed");
  assert(size >= 0, "int won't convert to size_t");
  HeapWord* obj = common_mem_allocate_noinit(klass, size, CHECK_NULL);
  ((oop)obj)->set_klass_gap(0);
  post_allocation_setup_array(klass, obj, length);
#ifndef PRODUCT
  const size_t hs = oopDesc::header_size()+1;
  Universe::heap()->check_for_non_bad_heap_word_value(obj+hs, size-hs);
#endif

  if (VerboseDebugAllocation)  tty->print_cr("array_allocated_nozero %p: %d %s - thread=%p, id=%d, region_manager_active=%d", obj, size, klass()->external_name(),THREAD,THREAD->get_id(),THREAD->is_region_manager_active());

  return (oop)obj;
}

inline void CollectedHeap::oop_iterate_no_header(OopClosure* cl) {
  NoHeaderExtendedOopClosure no_header_cl(cl);
  oop_iterate(&no_header_cl);
}

#ifndef PRODUCT

inline bool
CollectedHeap::promotion_should_fail(volatile size_t* count) {
  // Access to count is not atomic; the value does not have to be exact.
  if (PromotionFailureALot) {
    const size_t gc_num = total_collections();
    const size_t elapsed_gcs = gc_num - _promotion_failure_alot_gc_number;
    if (elapsed_gcs >= PromotionFailureALotInterval) {
      // Test for unsigned arithmetic wrap-around.
      if (++*count >= PromotionFailureALotCount) {
        *count = 0;
        return true;
      }
    }
  }
  return false;
}

inline bool CollectedHeap::promotion_should_fail() {
  return promotion_should_fail(&_promotion_failure_alot_count);
}

inline void CollectedHeap::reset_promotion_should_fail(volatile size_t* count) {
  if (PromotionFailureALot) {
    _promotion_failure_alot_gc_number = total_collections();
    *count = 0;
  }
}

inline void CollectedHeap::reset_promotion_should_fail() {
  reset_promotion_should_fail(&_promotion_failure_alot_count);
}
#endif  // #ifndef PRODUCT

#endif // SHARE_VM_GC_INTERFACE_COLLECTEDHEAP_INLINE_HPP
