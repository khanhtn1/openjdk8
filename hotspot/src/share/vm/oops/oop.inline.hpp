/*
 * Copyright (c) 1997, 2013, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 *
 */

#ifndef SHARE_VM_OOPS_OOP_INLINE_HPP
#define SHARE_VM_OOPS_OOP_INLINE_HPP

#include "gc_implementation/shared/ageTable.hpp"
#include "gc_implementation/shared/markSweep.inline.hpp"
#include "gc_interface/collectedHeap.inline.hpp"
#include "memory/barrierSet.inline.hpp"
#include "memory/cardTableModRefBS.hpp"
#include "memory/genCollectedHeap.hpp"
#include "memory/generation.hpp"
#include "memory/specialized_oop_closures.hpp"
#include "oops/arrayKlass.hpp"
#include "oops/arrayOop.hpp"
#include "oops/klass.inline.hpp"
#include "oops/markOop.inline.hpp"
#include "oops/oop.hpp"
#include "runtime/atomic.hpp"
#include "runtime/os.hpp"
#include "utilities/macros.hpp"
#ifdef TARGET_ARCH_x86
# include "bytes_x86.hpp"
#endif
#ifdef TARGET_ARCH_sparc
# include "bytes_sparc.hpp"
#endif
#ifdef TARGET_ARCH_zero
# include "bytes_zero.hpp"
#endif
#ifdef TARGET_ARCH_arm
# include "bytes_arm.hpp"
#endif
#ifdef TARGET_ARCH_ppc
# include "bytes_ppc.hpp"
#endif

// ASRMM starts
// inline methods for _ids field
inline region_id_t oopDesc::get_region_id() {
  return (region_id_t) ((_ids & RegionIdMask) >> RegionIdShift);
}

inline thread_id_t oopDesc::get_thread_id() {
  return (thread_id_t) ((_ids & ThreadIdMask) >> ThreadIdShift);
}

inline int oopDesc::get_alloc_id() {
  return (int) ((_ids & AllocIdMask) >> AllocIdShift);
}


inline julong oopDesc::get_ids() {
  return _ids;
}

inline julong oopDesc::get_ids_all() {
  return _ids;
}

inline julong oopDesc::get_ids_except_alloc() { // contains only Region and Thread Id
  return _ids & RegionAndThreadIdMask;
}

inline void oopDesc::set_region_id (int id) {
  assert ((id & ~max_jubyte) == 0, "Region id overflow: more than 1 byte");
  int temp = max_jubyte;
  _ids &= (~temp);
  _ids |= ((julong)id & max_jubyte) << RegionIdShift;
  assert (get_region_id() == id, "sanity check");
}

inline void oopDesc::set_thread_id (int id) {
  assert ((id & ~max_jubyte) == 0, "Thread id overflow: more than 1 byte");
  int temp = (int)max_jubyte << ThreadIdShift;
  _ids &= (~temp);
  _ids |= ((julong)id & max_jubyte) << ThreadIdShift;
  assert (get_thread_id() == id, "sanity check");
}

inline void oopDesc::set_alloc_id(int id) {
  assert ((id & ~max_juint) == 0, "Alloc id overflow: more than 4 bytes");
  _ids |=  ((julong)id & max_juint) << AllocIdShift;
}

inline void oopDesc::set_ids(int region, int thread) {
  assert ((region & ~max_jubyte) == 0, "Region id overflow: more than 1 byte");
  assert ((thread & ~max_jubyte) == 0, "Thread id overflow: more than 1 byte");

  // 0 is to fill 0 all other high bits
  _ids = 0 | (((julong)region & max_jubyte) << RegionIdShift)
        | (((julong)thread & max_jubyte) << ThreadIdShift);
  assert (get_region_id() == region, "sanity check region id");
  assert (get_thread_id() == thread, "sanity check thread id");


}


inline void oopDesc::set_ids(int region, int thread, int alloc) {
  ShouldNotReachHere(); //save for future dev.
  assert ((region & ~max_jubyte) == 0, "Region id overflow: more than 1 byte");
  //assert ((page & ~max_jushort) == 0, "Page id overflow: more than 2 bytes");
  assert ((thread & ~max_jubyte) == 0, "Thread id overflow: more than 1 byte");
  assert ((alloc & ~max_juint) == 0, "Alloc id overflow: more than 4 bytes");

  // 0 is to fill 0 all other high bits
  _ids = 0 | (((julong)region & max_jubyte) << RegionIdShift)
    | (((julong)thread & max_jubyte) << ThreadIdShift)
    | (((julong)alloc & max_juint) << AllocIdShift);
}


inline bool oopDesc::is_in_region_physically() {
  return Universe::heap()->is_in_region(this);
}

inline bool oopDesc::is_region_allocated() {
  return (Universe::heap()->is_in_region(this) && get_region_id() != 0);
}


// ASRMM ends


// ASRMM starts, Khanh, Feb 1 2016
// TODO: We will change the IDs that we injected in the header to optimize
// Check left obj and right obj to decide record the right in remember set or not
// Return values:
// - 0 means nothing
// - 1 means (valid) inter-region: record right in its page's remember set: valid means right is the child of left.
// - 2 means heap-region: record right in heap's remember set

//  Universe::heap()->is_in_real_heap(void*) is checking differently from Universe::heap()->is_in_region(void*)
//        this uses MemRegion _real_heap				this uses MemRegion _iheap
inline int oopDesc::write_barrier_helper(oop left, void* field, oop right) {
  if (!RegionAllocate) return 0;

//  assert(left->is_oop(), err_msg("left oop %p should be a real oop", left));
//  assert(right->is_oop(), err_msg("right oop %p should be a real oop", right));
  assert(left != NULL, "sanity check - write barrier helper - left oop is NULL");

  if (right == NULL) return 0;
  
  region_id_t l_region = 0;
  region_id_t r_region = 0;
  bool left_in_heap = !Universe::heap()->is_in_region(left) || (l_region=left->get_region_id(), l_region==0);

  bool right_in_heap = !Universe::heap()->is_in_region(right) || (r_region=right->get_region_id(), r_region==0);
  
  if(ReportYakReferenceStats){
    Region::TOTAL_REFERENCES ++;
  }

  if (left_in_heap) {
    //Feb 27: since we didn't move esc to HEAP: this assert doesn't cover 100%
    //assert(Universe::heap()->is_in_real_heap(left), err_msg("left %p is not in real_heap ",left));
    if (right_in_heap) {
      // Feb 27: same as above
      //assert(Universe::heap()->is_in_real_heap(right), err_msg("right %p is not in real_heap ", right));
      return 0;
    }
    
    if(ReportYakReferenceStats){
      Region::REFERENCES_FROM_HEAP_TO_REGION++;
    }
    // right in region;
    if(DebugWriteBarrierHelper) {
      tty->print_cr("%p escaping from region[%d,%d] to heap %p.%p=%p",right, right->get_region_id(), right->get_thread_id(), left, field, right);
    }
    Region::add_escaping_object(right, field, left, 0);
    return 1;
  } 

  // left in region
  if (right_in_heap) {
    //assert(Universe::heap()->is_in_real_heap(right), err_msg("right %p is not in real_heap ", right));
    if(DebugWriteBarrierHelper)
      tty->print_cr("%p escaping from heap to region[%d,%d] %p.%p=%p",right, left->get_region_id(), left->get_thread_id(), left, field, right);
    
    if(ReportYakReferenceStats){
      Region::OTHER_REFERENCES ++;
    }

    Region::add_heap_object(right, field, left);
    return 2; // record in heap remembered set
  }

  //  both in regions
  if (!IgnoreCrossThreadBoundary) {
    thread_id_t l_thread = left->get_thread_id();
    thread_id_t r_thread = right->get_thread_id();
    if (l_thread != r_thread) {
      if(DebugWriteBarrierHelper){
	tty->print_cr("In write_barrier_helper and found cross thread boundary case: left=%p[%d,%d] and right=%p[%d,%d]", left,l_region,l_thread,right,r_region,r_thread);
      }
      if(ReportYakReferenceStats){
	Region::REFERENCES_FROM_HEAP_TO_REGION++;
      }
      Region::add_escaping_object(right, field, left, 0);
      return 1; // cross thread boundary, record this
    }
  }

  // in same thread;
  if (l_region >= r_region) return 0; // intra-region(==)  or right is parent of left (>) 
  if(DebugWriteBarrierHelper){
    thread_id_t l_thread = left->get_thread_id();
    thread_id_t r_thread = right->get_thread_id();
    tty->print_cr("%p escaping from region[%d,%d] to region[%d,%d] %p.%p=%p", right, r_region, r_thread, l_region, l_thread, left, field, right);
  }

  if(ReportYakReferenceStats){
    Region::REFERENCES_FROM_HEAP_TO_REGION++;
  }
  Region::add_escaping_object(right, field, left, l_region);
  return 1;
}


inline void oopDesc::write_barrier_array_copy_short(arrayOop array, void* addr, int length) {
  if (!RegionAllocate) return;

  assert(array != NULL, "sanity check - write barrier array copy - dest array is NULL");
  assert(array->is_objArray(), err_msg("array %p should be objArrayOop", array));


  oop* start_addr = (oop*) addr;
  oop* end_addr = start_addr + length;

  assert((uintptr_t)(((objArrayOop)array)->base() + array->length()) >= (uintptr_t)end_addr,
      err_msg("array copy exceeds the upper bound, array %p, addr %p, length %d", array, addr, length));
  assert((uintptr_t)((objArrayOop)array)->base() <= (uintptr_t)addr,
      err_msg("array copy exceeds the lower bound, array %p, addr %p, length %d", array, addr, length));

  for (oop* p = start_addr; p < end_addr; p++) {
    oop obj = load_heap_oop(p);
    if (oopDesc::is_null(obj)) continue;
    write_barrier_helper(array, (void*)p, obj);
  }
}


inline void oopDesc::write_barrier_array_copy_long(arrayOop array, void* addr, int length) {
  if (!RegionAllocate) return;

  assert(array != NULL, "sanity check - write barrier array copy - dest array is NULL");
  assert(array->is_objArray(), err_msg("array %p should be objArrayOop", array));


  if(ReportYakReferenceStats){
    Region::TOTAL_REFERENCES ++;
  }
  
  oop* start_addr = (oop*) addr;
  oop* end_addr = start_addr + length;
  
  assert((uintptr_t)(((objArrayOop)array)->base() + array->length()) >= (uintptr_t)end_addr,
      err_msg("array copy exceeds the upper bound, array %p, addr %p, length %d", array, addr, length));
  assert((uintptr_t)((objArrayOop)array)->base() <= (uintptr_t)start_addr,
      err_msg("array copy exceeds the lower bound, array %p, addr %p, length %d", array, addr, length));

  region_id_t l_region = 0;
  region_id_t r_region = 0;
  oop left = array;

  bool left_in_heap = !Universe::heap()->is_in_region(left) || (l_region=left->get_region_id(), l_region==0);


  for (oop* p = start_addr; p <end_addr; p++) {
    oop obj = load_heap_oop(p);
    if (obj == NULL) continue;
    void* field = (void*) p;
    oop right = obj;
    
    bool right_in_heap = !Universe::heap()->is_in_region(right) || (r_region=right->get_region_id(), r_region==0);


    if (left_in_heap) {
      if (!right_in_heap) {
        // right in region;
        if(DebugWriteBarrierHelper) {
          tty->print_cr("%p arraycopy escaping from region[%d,%d] to heap %p.%p=%p",
              right, right->get_region_id(), right->get_thread_id(), left, field, right);
        }
	if(ReportYakReferenceStats){
	  Region::REFERENCES_FROM_HEAP_TO_REGION++;
	}
        Region::add_escaping_object(right, field, left, 0);
      }
      continue;
    } 

    // left in region
    if (right_in_heap) {
      if(DebugWriteBarrierHelper) {
        tty->print_cr("%p arraycopy escaping from heap to region[%d,%d] %p.%p=%p",
                      right, left->get_region_id(), left->get_thread_id(), left, field, right);
      }
      if(ReportYakReferenceStats){
	Region::OTHER_REFERENCES++;
      }
      Region::add_heap_object(right, field, left);
      continue;
    }
 
    //  both in regions
    if (!IgnoreCrossThreadBoundary){
      thread_id_t l_thread = left->get_thread_id();
      thread_id_t r_thread = right->get_thread_id();
      assert (l_thread != 0, err_msg("left region obj:: invalid thread_id, left %p[%d,%d]",left,l_region,l_thread));
      assert (r_thread != 0, err_msg("right region obj:: invalid thread_id, right %p[%d,%d]",right,r_region,r_thread));
      if (l_thread != r_thread) {
        if (DebugWriteBarrierHelper) {
          tty->print_cr("In arraycopy write_barrier_helper and found cross thred boundary case: left=%p[%d,%d] and right=%p[%d,%d]",
                        left,l_region,l_thread,right,r_region,r_thread);
        }
	if(ReportYakReferenceStats){
	  Region::REFERENCES_FROM_HEAP_TO_REGION++;
	}

        Region::add_escaping_object(right, field, left, 0);
        continue;
      }
    }
     
    
    if (l_region < r_region) {  // NOT intra-region(==)  or right is parent of left (>)
      if (DebugWriteBarrierHelper) {
        thread_id_t l_thread = left->get_thread_id();
        thread_id_t r_thread = right->get_thread_id();
        tty->print_cr("%p arraycopy escaping from region[%d,%d] to region[%d,%d] %p.%p=%p",
                      right, r_region, r_thread, l_region, l_thread, left, field, right);
      }
      if(ReportYakReferenceStats){
	Region::REFERENCES_FROM_HEAP_TO_REGION++;
      }
      Region::add_escaping_object(right, field, left, l_region);
    }
  }
}


// Check if obj is shared by multiple threads
// Returns values:
// - 0 means nothing
// - 1 means it is shared, put it in its page's remember set
inline int oopDesc::read_barrier_helper(oop obj, Thread* thread) {
  if(true) return 0;
  if (!RegionAllocate || obj == NULL) return 0; // obj is loaded from a field of an object so it's possible it's NULL

  // only care about objects in region, objects in heap are ignored  
  if (Universe::heap()->is_in_region(obj) && obj->get_region_id() != 0) {
    assert (obj->is_oop(), err_msg("sanity check obj=%p", obj));
    assert (obj->get_thread_id() != 0, err_msg(" null thread? obj=%p",obj));
    thread_id_t thread_id = (thread_id_t)(thread == NULL ? Thread::current()->get_id() : thread->get_id());

    if (thread_id != 0 && obj->get_thread_id() != thread_id) {
      if (DebugLoadStatement)
        tty->print_cr("read_barrier_helper: obj=%p, region-id=%d, thread-id=%d, Shared Obj detected: in thread %p , id=%d",
            obj, obj->get_region_id(), obj->get_thread_id(),thread,thread_id);
      Region::add_escaping_object(obj); // TODO: I already have region_id and thread_id - BUT just another read to get Region* anyway - same with write_barrier_helper
      return 1; // cross thread boundary
    }
    return 0;
  }
  return 0;
}
// ASRMM ends

// Implementation of all inlined member functions defined in oop.hpp
// We need a separate file to avoid circular references

inline void oopDesc::release_set_mark(markOop m) {
  OrderAccess::release_store_ptr(&_mark, m);
}

inline markOop oopDesc::cas_set_mark(markOop new_mark, markOop old_mark) {
  return (markOop) Atomic::cmpxchg_ptr(new_mark, &_mark, old_mark);
}

inline Klass* oopDesc::klass() const {
  if (UseCompressedClassPointers) {
    return Klass::decode_klass_not_null(_metadata._compressed_klass);
  } else {
    return _metadata._klass;
  }
}

inline Klass* oopDesc::klass_or_null() const volatile {
  // can be NULL in CMS
  if (UseCompressedClassPointers) {
    return Klass::decode_klass(_metadata._compressed_klass);
  } else {
    return _metadata._klass;
  }
}

inline int oopDesc::klass_gap_offset_in_bytes() {
  assert(UseCompressedClassPointers, "only applicable to compressed klass pointers");
  return oopDesc::klass_offset_in_bytes() + sizeof(narrowKlass);
}

inline Klass** oopDesc::klass_addr() {
  // Only used internally and with CMS and will not work with
  // UseCompressedOops
  assert(!UseCompressedClassPointers, "only supported with uncompressed klass pointers");
  return (Klass**) &_metadata._klass;
}

inline narrowKlass* oopDesc::compressed_klass_addr() {
  assert(UseCompressedClassPointers, "only called by compressed klass pointers");
  return &_metadata._compressed_klass;
}

inline void oopDesc::set_klass(Klass* k) {
  // since klasses are promoted no store check is needed
  assert(Universe::is_bootstrapping() || k != NULL, "must be a real Klass*");
  assert(Universe::is_bootstrapping() || k->is_klass(), "not a Klass*");
  if (UseCompressedClassPointers) {
    *compressed_klass_addr() = Klass::encode_klass_not_null(k);
  } else {
    *klass_addr() = k;
  }
}

inline int oopDesc::klass_gap() const {
  return *(int*)(((intptr_t)this) + klass_gap_offset_in_bytes());
}

inline void oopDesc::set_klass_gap(int v) {
  if (UseCompressedClassPointers) {
    *(int*)(((intptr_t)this) + klass_gap_offset_in_bytes()) = v;
  }
}

inline void oopDesc::set_klass_to_list_ptr(oop k) {
  // This is only to be used during GC, for from-space objects, so no
  // barrier is needed.
  if (UseCompressedClassPointers) {
    _metadata._compressed_klass = (narrowKlass)encode_heap_oop(k);  // may be null (parnew overflow handling)
  } else {
    _metadata._klass = (Klass*)(address)k;
  }
}

inline oop oopDesc::list_ptr_from_klass() {
  // This is only to be used during GC, for from-space objects.
  if (UseCompressedClassPointers) {
    return decode_heap_oop((narrowOop)_metadata._compressed_klass);
  } else {
    // Special case for GC
    return (oop)(address)_metadata._klass;
  }
}

inline void   oopDesc::init_mark()                 { set_mark(markOopDesc::prototype_for_object(this)); }

inline bool oopDesc::is_a(Klass* k)        const { return klass()->is_subtype_of(k); }

inline bool oopDesc::is_instance()           const { return klass()->oop_is_instance(); }
inline bool oopDesc::is_instanceMirror()     const { return klass()->oop_is_instanceMirror(); }
inline bool oopDesc::is_instanceRef()        const { return klass()->oop_is_instanceRef(); }
inline bool oopDesc::is_array()              const { return klass()->oop_is_array(); }
inline bool oopDesc::is_objArray()           const { return klass()->oop_is_objArray(); }
inline bool oopDesc::is_typeArray()          const { return klass()->oop_is_typeArray(); }

inline void*     oopDesc::field_base(int offset)        const { return (void*)&((char*)this)[offset]; }

template <class T> inline T* oopDesc::obj_field_addr(int offset) const { 
  // ASRMM starts
  if(VerboseDebug) tty->print_cr("obj_field_address = this[%p]=%p.%d => %p",this, field_base(0),offset,field_base(offset));
  // ASRMM ends
  return (T*)field_base(offset); 
}

inline Metadata** oopDesc::metadata_field_addr(int offset) const { return (Metadata**)field_base(offset); }
inline jbyte*    oopDesc::byte_field_addr(int offset)   const { return (jbyte*)   field_base(offset); }
inline jchar*    oopDesc::char_field_addr(int offset)   const { return (jchar*)   field_base(offset); }
inline jboolean* oopDesc::bool_field_addr(int offset)   const { return (jboolean*)field_base(offset); }
inline jint*     oopDesc::int_field_addr(int offset)    const { return (jint*)    field_base(offset); }
inline jshort*   oopDesc::short_field_addr(int offset)  const { return (jshort*)  field_base(offset); }
inline jlong*    oopDesc::long_field_addr(int offset)   const { return (jlong*)   field_base(offset); }
inline jfloat*   oopDesc::float_field_addr(int offset)  const { return (jfloat*)  field_base(offset); }
inline jdouble*  oopDesc::double_field_addr(int offset) const { return (jdouble*) field_base(offset); }
inline address*  oopDesc::address_field_addr(int offset) const { return (address*) field_base(offset); }


// Functions for getting and setting oops within instance objects.
// If the oops are compressed, the type passed to these overloaded functions
// is narrowOop.  All functions are overloaded so they can be called by
// template functions without conditionals (the compiler instantiates via
// the right type and inlines the appopriate code).

inline bool oopDesc::is_null(oop obj)       { return obj == NULL; }
inline bool oopDesc::is_null(narrowOop obj) { return obj == 0; }

// Algorithm for encoding and decoding oops from 64 bit pointers to 32 bit
// offset from the heap base.  Saving the check for null can save instructions
// in inner GC loops so these are separated.

inline bool check_obj_alignment(oop obj) {
  return cast_from_oop<intptr_t>(obj) % MinObjAlignmentInBytes == 0;
}

inline narrowOop oopDesc::encode_heap_oop_not_null(oop v) {
  assert(!is_null(v), "oop value can never be zero");
  assert(check_obj_alignment(v), "Address not aligned");
  assert(Universe::heap()->is_in_reserved(v), "Address not in heap");
  address base = Universe::narrow_oop_base();
  int    shift = Universe::narrow_oop_shift();
  uint64_t  pd = (uint64_t)(pointer_delta((void*)v, (void*)base, 1));
  assert(OopEncodingHeapMax > pd, "change encoding max if new encoding");
  uint64_t result = pd >> shift;
  assert((result & CONST64(0xffffffff00000000)) == 0, "narrow oop overflow");
  assert(decode_heap_oop(result) == v, "reversibility");
  return (narrowOop)result;
}

inline narrowOop oopDesc::encode_heap_oop(oop v) {
  return (is_null(v)) ? (narrowOop)0 : encode_heap_oop_not_null(v);
}

inline oop oopDesc::decode_heap_oop_not_null(narrowOop v) {
  assert(!is_null(v), "narrow oop value can never be zero");
  address base = Universe::narrow_oop_base();
  int    shift = Universe::narrow_oop_shift();
  oop result = (oop)(void*)((uintptr_t)base + ((uintptr_t)v << shift));
  assert(check_obj_alignment(result), err_msg("address not aligned: " PTR_FORMAT, (void*) result));
  return result;
}

inline oop oopDesc::decode_heap_oop(narrowOop v) {
  return is_null(v) ? (oop)NULL : decode_heap_oop_not_null(v);
}

inline oop oopDesc::decode_heap_oop_not_null(oop v) { return v; }
inline oop oopDesc::decode_heap_oop(oop v)  { return v; }

// Load an oop out of the Java heap as is without decoding.
// Called by GC to check for null before decoding.
inline oop       oopDesc::load_heap_oop(oop* p)          { return *p; }
inline narrowOop oopDesc::load_heap_oop(narrowOop* p)    { return *p; }

// Load and decode an oop out of the Java heap into a wide oop.
 inline oop oopDesc::load_decode_heap_oop_not_null(oop* p)       { return *p; } // Khanh Feb 1 2016 : correct plcase to add read barrier helper?
inline oop oopDesc::load_decode_heap_oop_not_null(narrowOop* p) {
  return decode_heap_oop_not_null(*p);
}

// Load and decode an oop out of the heap accepting null
 inline oop oopDesc::load_decode_heap_oop(oop* p) { return *p; } // Khanh Feb 1 2016: correct place to add read barrier helper?
inline oop oopDesc::load_decode_heap_oop(narrowOop* p) {
  return decode_heap_oop(*p);
}

// Store already encoded heap oop into the heap.
inline void oopDesc::store_heap_oop(oop* p, oop v)                 { *p = v; }
inline void oopDesc::store_heap_oop(narrowOop* p, narrowOop v)     { *p = v; }

// Encode and store a heap oop.
inline void oopDesc::encode_store_heap_oop_not_null(narrowOop* p, oop v) {
  *p = encode_heap_oop_not_null(v);
}
inline void oopDesc::encode_store_heap_oop_not_null(oop* p, oop v) { *p = v; }

// Encode and store a heap oop allowing for null.
inline void oopDesc::encode_store_heap_oop(narrowOop* p, oop v) {
  *p = encode_heap_oop(v);
}
inline void oopDesc::encode_store_heap_oop(oop* p, oop v) { *p = v; }

// Store heap oop as is for volatile fields.
inline void oopDesc::release_store_heap_oop(volatile oop* p, oop v) {
  OrderAccess::release_store_ptr(p, v);
}
inline void oopDesc::release_store_heap_oop(volatile narrowOop* p,
                                            narrowOop v) {
  OrderAccess::release_store(p, v);
}

inline void oopDesc::release_encode_store_heap_oop_not_null(
                                                volatile narrowOop* p, oop v) {
  // heap oop is not pointer sized.
  OrderAccess::release_store(p, encode_heap_oop_not_null(v));
}

inline void oopDesc::release_encode_store_heap_oop_not_null(
                                                      volatile oop* p, oop v) {
  OrderAccess::release_store_ptr(p, v);
}

inline void oopDesc::release_encode_store_heap_oop(volatile oop* p,
                                                           oop v) {
  OrderAccess::release_store_ptr(p, v);
}
inline void oopDesc::release_encode_store_heap_oop(
                                                volatile narrowOop* p, oop v) {
  OrderAccess::release_store(p, encode_heap_oop(v));
}


// These functions are only used to exchange oop fields in instances,
// not headers.
inline oop oopDesc::atomic_exchange_oop(oop exchange_value, volatile HeapWord *dest) {
  if (VerboseDebug) tty->print_cr("atomic exchange oop");

  if (UseCompressedOops) {
    // encode exchange value from oop to T
    narrowOop val = encode_heap_oop(exchange_value);
    narrowOop old = (narrowOop)Atomic::xchg(val, (narrowOop*)dest);
    // decode old from T to oop
    return decode_heap_oop(old);
  } else {
    return (oop)Atomic::xchg_ptr(exchange_value, (oop*)dest);
  }
}

// In order to put or get a field out of an instance, must first check
// if the field has been compressed and uncompress it.
inline oop oopDesc::obj_field(int offset) const {
  
  // ASRMM starts, Khanh Feb 1 2016
  
  oop result =  UseCompressedOops ?
    load_decode_heap_oop(obj_field_addr<narrowOop>(offset)) :
    load_decode_heap_oop(obj_field_addr<oop>(offset));

  //if (RegionAllocate) {
  //   int code = read_barrier_helper(result);
    //if (DebugLoadStatement)  {
    // tty->print_cr("obj_field load field this[%p].offset=%d, field_obj=%p", this,offset,result);
    // ResourceMark rm;
    //  tty->print_cr("type info: this=%s result=%s", klass()->signature_name(), result == NULL ? " " : result->klass()->signature_name());
    //}
    //if (code != 0) {
    // if (DebugLoadStatement)
    //	tty->print_cr("obj_field this[%p].offset=%d Shared Obj detected: adding obj=%p", this,offset,result);
    //  
    //}
  // }

  return result;
  // ASRMM ends

  /* original code:
  return UseCompressedOops ?
    load_decode_heap_oop(obj_field_addr<narrowOop>(offset)) :
    load_decode_heap_oop(obj_field_addr<oop>(offset));
  */
}

inline volatile oop oopDesc::obj_field_volatile(int offset) const {
  volatile oop value = obj_field(offset);
  OrderAccess::acquire();
  return value;
}

inline void oopDesc::obj_field_put(int offset, oop value) {
  assert (this != NULL, "sanity");
  if (VerboseDebug ||DebugStoreStatement) tty->print_cr("obj_field_put this[%p].offset=%d, value=%p", this,offset,value); 
  UseCompressedOops ? oop_store(obj_field_addr<narrowOop>(offset), value, this) :
    oop_store(obj_field_addr<oop>(offset),       value, this);
}

inline Metadata* oopDesc::metadata_field(int offset) const {
  return *metadata_field_addr(offset);
}

inline void oopDesc::metadata_field_put(int offset, Metadata* value) {
  *metadata_field_addr(offset) = value;
}

inline void oopDesc::obj_field_put_raw(int offset, oop value) {
  if (VerboseDebug || DebugStoreStatement) tty->print_cr("obj_field_put_raw offset=%d, value=%p", offset,value);

  //  int code = write_barrier_helper(this, (void*)obj_field_addr<oop>(offset), value);

  UseCompressedOops ?
    encode_store_heap_oop(obj_field_addr<narrowOop>(offset), value) :
    encode_store_heap_oop(obj_field_addr<oop>(offset),       value);

  int code = write_barrier_helper(this, (void*)obj_field_addr<oop>(offset), value);
  // Remove this
  if (code !=0) {
    if (DebugStoreStatement) tty->print_cr("obj_field_put_raw offset=%d, value=%p code is %d", offset,value, code);
  }

}
inline void oopDesc::obj_field_put_volatile(int offset, oop value) {
  OrderAccess::release();
  obj_field_put(offset, value);
  OrderAccess::fence();
}

inline jbyte oopDesc::byte_field(int offset) const                  { return (jbyte) *byte_field_addr(offset);    }
inline void oopDesc::byte_field_put(int offset, jbyte contents)     { *byte_field_addr(offset) = (jint) contents; }

inline jboolean oopDesc::bool_field(int offset) const               { return (jboolean) *bool_field_addr(offset); }
inline void oopDesc::bool_field_put(int offset, jboolean contents)  { *bool_field_addr(offset) = (jint) contents; }

inline jchar oopDesc::char_field(int offset) const                  { return (jchar) *char_field_addr(offset);    }
inline void oopDesc::char_field_put(int offset, jchar contents)     { *char_field_addr(offset) = (jint) contents; }

inline jint oopDesc::int_field(int offset) const                    { return *int_field_addr(offset);        }
inline void oopDesc::int_field_put(int offset, jint contents)       { *int_field_addr(offset) = contents;    }

inline jshort oopDesc::short_field(int offset) const                { return (jshort) *short_field_addr(offset);  }
inline void oopDesc::short_field_put(int offset, jshort contents)   { *short_field_addr(offset) = (jint) contents;}

inline jlong oopDesc::long_field(int offset) const                  { return *long_field_addr(offset);       }
inline void oopDesc::long_field_put(int offset, jlong contents)     { *long_field_addr(offset) = contents;   }

inline jfloat oopDesc::float_field(int offset) const                { return *float_field_addr(offset);      }
inline void oopDesc::float_field_put(int offset, jfloat contents)   { *float_field_addr(offset) = contents;  }

inline jdouble oopDesc::double_field(int offset) const              { return *double_field_addr(offset);     }
inline void oopDesc::double_field_put(int offset, jdouble contents) { *double_field_addr(offset) = contents; }

inline address oopDesc::address_field(int offset) const              { return *address_field_addr(offset);     }
inline void oopDesc::address_field_put(int offset, address contents) { *address_field_addr(offset) = contents; }

inline oop oopDesc::obj_field_acquire(int offset) const {
  return UseCompressedOops ?
             decode_heap_oop((narrowOop)
               OrderAccess::load_acquire(obj_field_addr<narrowOop>(offset)))
           : decode_heap_oop((oop)
               OrderAccess::load_ptr_acquire(obj_field_addr<oop>(offset)));
}
inline void oopDesc::release_obj_field_put(int offset, oop value) {
  if(VerboseDebug) tty->print_cr("release_obj_field_put %p.%d=%p", this,offset,value);
  UseCompressedOops ?
    oop_store((volatile narrowOop*)obj_field_addr<narrowOop>(offset), value, this) :
    oop_store((volatile oop*)      obj_field_addr<oop>(offset),       value, this);
}

inline jbyte oopDesc::byte_field_acquire(int offset) const                  { return OrderAccess::load_acquire(byte_field_addr(offset));     }
inline void oopDesc::release_byte_field_put(int offset, jbyte contents)     { OrderAccess::release_store(byte_field_addr(offset), contents); }

inline jboolean oopDesc::bool_field_acquire(int offset) const               { return OrderAccess::load_acquire(bool_field_addr(offset));     }
inline void oopDesc::release_bool_field_put(int offset, jboolean contents)  { OrderAccess::release_store(bool_field_addr(offset), contents); }

inline jchar oopDesc::char_field_acquire(int offset) const                  { return OrderAccess::load_acquire(char_field_addr(offset));     }
inline void oopDesc::release_char_field_put(int offset, jchar contents)     { OrderAccess::release_store(char_field_addr(offset), contents); }

inline jint oopDesc::int_field_acquire(int offset) const                    { return OrderAccess::load_acquire(int_field_addr(offset));      }
inline void oopDesc::release_int_field_put(int offset, jint contents)       { OrderAccess::release_store(int_field_addr(offset), contents);  }

inline jshort oopDesc::short_field_acquire(int offset) const                { return (jshort)OrderAccess::load_acquire(short_field_addr(offset)); }
inline void oopDesc::release_short_field_put(int offset, jshort contents)   { OrderAccess::release_store(short_field_addr(offset), contents);     }

inline jlong oopDesc::long_field_acquire(int offset) const                  { return OrderAccess::load_acquire(long_field_addr(offset));       }
inline void oopDesc::release_long_field_put(int offset, jlong contents)     { OrderAccess::release_store(long_field_addr(offset), contents);   }

inline jfloat oopDesc::float_field_acquire(int offset) const                { return OrderAccess::load_acquire(float_field_addr(offset));      }
inline void oopDesc::release_float_field_put(int offset, jfloat contents)   { OrderAccess::release_store(float_field_addr(offset), contents);  }

inline jdouble oopDesc::double_field_acquire(int offset) const              { return OrderAccess::load_acquire(double_field_addr(offset));     }
inline void oopDesc::release_double_field_put(int offset, jdouble contents) { OrderAccess::release_store(double_field_addr(offset), contents); }

inline address oopDesc::address_field_acquire(int offset) const             { return (address) OrderAccess::load_ptr_acquire(address_field_addr(offset)); }
inline void oopDesc::release_address_field_put(int offset, address contents) { OrderAccess::release_store_ptr(address_field_addr(offset), contents); }

inline int oopDesc::size_given_klass(Klass* klass)  {
  int lh = klass->layout_helper();
  int s;

  // lh is now a value computed at class initialization that may hint
  // at the size.  For instances, this is positive and equal to the
  // size.  For arrays, this is negative and provides log2 of the
  // array element size.  For other oops, it is zero and thus requires
  // a virtual call.
  //
  // We go to all this trouble because the size computation is at the
  // heart of phase 2 of mark-compaction, and called for every object,
  // alive or dead.  So the speed here is equal in importance to the
  // speed of allocation.

  if (lh > Klass::_lh_neutral_value) {
    if (!Klass::layout_helper_needs_slow_path(lh)) {
      s = lh >> LogHeapWordSize;  // deliver size scaled by wordSize
    } else {
      s = klass->oop_size(this);
    }
  } else if (lh <= Klass::_lh_neutral_value) {
    // The most common case is instances; fall through if so.
    if (lh < Klass::_lh_neutral_value) {
      // Second most common case is arrays.  We have to fetch the
      // length of the array, shift (multiply) it appropriately,
      // up to wordSize, add the header, and align to object size.
      size_t size_in_bytes;
#ifdef _M_IA64
      // The Windows Itanium Aug 2002 SDK hoists this load above
      // the check for s < 0.  An oop at the end of the heap will
      // cause an access violation if this load is performed on a non
      // array oop.  Making the reference volatile prohibits this.
      // (%%% please explain by what magic the length is actually fetched!)
      volatile int *array_length;
      array_length = (volatile int *)( (intptr_t)this +
                          arrayOopDesc::length_offset_in_bytes() );
      assert(array_length > 0, "Integer arithmetic problem somewhere");
      // Put into size_t to avoid overflow.
      size_in_bytes = (size_t) array_length;
      size_in_bytes = size_in_bytes << Klass::layout_helper_log2_element_size(lh);
#else
      size_t array_length = (size_t) ((arrayOop)this)->length();
      size_in_bytes = array_length << Klass::layout_helper_log2_element_size(lh);
#endif
      size_in_bytes += Klass::layout_helper_header_size(lh);

      // This code could be simplified, but by keeping array_header_in_bytes
      // in units of bytes and doing it this way we can round up just once,
      // skipping the intermediate round to HeapWordSize.  Cast the result
      // of round_to to size_t to guarantee unsigned division == right shift.
      s = (int)((size_t)round_to(size_in_bytes, MinObjAlignmentInBytes) /
        HeapWordSize);

      // UseParNewGC, UseParallelGC and UseG1GC can change the length field
      // of an "old copy" of an object array in the young gen so it indicates
      // the grey portion of an already copied array. This will cause the first
      // disjunct below to fail if the two comparands are computed across such
      // a concurrent change.
      // UseParNewGC also runs with promotion labs (which look like int
      // filler arrays) which are subject to changing their declared size
      // when finally retiring a PLAB; this also can cause the first disjunct
      // to fail for another worker thread that is concurrently walking the block
      // offset table. Both these invariant failures are benign for their
      // current uses; we relax the assertion checking to cover these two cases below:
      //     is_objArray() && is_forwarded()   // covers first scenario above
      //  || is_typeArray()                    // covers second scenario above
      // If and when UseParallelGC uses the same obj array oop stealing/chunking
      // technique, we will need to suitably modify the assertion.
      assert((s == klass->oop_size(this)) ||
             (Universe::heap()->is_gc_active() &&
              ((is_typeArray() && UseParNewGC) ||
               (is_objArray()  && is_forwarded() && (UseParNewGC || UseParallelGC || UseG1GC)))),
             "wrong array object size");
    } else {
      // Must be zero, so bite the bullet and take the virtual call.
      s = klass->oop_size(this);
    }
  }

  assert(s % MinObjAlignment == 0, "alignment check");
  assert(s > 0, "Bad size calculated");
  return s;
}


inline int oopDesc::size()  {
  return size_given_klass(klass());
}

inline void update_barrier_set(void* p, oop v) {
  assert(oopDesc::bs() != NULL, "Uninitialized bs in oop!");
  oopDesc::bs()->write_ref_field(p, v);
}

template <class T> inline void update_barrier_set_pre(T* p, oop v) {
  oopDesc::bs()->write_ref_field_pre(p, v);
}

// ASRMM refactor: called by referenceProcessor
template <class T> inline void oop_store_original(T* p, oop v) {
    if (always_do_update_barrier) {
      oop_store((volatile T*)p, v); // missing the 3rd argument -> NULL -> assertion failure but that always_do_update_barrier is false anyway
    } else {
      //tty->print("******* storing into %p value %p ", p, v);
      //tty->print_cr("p in region= %d, v in region = %d **********", Universe::heap()->is_in_region(p),Universe::heap()->is_in_region(v));
      update_barrier_set_pre(p, v);
      oopDesc::encode_store_heap_oop(p, v);
      update_barrier_set((void*)p, v);  // cast away type
    }
  }

template <class T> inline void oop_store(T* p, oop v, oop o) {
  if (VerboseDebug || DebugOopStore) tty->print_cr("oop_store %p", p);
  
  assert (o !=NULL, "oop_store Failed to pass in receiver object!!!");

  if (always_do_update_barrier) {
    oop_store((volatile T*)p, v, o);
  } else {
    //    int code = oopDesc::write_barrier_helper(o,(void*)p,v);
    
    update_barrier_set_pre(p, v);
    oopDesc::encode_store_heap_oop(p, v);
    update_barrier_set((void*)p, v);  // cast away type
    

    int code = oopDesc::write_barrier_helper(o,(void*)p,v);
    //Remove 
    if (code != 0) {
      
    }

  }
}

template <class T> inline void oop_store(volatile T* p, oop v, oop o) {
  if (VerboseDebug || DebugOopStore) tty->print_cr("oop_store volatile %p ", p );

  assert (o !=NULL, "oop_store volatile Failed to pass in receiver object!!!");

  //  int code = oopDesc::write_barrier_helper(o, (void*)p, v);

  update_barrier_set_pre((T*)p, v);   // cast away volatile
  // Used by release_obj_field_put, so use release_store_ptr.
  oopDesc::release_encode_store_heap_oop(p, v);
  update_barrier_set((void*)p, v);    // cast away type

  int code = oopDesc::write_barrier_helper(o, (void*)p, v);
  // Remove 
  if (code != 0) {
    
  }

}

// Should replace *addr = oop assignments where addr type depends on UseCompressedOops
// (without having to remember the function name this calls).
//inline void oop_store_raw(HeapWord* addr, oop value, oop o=NULL) {
inline void oop_store_raw(HeapWord* addr, oop value) {

  if (VerboseDebug||DebugOopStore) tty->print_cr("oop_store raw addr=%p value=%p ",addr,value);

  //  if (value != NULL)  assert (o !=NULL, "oop_store_raw Failed to pass in receiver object!!!");

  //TODO
  if (UseCompressedOops) {
    oopDesc::encode_store_heap_oop((narrowOop*)addr, value);
  } else {

    assert(!Universe::heap()->is_in_region(addr), "addr is in region space");
    assert(value == NULL || (value !=NULL && !Universe::heap()->is_in_region(value)), "non-null value is in region space");
    //int code = oopDesc::write_barrier_helper(o,value);

    oopDesc::encode_store_heap_oop((oop*)addr, value);

    //if (code != 0) {
    //  value->escaping_object_do_work();
    // }

  }
}

inline oop oopDesc::atomic_compare_exchange_oop(oop exchange_value,
                                                volatile HeapWord *dest,
                                                oop compare_value,
                                                bool prebarrier) {
  if (VerboseDebug) tty->print_cr("atomic compare exchange oop");

  if (UseCompressedOops) {
    if (prebarrier) {
      update_barrier_set_pre((narrowOop*)dest, exchange_value);
    }
    // encode exchange and compare value from oop to T
    narrowOop val = encode_heap_oop(exchange_value);
    narrowOop cmp = encode_heap_oop(compare_value);

    narrowOop old = (narrowOop) Atomic::cmpxchg(val, (narrowOop*)dest, cmp);
    // decode old from T to oop
    return decode_heap_oop(old);
  } else {
    if (prebarrier) {
      update_barrier_set_pre((oop*)dest, exchange_value);
    }
    return (oop)Atomic::cmpxchg_ptr(exchange_value, (oop*)dest, compare_value);
  }
}

// Used only for markSweep, scavenging
inline bool oopDesc::is_gc_marked() const {
  return mark()->is_marked();
}

inline bool oopDesc::is_locked() const {
  return mark()->is_locked();
}

inline bool oopDesc::is_unlocked() const {
  return mark()->is_unlocked();
}

inline bool oopDesc::has_bias_pattern() const {
  return mark()->has_bias_pattern();
}


// used only for asserts
inline bool oopDesc::is_oop(bool ignore_mark_word) const {
  oop obj = (oop) this;
  if (DebugIsOop) tty->print_cr("is_oop %p", obj);
  if (!check_obj_alignment(obj)) return false;
  if (DebugIsOop) tty->print_cr("is_oop %p pass alignment", obj);
  //if (!Universe::heap()->is_in_reserved(obj) ) return false;
  // Khanh Jan 13 2016: replace the condition above
  //if (!Universe::heap()->is_in_reserved_or_region(obj)) return false;
  // obj is aligned and accessible in heap
  if (!Universe::heap()->is_in_reserved(obj) /*&& !Universe::heap()->in_off_heap(obj)*/ )return false;//!obj->is_region_allocated() ) return false;
  //  if (DebugIsOop) tty->print_cr("is_oop %p pass in reserved %d and in offheap %d ", obj,Universe::heap()->is_in_reserved(obj),Universe::heap()->in_off_heap(obj) );
  void* klass = obj->klass_or_null();
  //if (DebugIsOop) tty->print_cr("before check klass pointer=%p", klass);
  if (Universe::heap()->is_in_reserved(obj->klass_or_null())) return false;
  //if (DebugIsOop) tty->print_cr("is_oop %p pass klass pointer ", obj);


  // Header verification: the mark is typically non-NULL. If we're
  // at a safepoint, it must not be null.
  // Outside of a safepoint, the header could be changing (for example,
  // another thread could be inflating a lock on this object).
  if (ignore_mark_word) {
    return true;
  }
  if (mark() != NULL) {
    return true;
  }
  return !SafepointSynchronize::is_at_safepoint();
}


// used only for asserts
inline  bool oopDesc::is_oop_or_null(bool ignore_mark_word) const {
  //  if (VerboseDebug) tty->print_cr("is_oop_or_null %p",this);
  ShouldNotReachHere();
  //  return this == NULL ? true : is_oop(ignore_mark_word);
}


//Khanh 
inline bool oopDesc::null_or_is_oop(oop p, bool ignore_mark_word) {
  if (p == NULL) return true;
  return p->is_oop(ignore_mark_word);
}

#ifndef PRODUCT
// used only for asserts
inline bool oopDesc::is_unlocked_oop() const {
  if (!Universe::heap()->is_in_reserved(this)) return false;
  return mark()->is_unlocked();
}
#endif // PRODUCT

inline void oopDesc::follow_contents(void) {
  assert (is_gc_marked(), "should be marked");
  klass()->oop_follow_contents(this);
}

// Used by scavengers

inline bool oopDesc::is_forwarded() const {
  // The extra heap check is needed since the obj might be locked, in which case the
  // mark would point to a stack location and have the sentinel bit cleared
  return mark()->is_marked();
}

// Used by scavengers
inline void oopDesc::forward_to(oop p) {
  assert(check_obj_alignment(p),
         "forwarding to something not aligned");
  assert(Universe::heap()->is_in_reserved(p),
         "forwarding to something not in heap");
  markOop m = markOopDesc::encode_pointer_as_mark(p);
  assert(m->decode_pointer() == p, "encoding must be reversable");
  set_mark(m);
}

// Used by parallel scavengers
inline bool oopDesc::cas_forward_to(oop p, markOop compare) {
  assert(check_obj_alignment(p),
         "forwarding to something not aligned");
  assert(Universe::heap()->is_in_reserved(p),
         "forwarding to something not in heap");
  markOop m = markOopDesc::encode_pointer_as_mark(p);
  assert(m->decode_pointer() == p, "encoding must be reversable");
  return cas_set_mark(m, compare) == compare;
}

// Note that the forwardee is not the same thing as the displaced_mark.
// The forwardee is used when copying during scavenge and mark-sweep.
// It does need to clear the low two locking- and GC-related bits.
inline oop oopDesc::forwardee() const {
  return (oop) mark()->decode_pointer();
}

inline bool oopDesc::has_displaced_mark() const {
  return mark()->has_displaced_mark_helper();
}

inline markOop oopDesc::displaced_mark() const {
  return mark()->displaced_mark_helper();
}

inline void oopDesc::set_displaced_mark(markOop m) {
  mark()->set_displaced_mark_helper(m);
}

// The following method needs to be MT safe.
inline uint oopDesc::age() const {
  assert(!is_forwarded(), "Attempt to read age from forwarded mark");
  if (has_displaced_mark()) {
    return displaced_mark()->age();
  } else {
    return mark()->age();
  }
}

inline void oopDesc::incr_age() {
  assert(!is_forwarded(), "Attempt to increment age of forwarded mark");
  if (has_displaced_mark()) {
    set_displaced_mark(displaced_mark()->incr_age());
  } else {
    set_mark(mark()->incr_age());
  }
}


inline intptr_t oopDesc::identity_hash() {
  // Fast case; if the object is unlocked and the hash value is set, no locking is needed
  // Note: The mark must be read into local variable to avoid concurrent updates.
  markOop mrk = mark();
  if (mrk->is_unlocked() && !mrk->has_no_hash()) {
    return mrk->hash();
  } else if (mrk->is_marked()) {
    return mrk->hash();
  } else {
    return slow_identity_hash();
  }
}

inline int oopDesc::adjust_pointers() {
  debug_only(int check_size = size());
  int s = klass()->oop_adjust_pointers(this);
  assert(s == check_size, "should be the same");
  return s;
}

#define OOP_ITERATE_DEFN(OopClosureType, nv_suffix)                        \
                                                                           \
inline int oopDesc::oop_iterate(OopClosureType* blk) {                     \
  SpecializationStats::record_call();                                      \
  return klass()->oop_oop_iterate##nv_suffix(this, blk);               \
}                                                                          \
                                                                           \
inline int oopDesc::oop_iterate(OopClosureType* blk, MemRegion mr) {       \
  SpecializationStats::record_call();                                      \
  return klass()->oop_oop_iterate##nv_suffix##_m(this, blk, mr);       \
}


inline int oopDesc::oop_iterate_no_header(OopClosure* blk) {
  // The NoHeaderExtendedOopClosure wraps the OopClosure and proxies all
  // the do_oop calls, but turns off all other features in ExtendedOopClosure.
  NoHeaderExtendedOopClosure cl(blk);
  return oop_iterate(&cl);
}

inline int oopDesc::oop_iterate_no_header(OopClosure* blk, MemRegion mr) {
  NoHeaderExtendedOopClosure cl(blk);
  return oop_iterate(&cl, mr);
}

ALL_OOP_OOP_ITERATE_CLOSURES_1(OOP_ITERATE_DEFN)
ALL_OOP_OOP_ITERATE_CLOSURES_2(OOP_ITERATE_DEFN)

#if INCLUDE_ALL_GCS
#define OOP_ITERATE_BACKWARDS_DEFN(OopClosureType, nv_suffix)              \
                                                                           \
inline int oopDesc::oop_iterate_backwards(OopClosureType* blk) {           \
  SpecializationStats::record_call();                                      \
  return klass()->oop_oop_iterate_backwards##nv_suffix(this, blk);     \
}

ALL_OOP_OOP_ITERATE_CLOSURES_1(OOP_ITERATE_BACKWARDS_DEFN)
ALL_OOP_OOP_ITERATE_CLOSURES_2(OOP_ITERATE_BACKWARDS_DEFN)
#endif // INCLUDE_ALL_GCS

#endif // SHARE_VM_OOPS_OOP_INLINE_HPP
