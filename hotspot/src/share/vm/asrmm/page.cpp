/*
 * page.cpp
 *
 *  Created on: Nov 23, 2015
 *      Author: Khanh
 */

#include "page.hpp"
#include "memory/allocation.hpp"
#include "memory/allocation.inline.hpp"
#include "utilities/copy.hpp"
#include "gc_interface/collectedHeap.hpp"
#include "remSet.hpp"



long Page::Page_Full_Threshold = PageSize * 0.8;
size_t HoleSummary::Hole_Distance_Threshold = 3; 
bool Page::BITARRAY_ENABLED = false;

// ===== Start of Hole Summary ===== //
void HoleSummary::print() {
  
  for(size_t i = 0; i < _num_holes; i++){
    assert (*(_summary + (i <<1)) != NULL, "sanity");
    assert (*(_summary + (i <<1) + 1) != NULL, "sanity");
    tty->print_cr("%p \t %p", *(_summary + (i <<1)), *(_summary + (i <<1) + 1));
  }

}

void HoleSummary::insert(HeapWord* addr, size_t size) {
    if(_summary == NULL){
      size_t s = sizeof(HeapWord*) * _capacity;
      _summary = (HeapWord**)malloc(s);
      memset(_summary, 0, s);
    } // is this needed??? didn't we already alloc before?

    assert (addr != NULL, "sanity");
    if (DebugHoleSummary)
      tty->print_cr("\n adding %p, size %d -> %p", addr, size, addr+size);
    _total_size += size;
    if(_num_holes == 0){
      *_summary = addr;
      *(_summary+1) = addr + size;
      _num_holes =1;
      _cursor = _summary;
      if (DebugHoleSummary)  print();
      return;
    }
    size_t half = _num_holes -1;

    HeapWord** left = _summary;
    HeapWord** right = _summary + (half << 1); // last valid element -- it's 0-based

    // FastPath and duplicate removal
    if (addr == *left || addr == *right) {
      if (DebugHoleSummary) 
        tty->print_cr("duplicated at %s", addr == *left ? "head" : "tail");
      if (addr == *left) assert (pointer_delta(*(left+1), *left) ==  size, "must be");
      else assert (pointer_delta(*(right+1), *right) ==  size, "must be");
      return;
    }

    //find the pos
    HeapWord** pos= NULL;
    bool found_pos = false;
    if (addr < *left) {
      if ((addr +size) == *left){ // merge opportunity
        *left= addr;
        if (DebugHoleSummary) 
          tty->print_cr("merged head ");
        return;
      }
      found_pos = true;
      pos = left;
    }

    if (!found_pos) {
      if (addr > *right) {
        if (*(right+1) == addr) { //merge opportunity
          *(right+1) = addr + size;
          if (DebugHoleSummary)
            tty->print_cr("merged tail");
          return;
        }
        found_pos = true;
        pos = right+2;
      }
    }

    if (!found_pos) {
      while(left < right) {
        half = (right - left) >> 1 ;
        HeapWord** midpoint = left +  (half << 1); // find

        if(addr > *midpoint) {
          left = midpoint + 2; //advance to next valid element
        } else if(addr < *midpoint){
          right = midpoint -2; // back to prev valid element
        }else{
          assert(false, "A hole with the same address already exists!");
          return;
        }
      }

      assert(left < _summary + _capacity, "buffer overflow 1!");

      if (left >right) {
        left = right;
        //assert (false, "go too far");
      }

      //find the pos
      if(addr < *left){
        pos = left ;
      } else if (addr == *left){
        assert(false, "A hole with the same address left already exists!");
      } else if (addr == *right) {
        assert(false, "A hole with the same address right already exists!");
      } else {
        pos = left+2;
      }
      found_pos = true;
    }

    assert (found_pos, "sanity");
    if (DebugHoleSummary) {
      tty->print_cr("before adding");
      print();
    }
    
    //Here we try to see if we can merge holes
    bool expand_prev = false; // are we expanding prev hole? 
    bool expand_next = false; // are we expanding next hole?

    if(pos != _summary){
      HeapWord* prev_end = *(pos - 1);
      if (prev_end == addr) {
        expand_prev = true;
      } 
      //else {
      //  assert ((size_t)(addr - prev_end) > Hole_Distance_Threshold, err_msg("should be large %p %p %d", addr, prev_end, (size_t)(addr - prev_end)));
      //}
      // Let's just be conservative, only if you can make the hole continuos then merge, otherwise, dont
    }

   
    /*   size_t diff = (size_t)(addr - prev_end);
      if(diff <= Hole_Distance_Threshold){
        *(pos - 1) = addr + size;
        _total_size += diff;
        tty->print_cr("merged1 %p, %p, %p, %p", *(pos - 2), prev_end, addr, addr+size);
        //        return;
      }
      }*/


    HeapWord** end_summary =  _summary + (_num_holes << 1);
    if(pos < end_summary){
      HeapWord* next_start = *(pos);
      if (next_start == (addr + size)) {
        expand_next = true;
      } 
      //else {
      //  assert ((size_t)(next_start - (addr + size)) > Hole_Distance_Threshold, "should be large");
      //} Same as above
    }

    /*
      size_t diff = (size_t)(next_start - (addr + size));
      if(diff <= Hole_Distance_Threshold){
        *pos = addr;
        _total_size += diff;
        tty->print_cr("merged2 %p, %p, %p, %p", next_start, *(pos+1), addr, addr+size);
        return;
      }
      }*/

    if (expand_prev) {
      if (expand_next) {
        // special case, shrink down 1 hole
        if (DebugHoleSummary) 
          tty->print_cr("special case");
        *(pos - 1) = *( pos + 1);
        if (DebugHoleSummary) {
          tty->print_cr("before memmove");
          print();
        }
        size_t num_bytes = pointer_delta((void*)end_summary, (void*)(pos+2), 1);
        memmove((void*)(pos),  (void*)(pos+2), num_bytes);
        if(DebugHoleSummary) {
          tty->print_cr("after memmove");
          print();
        }
        _num_holes --;
        return;
      }
      else { // expand previous hole
        *(pos - 1) = addr + size;        
        if (DebugHoleSummary) {
          tty->print_cr("expand previous hole");
          print();
        }
        return;
      }
    } else {
      if (expand_next) {  // expand next hole
        *pos = addr;
        if (DebugHoleSummary) {
          tty->print_cr("expand next hole");
          print();
        }
        return;

      }
    }

    //No merging opportunities are found
    //Let's insert a new entry
    if(pos < end_summary) {
      size_t num_bytes = pointer_delta((void*)end_summary, (void*)pos, 1);
      memmove((void*)(pos+2),  (const void*)pos, num_bytes);
    }
    assert(pos + 1 < _summary + _capacity, "buffer overflow!");
    assert (pos <= end_summary, "must be");

    *pos =  addr;
    *(pos+1) = addr + size;
    _num_holes ++;

    bool HOLE_SANITY_CHECK = false;
    if(HOLE_SANITY_CHECK || DebugHoleSummary){
      HeapWord *prev = NULL, *end = NULL;
      for(size_t i = 0; i < _num_holes; i++){
        assert (*(_summary + (i <<1)) != NULL, "sanity");
        assert (*(_summary + (i <<1) + 1) != NULL, "sanity");
        if(prev != NULL){
          //if(*(_summary + (i <<1)) < prev)
          //  tty->print_cr("wrong, i: %d, prev %p %p new %p %p", i, prev, *(_summary + ((i <<1) - 1)), *(_summary + (i <<1)),  *(_summary + ((i <<1)+1)));
          assert (*(_summary + (i <<1)) > prev && *(_summary + (i <<1)) > end , "not sorted!!!");
        }
        prev = *(_summary + (i <<1));
        end = *(_summary + (i <<1) + 1);
      }
    }
    if (DebugHoleSummary) {
      tty->print_cr("after adding");
      print();
    }
    if((_num_holes << 1) >= _capacity){
      size_t new_capacity = (_capacity << 1) * sizeof(HeapWord*);
      HeapWord **newarray = (HeapWord**)malloc(new_capacity);
      memset((void*)newarray, 0, new_capacity);
      memmove((void*)newarray, (const void*)_summary, sizeof(HeapWord*) * _capacity);
      free(_summary);
      _summary = newarray;
      _capacity = _capacity << 1;
    }
  }

// ===== End of Hole Summary ===== // 
Page::Page() {

}

Page::~Page() {
  if(_hs != NULL){
    delete _hs;
  }
}

// No longer use
/*
HeapWord* getSpace(size_t pageSize) {
  HeapWord* result = NULL;

  size_t t = pageSize << LogHeapWordSize;
  ReservedSpace rs(t);
  assert (rs.is_reserved(), " failed to reserve? ");

  MemRegion r = MemRegion((HeapWord*) rs.base(),
      (HeapWord*) (rs.base() + rs.size()));
  //  tty->print_cr("size=%d",pointer_delta((HeapWord*)rs.base(),(HeapWord*)(rs.base() + rs.size()))  );
  bool succeed = os::commit_memory((char*) rs.base(), t, true);
  assert (succeed, "failed to commit?");
  bool unguard = os::unguard_memory((char*) rs.base(), t);
  assert (unguard, "unguard");
  return (HeapWord*) rs.base();
}
*/

//pageSize in unit of HeapWord
Page* Page::brand_new_page(size_t pageSize) {

  HeapWord* space = Universe::heap()->allocate_new_page(pageSize);
  
  if(space == NULL){
    tty->print_cr("Cannot obtain a page!");
  }
  assert (pageSize == (size_t)PageSize, "must be"); // parameter is provided to future improvement where we have different size 
  assert(is_ptr_aligned((void*)space, (size_t) 1<<LogPageSize), "failed assumption, space not aligned to PageSize");

  // clear all
  Copy::zero_to_words(space, pageSize);

  Page* page = (Page*) space;
  
  page->initialize();

  if (DebugPageAllocation) {
    tty->print_cr("Allocated page %p of size %d starting from %p, top is %p", page, pageSize, space, page->top());
  }

  return page;
}

Page* Page::get_page(void* addr) {
  void* result = (void*) (uintptr_t(addr) & ~((1 << LogPageSize) - 1));
  //if (DebugPageAllocation) tty->print_cr("tracing back page start from addr %p = %p", addr, result);
  return (Page*) result;
}


void Page::initialize() {

  if (Page::BITARRAY_ENABLED) {
    set_top(get_holes_addr() + Page::bitmap_size_in_words());
  } else {
    set_top(get_holes_addr()); 
  }

  set_end((HeapWord*)this + PageSize);
  set_has_holes(false);
  init_hs();
  _dead_space_size = pointer_delta(_top,(HeapWord*)this);
}
  
void Page::reset() {
  if (Page::BITARRAY_ENABLED) {
    set_top(get_holes_addr() + Page::bitmap_size_in_words());
  } else {
    set_top (get_holes_addr());
  }
  if (UseYakHoleSummary) {
   _hs->reset_cursor();
  }
  
  assert ((void*)_top > (void*)this, "sanity");
  

}


// BitMap business:
// Inefficiency: begin and end should be offset from obj to the initial top: this+space_for_all_fields+bitmap_size;

BitArray* Page::get_bit_array() {
  return ((BitArray*)get_holes_addr());
}

bool Page::mark_hole(oop obj, size_t obj_size) {
  _has_holes = true;
  assert (Page::BITARRAY_ENABLED, "must be");

  size_t begin = pointer_delta((HeapWord*) obj, (HeapWord*) this);
  assert (begin >=0, "sanity");
  assert (begin <= get_bit_array()->size(), err_msg(" sanity begin %d",begin)); 
  size_t end = begin + obj_size;
  size_t one_index = get_bit_array()->get_next_one_offset_inline(begin, end);
  if (one_index < end) { // there is 1 in [begin, end] 
    if (DebugBuildMapInPage) tty->print_cr("Page %p: skip double marking %p {%d} %d-%d", this,obj, obj_size,begin,end);
    return false;
    
  }
  _dead_space_size+=obj_size;
  if (DebugBuildMapInPage) tty->print_cr("page %p: esc_obj is %p: begin=%d -- end=%d -- size=%d",this, obj,begin,end,obj_size);
  return get_bit_array()->set_range(begin, end);
}

void Page::clear_hole(oop obj, size_t obj_size) {
  
  assert (Page::BITARRAY_ENABLED, "must be");

  size_t begin = pointer_delta((HeapWord*) obj, (HeapWord*) this);

  size_t end = begin + obj_size;
  //if (DebugBuildMapInPage) tty->print_cr("esc obj at %d/%d is %p==%p: begin=%d -- end=%d -- size=%d",i,length,p,ptr,begin,end,ptr_size);
  get_bit_array()->clear_range(begin, end); 

}

