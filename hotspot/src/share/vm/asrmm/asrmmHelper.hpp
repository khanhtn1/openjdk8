/*
 * ASRMM code begin, author: Lu Fang, introduced - 02/22/2016.
 * This file contains some helper functions.
 */

#ifndef SHARE_VM_ASRMM_ASRMMHELPER_HPP
#define SHARE_VM_ASRMM_ASRMMHELPER_HPP

#include "classfile/javaClasses.hpp"
#include "gc_implementation/parallelScavenge/psCompactionManager.hpp"
#include "gc_implementation/parallelScavenge/psCompactionManager.inline.hpp"
#include "gc_implementation/parallelScavenge/psParallelCompact.hpp"
#include "gc_implementation/parallelScavenge/psScavenge.hpp"
#include "gc_implementation/parallelScavenge/psPromotionManager.hpp"
#include "gc_interface/collectedHeap.hpp"
#include "memory/universe.hpp"
#include "oops/instanceMirrorKlass.hpp"
#include "oops/objArrayKlass.hpp"
#include "oops/objArrayKlass.inline.hpp"
#include "oops/markOop.hpp"
#include "utilities/stack.hpp"

class ASRMMHelper {
 public:

  // This method check if obj is logically a heap object.
  inline static bool is_logic_in_heap(oop obj) {
    if (!Universe::heap()->is_in_region((const void*)obj)) return true;
    if (obj->get_region_id() == 0) return true;
    return false;
  }

  // This method moves the obj and set the region id for it.
  // Return the new address.
  inline static oop move_and_mark(oop obj,
      region_id_t region_id,
      thread_id_t thread_id) {
    assert(region_id != 0, "heap object cannot be moved!");
    assert(obj->get_region_id() != region_id, "move to the original region!");
    size_t obj_size = obj->size();

    // Copy the obj to the appropriate region.
    Region* new_region = RegionTable::get_region(region_id, thread_id);
    oop new_obj = (oop)new_region->allocate(obj_size);
    Copy::aligned_disjoint_words((HeapWord*)obj, (HeapWord*)new_obj, obj_size);
    new_obj->set_region_id(region_id);
      
    // Leave the forwarding mark.
    // XXX: since we only move the thread-local objects, forward_to should be enough.
    // If we have some unknown concurrent problem, we may try cas_forward_to.
    obj->forward_to(new_obj);
    
    assert(obj->is_forwarded(), "must be");
    assert(obj->forwardee() == new_obj, "must be same forward info");
    obj->set_region_id(region_id);
      
    return new_obj;
  }

  // This method processes one field for obj.
  inline static void process_field(oop* field, oop cur, Stack<oop, mtThread>* stack,
      region_id_t ret_region_id, // The id of the retiring region.
      region_id_t dest_region_id,
      thread_id_t thread_id) {

    assert(dest_region_id != ret_region_id, "no object stays in retiring region");

    oop obj = oopDesc::load_heap_oop(field);
    if (VerboseDebugProcessField) tty->print_cr("loading field=%p, oop=%p", field, obj);
    if (oopDesc::is_null(obj)) return;

    // cur.field ==> obj
    // 1) if cur is a heap object ==> 
    //   1.1) obj is a heap object, (do nothing)
    //   1.2) obj is in current region, (mark it, push it to the stack).
    //   1.3) obj is in another region, (do nothing, because we already record the ref in remset).
    // 2) if cur is a region object ==>
    //   2.1) obj is a heap object, (add it to heap remset).
    //   2.2) obj is in current region,
    //     2.2.1) if obj is forwarded, (update the reference).
    //     2.2.2) if obj is not forwareded, (move and mark it, update the reference,
    //                                       push it to the stack).
    //   2.3) obj is in another region, (do nothing, the reason is the same as 1.3).

    // 1 cur is a heap object.
    if (dest_region_id == 0) {
      // 1.1 a
      if (!Universe::heap()->is_in_region((const void*)obj)) {
        return;
      }

      region_id_t obj_region_id = obj->get_region_id();
      thread_id_t obj_thread_id = obj->get_thread_id();

      // 1.1 b
      if (obj_region_id == 0) return;

      // 1.2
      if (obj_region_id == ret_region_id
          /*&& obj_thread_id == thread_id Harry*/) {

        // Leave it in the region as a hole.
        Region* region = RegionTable::get_region(ret_region_id, obj_thread_id);
        if (region->mark_and_push_hole(obj)) { // in this we make sure no double marking for obj 
          // Set the region id as heap.
          obj->set_region_id(0);
          stack->push(obj);
        }
      }
      
      // 1.3
      return;
    }

    // 2 cur is in some region.
    // 2.1
    bool is_obj_heap = !Universe::heap()->is_in_region((const void*)obj);
    region_id_t obj_region_id = 0;
    thread_id_t obj_thread_id = 0;
    if (!is_obj_heap) {
      obj_region_id = obj->get_region_id();
      obj_thread_id = obj->get_thread_id();
      if (obj_region_id == 0) is_obj_heap = true;
    }
    if (is_obj_heap) {
      Region* dest_region = RegionTable::get_region(dest_region_id, thread_id);
      RemSet* heap_remset = dest_region->heap_rem_set();
      heap_remset->put(obj, field, cur, dest_region_id);
      return;
    }

    // 2.2
    if (thread_id == obj_thread_id
        && ret_region_id == obj_region_id) {
      oop new_obj = NULL;
      // 2.2.1
      if (obj->is_forwarded()) {
        new_obj = obj->forwardee();
        assert(new_obj->get_region_id() <= dest_region_id,
            "The forwarded object should be already moved to a higher region!");
        assert(new_obj->get_thread_id() == thread_id,
            "The region id should keep the same!");
      } else {
        new_obj = move_and_mark(obj, dest_region_id, thread_id);
        stack->push(new_obj);
      }
      assert(Universe::heap()->is_in_region(field),
          err_msg("try to write something outside the region: %p",field));
      oopDesc::store_heap_oop(field, new_obj);
    }

    // 2.3
    return;

  }

  inline static void analyze_oop(oop cur, Stack<oop, mtThread>* stack,
      region_id_t ret_region_id,
      region_id_t dest_region_id,
      thread_id_t thread_id) {
    assert(!oopDesc::is_null(cur), "The oop cur in analyze_oop cannot be NULL!");
    Klass* k = cur->klass();
    if (k->oop_is_instance()) {
      if (DebugRemSetRetire) tty->print_cr("start the instance case!");
      // Instance type.
      // Process class loader for this class.
      oop oop_cl = k->klass_holder();
      if (Universe::heap()->is_in_region((const void*)oop_cl)) {
        if (DebugRemSetRetire) tty->print_cr("ATTENTION: class holder is processed!");
        analyze_oop(oop_cl, stack, ret_region_id, dest_region_id, thread_id);
      }
      // Process fields in instance.
      InstanceKlass* ik = (InstanceKlass*)k;
      OopMapBlock* map = ik->start_of_nonstatic_oop_maps();
      OopMapBlock* const end_map = map + ik->nonstatic_oop_map_count();
      while (map < end_map) {
        oop* p = (oop*)(cur->obj_field_addr<oop>(map->offset()));
        oop* const end = p + map->count();
        while (p < end) {
          process_field(p, cur, stack, ret_region_id, dest_region_id, thread_id);
          ++ p;
        }
        ++ map;
      }
      if (DebugRemSetRetire) tty->print_cr("finish the instance case!");
      if (k->oop_is_instanceMirror()) {
        assert(false, "logic for instance mirror is incomplete!");
        if (DebugRemSetRetire) tty->print_cr("start the mirror case!");
        // Iterate all the static fields for this java class.
        // Mimic InstanceMirrorKlass_OOP_ITERATE
        oop* p = (oop*)InstanceMirrorKlass::start_of_static_fields(cur);
        int field_count = java_lang_Class::static_oop_field_count(cur);
        oop* const end = p + field_count;
        while (p < end) {
          process_field(p, cur, stack, ret_region_id, dest_region_id, thread_id);
          ++ p;
        }
        if (DebugRemSetRetire) tty->print_cr("end the mirror case!");
      } else if (k->oop_is_instanceRef()) {
        if (DebugRemSetRetire) tty->print_cr("start the ref case!");
        // Iterate referent/discovered/next fields.
        // Mimic InstanceRefKlass::specialized_oop_push_contents
        // XXX: We treat soft/weak/phantom refs all as strong refs.
        // Some useful links: 
        // http://www.docjar.com/html/api/java/lang/ref/Reference.java.html
        // http://www.docjar.com/html/api/java/lang/ref/ReferenceQueue.java.html
        // Treat referent as normal oop.
        oop* referent_addr = (oop*)java_lang_ref_Reference::referent_addr(cur);
        process_field(referent_addr, cur, stack, ret_region_id, dest_region_id, thread_id);
        // Treat discovered as normal oop if inactive.
        oop* next_addr = (oop*)java_lang_ref_Reference::next_addr(cur);
        oop next_oop = oopDesc::load_heap_oop(next_addr);
        if (!oopDesc::is_null(next_oop)) { // cur is not active.
          oop* discovered_addr = (oop*)java_lang_ref_Reference::discovered_addr(cur);
          process_field(discovered_addr, cur, stack, ret_region_id, dest_region_id, thread_id);
          process_field(next_addr, cur, stack, ret_region_id, dest_region_id, thread_id);
        }
        if (DebugRemSetRetire) tty->print_cr("end the ref case!");
      } else if (k->oop_is_instanceClassLoader()) {
        assert(false, "logic for instance class loader is incomplete!");
        if (DebugRemSetRetire) tty->print_cr("start the cl case!");
        // The special logic for InstanceClassLoaderKlass.
        ClassLoaderData* const loader_data = java_lang_ClassLoader::loader_data(cur);
        if (loader_data != NULL) {
          assert(false, "we may miss logic for InstanceClassLoader!");
          // XXX: I did not put implementation here.
          //if (loader_data->claim()) {
          //  
          //}
        }
        if (DebugRemSetRetire) tty->print_cr("end the cl case!");
      }
    } else if (k->oop_is_objArray()) {
      if (DebugRemSetRetire) tty->print_cr("start the array case!");
      // Object array type.
      oop* p = (oop*)(objArrayOop(cur))->base();
      oop* const end = p + (objArrayOop(cur))->length();
      while (p < end) {
        process_field(p, cur, stack, ret_region_id, dest_region_id, thread_id);
        ++ p;
      }
      if (DebugRemSetRetire) tty->print_cr("end the array case!");
    }
  }

  // This method process all the reachable objects from object obj.
  // obj should be never visited before.
  inline static oop process_all_reachable_obj_in_region(oop obj,
      region_id_t ret_region_id,
      region_id_t dest_region_id,
      thread_id_t thread_id) {
    assert(!obj->is_forwarded(), "this method should not be invoked by a forwarded object!");
    if (DebugRemSetRetire) {
      tty->print_cr("!!!!!!==> we are computing the closure in the region!!!!");
    }
    // For each objects, we need to iterate all the (reference) fields.
    // The hint is from oop.psgc.inline.hpp, oop_push_contents.
    // We have different implementations for different type of object:
    // 1) InstanceKlass
    //  1.1) Regular
    //  1.2) InstanceClassLoaderKlass ==> Regular
    //  1.3) InstanceMirrorKlass ==> Regular + extra logic
    //  1.4) InstanceRefKlass ==> specialized_oop_push_contents
    // 2) ArrayKlass
    //  2.1) ObjArrayKlass ==> ObjArrayKlass oop_push_contents
    //  2.2) TypeArrayKlass ==> should not reach here, since no reference here

    oop new_obj = obj;
    if (dest_region_id == 0) {
      assert(obj->get_region_id() != 0, "this method should not revisit heap object!");
     
      Region* region = RegionTable::get_region(ret_region_id, thread_id);
      if (region->mark_and_push_hole(obj)) { // no double marking 
        obj->set_region_id(0);
      }

    } else {
      new_obj = move_and_mark(obj, dest_region_id, thread_id);
    }
    Stack<oop, mtThread> stack;
    stack.push(new_obj);
    while (!stack.is_empty()) {
      // Each time, pop one oop from stack.
      // Iterate the fields of this object, and push oops into stack.
      oop cur = stack.pop();
      analyze_oop(cur, &stack, ret_region_id, dest_region_id, thread_id);
    }
    return new_obj;
  }

  // Full GC support. Traverse all the heap objects in/from regions, and push the contents to the manager.
  // Just directly declare the concrete type of parameter, for performance purpose.
  static void traverse_heap_objects_from_region(ParCompactionManager* compaction_manager) {
    for (int i = 1; i < 255; ++i) {
      for (int j = 1; j < 255; ++j) {
        Region* cur = RegionTable::fetch_region(j,i);
        if (cur == NULL) {
          //break;
          continue;
        }

        // Part 1. Process the references from heap rem set.
        RemSet* remset = cur->get_heap_rem_set();
        int valid_entry = 0;
        int total_entry = 0;
        if (remset != NULL) {
          total_entry = remset->size();
          EntryIterator* iter = remset->get_entry_iterator();
          while (iter->has_next()) {
            Entry* entry = iter->next();
            assert(entry != NULL, "must be");
            // This should be fine, since the source is from region, no race on them.
            if (!entry->is_valid()) {
              PRINT_YAK_DEBUG(DebugYakFullGC,
                  "Full GC Yac phase 1 (heap rem set): invalid escaping obj %p",
                  entry->get_esc_obj());
              continue;
            }
            oop obj = entry->get_esc_obj();
            PRINT_YAK_DEBUG(DebugYakFullGC, "Full GC Yac phase 1 (heap rem set): process heap obj %p", obj);
            // Skip all the heap objects physically in the region.
            if (Universe::heap()->is_in_reserved(obj)
                && !Universe::heap()->is_in_region(obj)) {
              // MT-safe: protected by mark_obj(obj).
              // If mark_obj(obj) returns true, this thread wins the ownership of obj.
              if (PSParallelCompact::mark_bitmap()->is_unmarked(obj)
                  && PSParallelCompact::mark_obj(obj)) {
                ++ valid_entry;
                obj->follow_contents(compaction_manager);
                PRINT_YAK_DEBUG(DebugYakFullGC, "Full GC Yac phase 1 (heap rem set): follow contents of %p, %s",
                    obj, obj->klass()->internal_name());
              } else {
                PRINT_YAK_DEBUG(DebugYakFullGC, "Full GC Yac phase 1 (heap rem set): %p is already processed, %s",
                    obj, obj->klass()->internal_name());
                ++ valid_entry;
              }
            }
          }
          delete iter;
          PRINT_YAK_DEBUG(DebugYakFullGC, "Full GC Yac phase 1 (heap rem set): %d valid entries, %d entries",
              valid_entry, total_entry);
        }

        // Part 2. Process all the holes in the regions.
        Stack<oop, mtThread>* holes = cur->hole_stack();
        StackIterator<oop, mtThread> iter_holes(*holes);
        while (!iter_holes.is_empty()) {
          oop obj = iter_holes.next();
          //tty->print_cr("process hole in Full GC at %p", obj);
          assert (obj->is_oop(), err_msg("hole %p in Full GC is not oop",obj));
          assert (Universe::heap()->is_in_region(obj), "should be");

          // XXX: Not sure if the bitmap covers our region or not... so directly call klass method.
          // No need to check the mark, since we only follows the contents.
          obj->klass()->oop_follow_contents(compaction_manager, obj);
          PRINT_YAK_DEBUG(DebugYakFullGC, "Full GC Yac phase 1 (holes): follow contents of hole %p, %s",
              obj, obj->klass()->internal_name());
        }
        PRINT_YAK_DEBUG(DebugYakFullGC, "Full GC Yac phase 1 (holes): %d-%d, %d holes", j, i, cur->hole_stack()->size());
      }
    }
  }

  // Minor GC support. Traverse all the heap objects in/from regions, and push the contents to the manager.
  // Just directly declare the concrete type of parameter, for performance purpose.
  static void traverse_heap_objects_from_region(PSPromotionManager* pm) {
    for (int i = 1; i < 255; ++i) {
      for (int j = 1; j < 255; ++j) {
        Region* cur = RegionTable::fetch_region(j, i);
        if (cur == NULL) {
          //break;
          continue;
        }

        // Part 1: Process the references from heap rem set.
        RemSet* remset = cur->get_heap_rem_set();
        int valid_entry = 0;
        int total_entry = 0;
        if (remset != NULL) {
          tty->print_cr("Minor GC region %d,%d heap_remset size %d", j,i, remset->size());
          total_entry = remset->size();
          EntryIterator* iter = remset->get_entry_iterator();
          while (iter->has_next()) {
            Entry* entry = iter->next();
            // This should be fine, since the source is from region, no race on them.
            if (!entry->is_valid()) {
              continue;
            }
            oop obj = entry->get_esc_obj();
            if (Universe::heap()->is_in_reserved(obj)
                && PSScavenge::is_obj_in_young(obj)
                && !Universe::heap()->is_in_region(obj)) {
              ++ valid_entry;
              oop new_obj = NULL;
              // Copy the objects to survivor space or tenured space.
              // Also follow the contents.
              // MT-safe: protected by copy_to_survivor_space.
              new_obj = pm->copy_to_survivor_space<false>(obj);
              PRINT_YAK_DEBUG(DebugYakMinorGC,
                  "Minor GC Yac phase 1 (heap rem set) - copy_to_survivor_space: %p ==> %p, %s",
                  obj, new_obj, obj->klass()->internal_name());
              assert(obj->is_forwarded(), "must be");
              assert(new_obj == obj->forwardee(), "must be the same forward info");
            }
          }
          delete iter;
          PRINT_YAK_DEBUG(DebugYakMinorGC, "Minor GC Yac phase 1 (heap rem set): %d valid entries, %d entries", valid_entry, total_entry);
        }

        // Part 2: Process all the holes in the regions.
        Stack<oop, mtThread>* holes = cur->hole_stack();
        //tty->print_cr("Minor GC region %d,%d hole_stack size %d", j,i, holes->size());
        StackIterator<oop, mtThread> iter_holes(*holes);
        while (!iter_holes.is_empty()) {
          oop obj = iter_holes.next();
          assert(obj->is_oop(), err_msg("hole %p in minor GC is not oop", obj));
          assert(Universe::heap()->is_in_region(obj), err_msg("hole %p is not in the region!", obj));
          // Only follow the contents and update the references.
          PRINT_YAK_DEBUG(DebugYakMinorGC,
              "Minor GC Yac phase 1 (holes) - push_contents: %p, %s",
              obj, obj->klass()->internal_name());
          obj->push_contents(pm);
        }
        PRINT_YAK_DEBUG(DebugYakMinorGC, "Minor GC Yac phase 1 (holes): %d-%d, %d holes", j, i, cur->hole_stack()->size());
      }
    }
  }

};

#endif // SHARE_VM_ASRMM_ASRMMHELPER_HPP

// ASRMM code end.
