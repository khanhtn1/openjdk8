/*
 * page.hpp
 *
 * 
 */

#ifndef SRC_SHARE_VM_ASRMM_PAGE_HPP
#define SRC_SHARE_VM_ASRMM_PAGE_HPP

//#include "utilities/debug.hpp"
//#include "utilities/globalDefinitions.hpp"

#include "oops/oop.hpp"
#include "memory/allocation.inline.hpp"
#include "memory/allocation.hpp"
#include "bitArray.hpp"
#include "bitArray.inline.hpp"


class Region;
class RemSet;
class Page;



/**
 * Added by Harry Xu on 4/14/16
 * Holes are now detected by summary instead of bitmap
*/
class HoleSummary: public CHeapObj<mtThread>{

private:
  // address of the array
  // each element is a pair of start and end addresses
  HeapWord** _summary;
  // number of holes 
  size_t _num_holes;
  // capacity of the array
  size_t _capacity;
  // total object sizes of the holes
  long _total_size;
  // cursor used for allocation
  HeapWord** _cursor;

  static size_t Hole_Distance_Threshold;

public: 
  HoleSummary(){
    _capacity = 100;
    _summary = NULL;
    _num_holes = 0;
    _total_size = 0;
    _cursor = NULL;
  }
  void reset_cursor(){
    if(_num_holes != 0)
      _cursor = _summary;
    else
      _cursor = NULL;
    tty->print_cr("CURSOR reset to %p, the num holes: %d", _cursor, _num_holes);
  }

  void reset(){
    free(_summary);
    _num_holes = 0;
    _capacity = 100;
    size_t s = sizeof(HeapWord*) * _capacity;
    _summary = (HeapWord**)malloc(s);
    memset(_summary, 0, s);
    _cursor = NULL;
  } 

  void insert(HeapWord* addr, size_t size); /*{
    if(_summary == NULL){
      size_t s = sizeof(HeapWord*) * _capacity;
      _summary = (HeapWord**)malloc(s);
      memset(_summary, 0, s);
    }

    assert (addr != NULL, "sanity");

    _total_size += size;
    if(_num_holes == 0){
      *_summary = addr;
      *(_summary+1) = addr + size;
      _num_holes =1;
      _cursor = _summary;
      return;
    }      
    size_t half = _num_holes -1;

    HeapWord** left = _summary;
    HeapWord** right = _summary + (half << 1); // last valid element -- it's 0-based

    // FastPath and duplicate removal
    if (addr == *left || addr == *right) {
      tty->print_cr("duplicated at %s", addr == *left ? "head" : "tail");
      if (addr == *left) assert (pointer_delta(*(left+1), *left) ==  size, "must be");
      else assert (pointer_delta(*(right+1), *right) ==  size, "must be");
      return;
    }

    //find the pos
    HeapWord** pos= NULL;
    bool found_pos = false;
    if (addr < *left) {
      if ((addr +size) == *left){ // merge opportunity
        *left= addr;
        return;
      }
      found_pos = true;
      pos = left;
    }
 
    if (!found_pos) {
      if (addr > *right) {
        if (*(right+1) == addr) { //merge opportunity
          *(right+1) = addr + size;
          return;
        }
        found_pos = true;
        pos = right+2;
      }
    }
    
    if (!found_pos) {
      while(left < right) {
        half = (right - left) >> 1 ;
        HeapWord** midpoint = left +  (half << 1); // find

        if(addr > *midpoint) {
          left = midpoint + 2; //advance to next valid element       
        } else if(addr < *midpoint){
          right = midpoint -2; // back to prev valid element
        }else{
          assert(false, "A hole with the same address already exists!");
          return;
        }
      }
      
      assert(left < _summary + _capacity, "buffer overflow 1!");
      
      if (left >right) {
        left = right;
        //assert (false, "go too far");
      }
      
      //find the pos
      if(addr < *left){
        pos = left ;
      } else if (addr == *left){
        assert(false, "A hole with the same address left already exists!");
      } else if (addr == *right) {
        assert(false, "A hole with the same address right already exists!");
      } else {
        pos = left+2;
      } 
      found_pos = true;
    }
    
    assert (found_pos, "sanity");

    //Here we try to see if we can merge holes
    if(pos != _summary){
      HeapWord* prev_end = *(pos - 1);
      size_t diff = (size_t)(addr - prev_end);
      if(diff <= Hole_Distance_Threshold){
        *(pos - 1) = addr + size;
        _total_size += diff;
        //tty->print_cr("merged1 %p, %p, %p, %p", *(pos - 2), prev_end, addr, addr+size);
        return;
      }
    }
    HeapWord** end_summary =  _summary + (_num_holes << 1);
    if(pos < end_summary){
      HeapWord* next_start = *(pos);
      size_t diff = (size_t)(next_start - (addr + size));
      if(diff <= Hole_Distance_Threshold){
        *pos = addr;
        _total_size += diff;
        //tty->print_cr("merged2 %p, %p, %p, %p", next_start, *(pos+1), addr, addr+size);
        return;
      }
    }
    
    //No merging opportunities are found
    //Let's insert a new entry
    if(pos < end_summary) {
      size_t num_bytes = pointer_delta((void*)end_summary, (void*)pos, 1);
      memmove((void*)(pos+2),  (const void*)pos, num_bytes);
    }
    assert(pos + 1 < _summary + _capacity, "buffer overflow!");
    assert (pos <= end_summary, "must be");
    
    *pos =  addr;
    *(pos+1) = addr + size;
    _num_holes ++;

    
    bool HOLE_SANITY_CHECK = true;
    if(HOLE_SANITY_CHECK){
      HeapWord *prev = NULL, *end = NULL;
      for(size_t i = 0; i < _num_holes; i++){
        assert (*(_summary + (i <<1)) != NULL, "sanity");
        assert (*(_summary + (i <<1) + 1) != NULL, "sanity");
        if(prev != NULL){
          // if(*(_summary + (i <<1)) < prev)
          //  tty->print_cr("wrong, i: %d, prev %p %p new %p %p", i, prev, *(_summary + ((i <<1) - 1)), *(_summary + (i <<1)),  *(_summary + ((i <<1)+1)));
          assert (*(_summary + (i <<1)) > prev && *(_summary + (i <<1)) > end , "not sorted!!!");
        }
        prev = *(_summary + (i <<1));
        end = *(_summary + (i <<1) + 1);
      }
    }
    
    if((_num_holes << 1) >= _capacity){
      size_t new_capacity = (_capacity << 1) * sizeof(HeapWord*);
      HeapWord **newarray = (HeapWord**)malloc(new_capacity);
      memset((void*)newarray, 0, new_capacity);
      memmove((void*)newarray, (const void*)_summary, sizeof(HeapWord*) * _capacity);
      free(_summary);
      _summary = newarray;
      _capacity = _capacity << 1;
    }
  }
  */
  
  void print();
  inline long get_total_hole_size() {return _total_size;}
  inline size_t get_num_holes() { return _num_holes; }

  // The function returns a new bump pointer, after considering the holes
  inline HeapWord* get_allocable_address(HeapWord* bp, size_t size){
    if(_num_holes == 0 || _cursor == NULL) return bp;
    HeapWord** boundary = _summary + (_num_holes <<1);
    assert(_cursor < boundary, "_cursor out of boundary!");
    //if(_cursor == NULL) {
    //  return bp; // no more holes in front of us
    //}
    HeapWord* end_addr = bp + size;
    if(end_addr < (HeapWord*)*_cursor){
      return bp; // the next hole is still far away
    } else{
      HeapWord* new_addr = *(_cursor + 1); // the end address of the first hole
      HeapWord** pos = _cursor + 2;
      while(pos < boundary){       
        end_addr = new_addr + size;
        //test if the end address plus the requested size reaches the
        //start address of the next hole
        if(end_addr < *pos){
          _cursor = pos;
          //assert(_cursor < boundary, "_cursor out of boundary!");
          return new_addr;
        }
        new_addr = *(pos + 1);
        pos += 2;
      }
      //we are coming to the end
      _cursor = NULL;
      return new_addr;
    }
  }
};

class Page: public CHeapObj<mtThread> {
  friend class VMStructs;
  
private:
  
  HeapWord* _top; // address after last allocation
  HeapWord* _end; // end address (exclusive)
  
  bool _has_holes;
  size_t _dead_space_size;
  // support complex allocation policy, if bitmap[bit]=true means that space is occupied, we cannot allocate another obj in it
  HoleSummary* _hs;
  
  HeapWord _holes; //this means an array of size bitmap_size_in_words() // I can't do HeapWord[bitmap_size_in_words] _holes;
  
  static long Page_Full_Threshold;
  
public:
  static bool BITARRAY_ENABLED;

  //helper static
  static const size_t bitmap_size_in_words() {
    return PageSize >> (LogBitsPerWord + LogBitArrayWordsPerBit);
  }
  
  inline HoleSummary* get_hole_summary(){ return _hs; }
  //factory method
  static Page* brand_new_page(size_t pageSize = PageSize);

  // from an arbitrary address, trace back the page that contains it
  static Page* get_page(void* ptr);
  
  Page();
  ~Page();
  
  inline void clear_holes(){ 
    if(UseYakHoleSummary){
      _hs->reset(); 
    }
  }
 
 HeapWord* allocate(size_t size) {
  assert ((void*)_top > (void*)this, "check");
  HeapWord* obj = _top; 
  if(UseYakHoleSummary && has_holes()){
    obj = get_hole_summary()->get_allocable_address(obj, size);
  }
  if (pointer_delta(_end, obj) > size) {
    // This addition is safe because we know that top is
    // at least size below end, so the add can't wrap.
    HeapWord* new_top = obj + size;
    
    set_top(new_top);
    
    if (DebugMarkNativeOop)
      tty->print_cr("allocating oop %p of size %d with page [%p:%p:%p)=%d", obj,
          size, this, _top, _end, pointer_delta(_end, _top));

    return obj;
  }

  // not enough space, will be handled by the caller Region::allocate(): new Page will be given
  return NULL;
}

 
  // Apr 21 2016 intialization of hs should be moved to get_hole_summary instead, i.e., lazy initialization
  void init_hs() {
    if(UseYakHoleSummary){
      _hs = new HoleSummary();
    }
  }

  //It depends on the total size of the hole objects
  bool is_reusable(){
    if(UseYakHoleSummary){
      return _hs->get_total_hole_size() < Page_Full_Threshold;
    } else{
      return false;
    }
  }
  
  void initialize();

  HeapWord* top() {
    return _top;
  }

  HeapWord* end() {
    return _end;
  }

  void set_top(HeapWord* top) {
    _top = top;
  }

  void set_end(HeapWord* end) {
    _end = end;
  }

  size_t used() {
    return pointer_delta(top(), (HeapWord*) this);
  }

  size_t used_bytes() {
    return pointer_delta(top(), (HeapWord*) this, 1);
  }

  size_t free() {
    return pointer_delta(end(), top());
  }

  size_t free_size() {
    return (size_t)PageSize - _dead_space_size; 
  }

  size_t dead_size() {
    return _dead_space_size;
  }

 

  // reset the page for reuse later
  // clear the bitmap if applicable
  void reset();

  bool mark_hole(oop obj, size_t obj_size);
  void clear_hole(oop obj, size_t obj_size);

  BitArray* get_bit_array();

  HeapWord* get_holes_addr() {
    return &_holes;
  }

  void set_has_holes(bool val) {
    _has_holes = val;
  }

  bool has_holes() {
    return _has_holes;
  }

};

#endif /* SRC_SHARE_VM_ASRMM_PAGE_HPP_ */
