/* This is the thread-local storage manager that is in charge of pages
 * Each thread has 1 storage manager only
 * TODO:
 * - Class of pages?
 * 
 */
#ifndef SRC_SHARE_VM_ASRMM_STORAGEMANAGER_HPP
#define SRC_SHARE_VM_ASRMM_STORAGEMANAGER_HPP


#include "page.hpp"
#include "linkedArray.hpp"
#include "utilities/stack.hpp"
#include "utilities/stack.inline.hpp"

class LargePageDesc : public CHeapObj<mtThread> {
  
public:
  HeapWord*      _addr;
  size_t _size;
  LargePageDesc* _next;
  
  LargePageDesc();
  LargePageDesc(HeapWord* page_addr, size_t page_size);
  ~LargePageDesc();

  void set_next(LargePageDesc* next) {
    _next = next;
  }

  LargePageDesc* get_next() {
    return _next;
  }

  HeapWord* get_addr() {
    return _addr;
  }
  
  size_t get_size() {
    return _size;
  } 

};

class StorageManager : public CHeapObj<mtThread> {
  friend class VMStructs;

private:

  Stack<Page*, mtThread> _free_pages; 
  Stack<Page*, mtThread> _holes_pages; 
  Stack<Page*, mtThread> _useless_pages; // pages whose free size is less than UsefulPageThreshold
  
  size_t _page_count;

  struct LargePageDesc* _large_pages;
  struct LargePageDesc* _large_holes;

  size_t _large_page_count;

  // Khanh Feb 27 2016: use the library stack instead of my own implementation :D
  //LinkedArray<Page*>* _free_pages;
  //LinkedArray<Page*>* _holes_pages;

  Thread* _thread; // thread that owns this manager :Dec 4 I think we can throw this away, use Thread::current() instead later

  long _free_space;
  
  long _total_allocated;
  
  bool _free_called;

  
public:
  StorageManager();
  StorageManager(Thread* thread);
  ~StorageManager();
  //copy constructor
  StorageManager(const StorageManager& other);

  void initialize(Thread* t);

  void add_space_stat(long* free_space, long* total_space){
    if(!_free_called){
      _free_called = true;
      *free_space += _free_space;
      *total_space += _total_allocated;
    } 
  }


  void restore_useless_pages();
  // push on stack
  void store(Page* aPage);

  void store_large_page(HeapWord* large_obj, size_t size, bool large_hole);

  // pop the stack
  Page* give();

  HeapWord* give_large_page(size_t size_requested);

  // if this has any page under its control
  bool active();

  bool has_page();

  bool has_large_page();

  void break_large_page(LargePageDesc* large_page);

  void break_one_large_page();

  void break_all_large_pages();

  // free all pages back to the OS
  void freeAll();


};

#endif // SRC_SHARE_VM_ASRMM_STORAGEMANAGER_HPP
