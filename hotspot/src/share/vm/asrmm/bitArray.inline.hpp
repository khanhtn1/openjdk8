// bitArray.inline.hpp



#ifndef SHARE_VM_ASRMM_BITARRAY_INLINE_HPP
#define SHARE_VM_ASRMM_BITARRAY_INLINE_HPP

#include "bitArray.hpp"

inline void BitArray::verify_index(idx_t index) const {
  assert(index < (size_t)PageSize, "BitArray index out of bounds");
}

inline void BitArray::verify_range(idx_t beg_index, idx_t end_index) const {
  assert(beg_index <= end_index, "BitArray range error");
  // Note that [0,0) and [size,size) are both valid ranges.
  if (end_index != (size_t)PageSize) verify_index(end_index);
}


inline void BitArray::set_bit(idx_t bit) {
  verify_index(bit);
  *word_addr(bit) |= bit_mask(bit);
}

inline void BitArray::clear_bit(idx_t bit) {
  verify_index(bit);
  *word_addr(bit) &= ~bit_mask(bit);
}

inline void BitArray::set_range_of_words(idx_t beg, idx_t end) {
  bm_word_t* mmap = map();
  for (idx_t i = beg; i < end; ++i) mmap[i] = ~(uintptr_t)0;
}

inline void BitArray::clear_range_of_words(idx_t beg, idx_t end) {
  bm_word_t* mmap = map();
  for (idx_t i = beg; i < end; ++i) mmap[i] = 0;
}

inline void BitArray::clear() {
  clear_range_of_words(0, size_in_words());
}

inline BitArray::idx_t
BitArray::get_next_one_offset_inline(idx_t l_offset, idx_t r_offset) const {
  assert(l_offset <= size(), "BitArray index out of bounds");
  assert(r_offset <= size(), "BitArray index out of bounds");
  assert(l_offset <= r_offset, "l_offset > r_offset ?");

  if (l_offset == r_offset) {
    return l_offset;
  }

  l_offset = align_size_down(l_offset, BitArrayWordsPerBit);
  idx_t   index = word_index(l_offset);
  idx_t r_index = word_index(r_offset-1) + 1;
  idx_t res_offset = l_offset;

  // check bits including and to the _left_ of offset's position
  idx_t pos = bit_in_word(res_offset);
  bm_word_t res = map(index) >> pos;
  if (res != (uintptr_t)NoBits) {
    // find the position of the 1-bit
    for (; !(res & 1); ) {
      res = res >> 1;
      res_offset+= BitArrayWordsPerBit;
    }

#ifdef ASSERT
    // In the following assert, if r_offset is not bitamp word aligned,
    // checking that res_offset is strictly less than r_offset is too
    // strong and will trip the assert.
    //
    // Consider the case where l_offset is bit 15 and r_offset is bit 17
    // of the same map word, and where bits [15:16:17:18] == [00:00:00:01].
    // All the bits in the range [l_offset:r_offset) are 0.
    // The loop that calculates res_offset, above, would yield the offset
    // of bit 18 because it's in the same map word as l_offset and there
    // is a set bit in that map word above l_offset (i.e. res != NoBits).
    //
    // In this case, however, we can assert is that res_offset is strictly
    // less than size() since we know that there is at least one set bit
    // at an offset above, but in the same map word as, r_offset.
    // Otherwise, if r_offset is word aligned then it will not be in the
    // same map word as l_offset (unless it equals l_offset). So either
    // there won't be a set bit between l_offset and the end of it's map
    // word (i.e. res == NoBits), or res_offset will be less than r_offset.

    idx_t limit = is_word_aligned(r_offset) ? r_offset : size();
    assert(res_offset >= l_offset && res_offset < limit, "just checking");
#endif // ASSERT
    return MIN2(res_offset, r_offset);
  }
  // skip over all word length 0-bit runs
  for (index++; index < r_index; index++) {
    res = map(index);
    if (res != (uintptr_t)NoBits) {
      // found a 1, return the offset
      for (res_offset = bit_index(index); !(res & 1); ) {
        res = res >> 1;
	res_offset+= BitArrayWordsPerBit;
      }
      assert(res & 1, "tautology; see loop condition");
      assert(res_offset >= l_offset, "just checking");
      return MIN2(res_offset, r_offset);
    }
  }
  return r_offset;
}


inline BitArray::idx_t
BitArray::get_next_zero_offset_inline(idx_t l_offset, idx_t r_offset) const {
  assert(l_offset <= size(), "BitMap index out of bounds");
  assert(r_offset <= size(), "BitMap index out of bounds");
  assert(l_offset <= r_offset, "l_offset > r_offset ?");

  if (l_offset == r_offset) {
    return l_offset;
  }

  l_offset = align_size_down(l_offset, BitArrayWordsPerBit);
  idx_t   index = word_index(l_offset);
  idx_t r_index = word_index(r_offset-1) + 1;
  idx_t res_offset = l_offset;

  // check bits including and to the _left_ of offset's position
  idx_t pos = bit_in_word(res_offset);
  bm_word_t res = (map(index) >> pos) | left_n_bits((int)pos);

  if (res != (uintptr_t)AllBits) {
    // find the position of the 0-bit
    for (; res & 1; ) {
      res = res >> 1;
      res_offset+= BitArrayWordsPerBit;
    }
    assert(res_offset >= l_offset, "just checking");
    return MIN2(res_offset, r_offset);
  }
  // skip over all word length 1-bit runs
  for (index++; index < r_index; index++) {
    res = map(index);
    if (res != (uintptr_t)AllBits) {
      // found a 0, return the offset
      for (res_offset = bit_index(index); res & 1;
           ) {
        res = res >> 1;
	res_offset+= BitArrayWordsPerBit;
      }
      assert(!(res & 1), "tautology; see loop condition");
      assert(res_offset >= l_offset, "just checking");
      return MIN2(res_offset, r_offset);
    }
  }
  return r_offset;
}


// Returns a bit mask for a range of bits [beg, end) within a single word.  Each
// bit in the mask is 0 if the bit is in the range, 1 if not in the range.  The
// returned mask can be used directly to clear the range, or inverted to set the
// range.  Note:  end must not be 0.
inline BitArray::bm_word_t
BitArray::inverted_bit_mask_for_range(idx_t beg, idx_t end) const {
  assert(end != 0, "does not work when end == 0");
  assert(beg == end || word_index(beg) == word_index(end - 1),
         err_msg("must be a single-word range beg=%d, end=%d, word(beg)=%d,word(end)=%d",beg,end,word_index(beg),word_index(end-1) ));
  bm_word_t mask = bit_mask(beg) - 1;   // low (right) bits
  if (bit_in_word(end) != 0) {
    mask |= ~(bit_mask(end) - 1);       // high (left) bits
  }
  return mask;
}

inline void BitArray::set_large_range_of_words(idx_t beg, idx_t end) {
  memset(map() + beg, ~(unsigned char)0, (end - beg) * sizeof(uintptr_t));
}

inline void BitArray::clear_large_range_of_words(idx_t beg, idx_t end) {
  memset(map() + beg, 0, (end - beg) * sizeof(uintptr_t));
}

inline BitArray::idx_t BitArray::word_index_round_up(idx_t bit) const {
  idx_t bit_rounded_up = bit + ((BitsPerWord - 1)<<LogBitArrayWordsPerBit);
  // Check for integer arithmetic overflow.
  return bit_rounded_up > bit ? word_index(bit_rounded_up) : size_in_words();
}

inline BitArray::idx_t BitArray::get_next_one_offset(idx_t l_offset,
						 idx_t r_offset) const {
  return get_next_one_offset_inline(l_offset, r_offset);
}

inline BitArray::idx_t BitArray::get_next_zero_offset(idx_t l_offset,
						  idx_t r_offset) const {
  return get_next_zero_offset_inline(l_offset, r_offset);
}


#endif
