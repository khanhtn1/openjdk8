/* FlexibleArray, modified based on utilities/growableArray.hpp
 */

#ifndef SHARE_VM_ASRMM_FLEXIBLEARRAY_HPP
#define SHARE_VM_ASRMM_FLEXIBLEARRAY_HPP

#include "utilities/debug.hpp"
#include "utilities/globalDefinitions.hpp"
#include "memory/allocation.hpp"
#include "memory/allocation.inline.hpp"

#define DEFAULT_CAPACITY 10

// A growable array.
template<class E> class FlexibleArray : public ResourceObj {
  friend class VMStructs;

private:
  int    _len;          // current length
  int    _max;          // maximum length
  E*     _data;         // data array
  
  void grow(int j) {
    // grow the array by doubling its size (amortized growth)
    int old_max = _max;
    if (_max == 0) _max = 1; // prevent endless loop
    while (j >= _max) _max = _max*2;
    // j < _max
    size_t byte_size = sizeof(E) * (size_t) _max;

    E* newData = (E*) malloc(byte_size);
    int i = 0;
    for (     ; i < _len; i++) ::new ((void*)&newData[i]) E(_data[i]);
    for (     ; i < _max; i++) ::new ((void*)&newData[i]) E();
    for (i = 0; i < old_max; i++) _data[i].~E();
    if (_data != NULL) {
      free(_data);
    }
    _data = newData;
  }


  void  clear_and_deallocate() {
    clear();
    if (_data != NULL) {
      for (int i = 0; i < _max; i++) _data[i].~E();
      free(_data);
      _data = NULL;
    }
  }


  

public :

  FlexibleArray() {_len=0; _max=DEFAULT_CAPACITY; _data=NULL;}
  FlexibleArray(int initial_size) {_len=0; _max=initial_size;  _data=NULL;}
  ~FlexibleArray() {
    if (_data != NULL) {
      for (int i = 0; i < _len; i++) _data[i].~E();
      free(_data);
      _data = NULL;
    }
  }
  
  //we control when to create the _data
  //the space returned to us is zeroed
  void initialize() {
    size_t byte_size = sizeof(E) * (size_t) _max;
    _data = (E*) malloc(byte_size);
    memset(_data,0, byte_size);
  } 

  void  clear()                 { _len = 0; }
  int   length() const          { return _len; }
  int   max_length() const      { return _max; }
  void  trunc_to(int l)         { assert(l <= _len,"cannot increase length"); _len = l; }
  bool  is_empty() const        { return _len == 0; }
  bool  is_nonempty() const     { return _len != 0; }
  bool  is_full() const         { return _len == _max; }
  E* data_addr() const      { return _data; }
  
  void print() {
    tty->print("Flexible Array " INTPTR_FORMAT, this);
    tty->print(": length %ld (_max %ld) { ", _len, _max);
    for (int i = 0; i < _len; i++) tty->print(INTPTR_FORMAT " ", *(intptr_t*)&(_data[i]));
    tty->print_cr("}");
  }


  int append(const E& elem) {
    if (_len == _max) grow(_len);
    int idx = _len++;
    _data[idx] = elem;
    return idx;
  }

  bool append_if_missing(const E& elem) {
    // Returns TRUE if elem is added.
    bool missed = !contains(elem);
    if (missed) append(elem);
    return missed;
  }

  E at(int i) const {
    assert(0 <= i && i < _len, "illegal index");
    return _data[i];
  }

  E* adr_at(int i) const {
    assert(0 <= i && i < _len, "illegal index");
    return &_data[i];
  }

  E first() const {
    assert(_len > 0, "empty list");
    return _data[0];
  }


  E top() const {
    assert(_len > 0, "empty list");
    return _data[_len-1];
  }

  void push(const E& elem) {
    //print();
    //printf("fa=%p, len=%d \n",this,_len); 
    append(elem);
    //printf("fa=%p, len=%d\n",this,_len);
    //print();  
  }

  E pop() {
    assert(_len > 0, "empty list");
    return _data[--_len];
  }

    void put_at(int i, const E& elem) {
    assert(0 <= i && i < _len, "illegal index");
    _data[i] = elem;
    //_data[i] is not destroyed
  }

  E at_grow(int i, const E& fill = E()) {
    assert(0 <= i, "negative index");

    if (i >= _len) {
      if (i >= _max) grow(i);
      for (int j = _len; j <= i; j++)
        _data[j] = fill;
      _len = i+1;
    }
    return _data[i];
  }

  void at_put_grow(int i, const E& elem, const E& fill = E()) {
    assert(0 <= i, "negative index");  
    if (i >= _len) {
      if (i >= _max) grow(i);
      for (int j = _len; j <= i; j++)
        _data[j] = fill;
      _len = i+1;
    }
    _data[i]=elem;
  }

  bool contains(const E& elem) const {
    for (int i = 0; i < _len; i++) {
      if (_data[i] == elem) return true;
    }
    return false;
  }

  int  find(const E& elem) const {
    for (int i = 0; i < _len; i++) {
      if (_data[i] == elem) return i;
    }
    return -1;
  }

  int  find_from_end(const E& elem) const {
    for (int i = _len-1; i >= 0; i--) {
      if (_data[i] == elem) return i;
    }
    return -1;
  }

  int  find(void* token, bool f(void*, E)) const {
    for (int i = 0; i < _len; i++) {
      if (f(token, _data[i])) return i;
    }
    return -1;
  }

  int  find_at_end(void* token, bool f(void*, E)) const {
    // start at the end of the array
    for (int i = _len-1; i >= 0; i--) {
      if (f(token, _data[i])) return i;
    }
    return -1;
  }

  void remove(const E& elem) {
    for (int i = 0; i < _len; i++) {
      if (_data[i] == elem) {
        for (int j = i + 1; j < _len; j++) _data[j-1] = _data[j];
        _len--;
        return;
      }
    }
    ShouldNotReachHere();
  }

  // The order is preserved.
  void remove_at(int index) {
    assert(0 <= index && index < _len, "illegal index");
    for (int j = index + 1; j < _len; j++) _data[j-1] = _data[j];
    _len--;
  }

  // The order is changed.
  void delete_at(int index) {
    assert(0 <= index && index < _len, "illegal index");
    if (index < --_len) {
      // Replace removed element with last one.
      _data[index] = _data[_len];
    }
  }

  // inserts the given element before the element at index i
  void insert_before(const int idx, const E& elem) {
    if (_len == _max) grow(_len);
    for (int j = _len - 1; j >= idx; j--) {
      _data[j + 1] = _data[j];
    }
    _len++;
    _data[idx] = elem;
  }

  void appendAll(const FlexibleArray<E>* l) {
    for (int i = 0; i < l->_len; i++) {
      at_put_grow(_len, l->_data[i], 0);
    }
  }

};


// Global FlexibleArray methods (one instance in the library per each 'E' type).

/*template<class E> void FlexibleArray<E>::grow(int j) {
  // grow the array by doubling its size (amortized growth)
  int old_max = _max;
  if (_max == 0) _max = 1; // prevent endless loop
  while (j >= _max) _max = _max*2;
  // j < _max
  size_t byte_size = sizeof(E) * (size_t) _max;

  E* newData = (E*) malloc(byte_size);
  int i = 0;
  for (     ; i < _len; i++) ::new ((void*)&newData[i]) E(_data[i]);
  for (     ; i < _max; i++) ::new ((void*)&newData[i]) E();
  for (i = 0; i < old_max; i++) _data[i].~E();
  if (_data != NULL) {
    free(_data);
  }
  _data = newData;
}

*/


#endif // SHARE_VM_ASRMM_FLEXIBLEARRAY_HPP

