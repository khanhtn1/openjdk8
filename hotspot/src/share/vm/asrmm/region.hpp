/* Describe what is a region in our context
 *
 * Wrap around memContext, which is actually implementation of a page from native memory
 */

#ifndef SRC_SHARE_VM_ASRMM_REGION_HPP
#define SRC_SHARE_VM_ASRMM_REGION_HPP

#include "utilities/debug.hpp"
#include "utilities/stack.hpp"
#include "utilities/stack.inline.hpp"
#include "flexibleArray.hpp"
#include "remSet.hpp"
#include "page.hpp"
#include "runtime/mutex.hpp"
#include "runtime/timer.hpp"

class RegionTable;
class RegionManager;

// RegionSignature, should not call new or delete
// ValueObj is supposed to be used as parameter w/o &-passing 
// and can be returned as return value

class RegionSignature VALUE_OBJ_CLASS_SPEC {

 public:
  static RegionSignature heap_signature;
 private:
  region_id_t _region_id;
  thread_id_t _thread_id;

 public:

  RegionSignature(oop obj);

  RegionSignature(int combined_id) {
    _region_id = combined_id & 0xff;
    assert(_region_id >= 0, "region_id cant be negative");

    //TODO: adjust #bits depends on the size of region_id_t
    _thread_id = ((combined_id >> BitsPerByte) & 0xff);
    assert(_thread_id >= 0, "thread_id cant be negative");
  }

  RegionSignature(int region, int thread) {
    assert((region & ~0xff) == 0, "region is more than 1 byte capacity");
    assert((thread & ~0xff) == 0, "thread is more than 1 byte capacity");

    _region_id = region & 0xff;
    _thread_id = thread & 0xff;

    assert(_region_id >= 0, "region_id cant be negative");
    assert(_thread_id >= 0, "thread_id cant be negative");
  }

  RegionSignature(region_id_t region, thread_id_t thread) {
    _region_id = region;
    _thread_id = thread;

    assert(_region_id >= 0, "region_id cant be negative");
    assert(_thread_id >= 0, "thread_id cant be negative");
  }

  bool operator ==(RegionSignature sig) {
    return (get_region_id() == sig.get_region_id()
        && get_thread_id() == sig.get_thread_id());
  }

  region_id_t get_region_id() {
    return _region_id;
  }
  thread_id_t get_thread_id() {
    return _thread_id;
  }

};



class Region: public CHeapObj<mtThread> {
  friend class VMStructs;

 private:

  bool _is_retired;
  bool _has_page_storage;
  bool _has_large_page_storage;

  region_id_t _region_id; 
  thread_id_t _thread_id; 

  RegionManager* _manager;

  Region* _parent;

  Page* _current_page;

  Stack<Page*, mtThread> _pages;

  Stack<oop, mtThread> _large_objects_stack; // store large objects starts

  Mutex _lock_esc_stack;
  Stack<oop, mtThread> _esc_stack; // used to store escaping objects from read statements

  Stack<oop, mtThread> _hole_stack; // quick way to iterate all the holes, but really UGLY...

  RemSet* _rem_set; // records incoming references from either a region or the heap

  RemSet* _heap_rem_set; // records outgoing references from this region to the heap
  RemSet* _another_heap_rem_set; // To avoid the concurrency issue, we have another heap remset.
  
  int _num_holes;
  
  long _num_objects_created;

  elapsedTimer _timer;

 public:
  /*Here are a bunch of static fields for statistics purposes*/
  static long TOTAL_REFERENCES;
  static long REFERENCES_FROM_HEAP_TO_REGION;
  static long OTHER_REFERENCES;
  static long FREE_SPACE;
  static long DEALLOCATION_TIME;
  static long OBJECT_MOVE_TIME_IN_GC;
  static long OBJECTS_COPIED;
  static volatile long ALLOCATED_BYTES;

  //Constructor
  Region(int iter_id, int thread_id, Region* parent, Thread* thread,
         bool has_page, bool has_large_page);

  //Destructor
  ~Region();

  double get_deallocation_time(){
    return _timer.seconds();
  }

  //Copy constructor
  Region(const Region& other);

  RegionManager* get_manager() {return _manager;}
 
  /*This function returns the actual number of holes only once*/
  int get_num_holes(){
    if(_num_holes >0 ){ 
      int t = _num_holes; 
      _num_holes = 0;
      return t; 
    }
    else return 0;
  }

  long get_num_objects_created(){return _num_objects_created;}

 inline bool is_retired(){
    return _is_retired;
  }

  void set_is_retired();

 inline Page* current_page() {
    return _current_page;
  }
  Region* parent() {
    return _parent;
  }
 inline  int get_region_id() {
    return _region_id;
  }
 inline int get_thread_id() {
    return _thread_id;
  }

  // this will create RemSet Object if NULL, don't call unless you won't do NULL check
  inline RemSet* rem_set(){
    if(_rem_set == NULL) {
      _rem_set = new RemSet();
    }
    return _rem_set;
  }
  
  // Null check needed
  inline RemSet* get_rem_set() {
    return _rem_set;
  }

  Mutex* lock_esc_stack() {
    return &_lock_esc_stack;
  }

  Stack<oop, mtThread>* esc_stack();
  
  void esc_stack_push_thread_safe(oop esc_obj);

  Stack<oop, mtThread>* hole_stack() {
    return &_hole_stack;
  }

  void clear_metadata();


  bool mark_and_push_hole(oop obj);
  
  // this will create RemSet if NULL, don't call unless you won't do NULL check
  inline RemSet* heap_rem_set(){
    if(_heap_rem_set == NULL){
      _heap_rem_set = new RemSet();
    }
    return _heap_rem_set;
  }
  
  // Null check needed
  inline RemSet* get_heap_rem_set() { 
    return _heap_rem_set;
  }
  
  RemSet* another_heap_rem_set();
  void swap_heap_rem_sets();

  bool is_same_region(int region_id, int thread_id) {
    return (get_region_id() == region_id && get_thread_id() == thread_id);
  }

  //  void initialize(int iter_id, int thread_id,  Region* parent, Thread* thread);


  inline HeapWord* allocate(size_t size) {
  
    if(ReportYakReferenceStats){
      _num_objects_created ++;
      Atomic::add(size, &Region::ALLOCATED_BYTES);
    }
    
    if (size >= (size_t)LargeObjectThreshold) {
      if (DebugLargeObjectAllocationInRegion) tty->print_cr("Large Object Allocation of size %d", size);
      
      size_t size_round_up = align_size_up(size,(size_t)PageSize);
      
      if (DebugLargeObjectAllocationInRegion) tty->print_cr("old size = %d, round_up=%d",size, size_round_up);
      
      assert (size_round_up % PageSize ==0, "sanity check, must be 0");
      
      HeapWord* result = create_large_page(size_round_up);
      
      assert(result != NULL, err_msg("cannot get new large page of size %d", size_round_up));
      if (DebugLargeObjectAllocationInRegion) tty->print_cr(" large object is = %p",result);
      return result;
    }
    
    HeapWord* result = _current_page->allocate(size);
    //result is allowed to be NULL;
    
    while (result == NULL) { //no space in current page.
      _current_page = create_page(); 
      result = _current_page->allocate(size);
      // Feb 27
      // the page recycled might be already full of holes already
      // thus  this assert is no longer valid
      //   assert (result != NULL, " Null allocation is not Allowed!!!");
    }
  
    return result;
  }
  

  Page* create_page();
  
  HeapWord* create_large_page(size_t size);

  // Region object stay alive for the whole execution?
  void retire();

  // do a join between 2 regions
  Region* joins(Region* another_region);

  static Region* join_two_regions(Region* r1, Region* r2);

  static RegionSignature join_two_regions(RegionSignature left,
      RegionSignature right);

  static RegionSignature join_two_regions(oop left, oop right);

  // remset 

  // Used by write_barrier
  inline static void add_escaping_object(oop esc_obj, void* field, oop referent, region_id_t dest_region_id) {

    if (IgnoreRegionRemSet) return;
    // 1- get Region object
    Region* region = esc_obj->get_region();
    assert (region != NULL, "sanity check");

    // 2- add to remset:: this is thread-safe because the put is locked on each Bucket
    region->rem_set()->put(esc_obj, field, referent, dest_region_id);
  }

  // heap remset
 inline static void add_heap_object(oop heap_obj, void* region_obj_field, oop region_obj) {

    assert (region_obj != NULL, "region_obj must be not NULL"); // This should not happen
    assert (heap_obj != NULL, "heap_obj must be not NULL"); // This cannot be true ever

    // 1- get Region object
    Region* region = region_obj->get_region();
    assert (region != NULL, "sanity check");

    // 2- add to heapremset:: this is thread-safe because the put is locked on each Bucket
    region->heap_rem_set()->put(heap_obj, region_obj_field, region_obj, (region_id_t)0);
  }


  // esc_stack
  // Used by read_barrier
 static void add_escaping_object(oop esc_obj) {

    ShouldNotReachHere(); // Apr 13 2016: We decided to not use read_barrier

    // 1- get Region object
    Region* region = esc_obj->get_region();
    assert (region != NULL, "sanity check");

    // 2- add to stack (no duplicate)

    // I think we have a race here: crashing at this
    //TODO: fix this
    region->esc_stack_push_thread_safe(esc_obj);
    /* StackIterator<oop,mtThread> si(*(region->esc_stack()));
   while (!si.is_empty()){
     oop v = si.next();
     if (v == esc_obj) return;
     }

   region->esc_stack()->push(esc_obj);
   region->lock_esc_stack()->unlock();*/
  }
};

#endif /* ASRMM_REGION_REGION_HPP */
