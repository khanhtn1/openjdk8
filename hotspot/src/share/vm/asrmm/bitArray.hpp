/*
 * bitArray.hpp
 *
 *
 */

// My own version of BitMap - unable to use utilities/BitMap
// so many things are from that class


#ifndef SRC_SHARE_VM_ASRMM_BITARRAY_HPP
#define SRC_SHARE_VM_ASRMM_BITARRAY_HPP

#include "memory/allocation.hpp"


class BitArray : public CHeapObj<mtThread> {

public:
  typedef size_t idx_t;         // Type used for bit and word indices.
  typedef uintptr_t bm_word_t;  // Element type of array that represents
                                // the bitmap.

private:

  //  bm_word_t* _map;     // First word in bitmap
  //  idx_t      _size;    // Size of bitmap (in bits)
  
  // Puts the given value at the given offset, using resize() to size
  // the bitmap appropriately if needed using factor-of-two expansion.
  //  void at_put_grow(idx_t index, bool value);

protected:
  // Return the position of bit within the word that contains it (e.g., if
  // bitmap words are 32 bits, return a number 0 <= n <= 31). 
  // input: bit [0.._size)
  static idx_t bit_in_word(idx_t bit) { return (bit >> LogBitArrayWordsPerBit) & (BitsPerWord - 1); }

  // Return a mask that will select the specified bit, when applied to the word
  // containing the bit.
  static bm_word_t bit_mask(idx_t bit) { return (bm_word_t)1 << bit_in_word(bit); }

  // Return the index of the word containing the specified bit.
  // input: bit [0.._size)
  static idx_t word_index(idx_t bit)  { return (bit >> (LogBitsPerWord + LogBitArrayWordsPerBit)); }

  // Return the bit number of the first bit in the specified word.
  static idx_t bit_index(idx_t word)  { return (word << (LogBitsPerWord + LogBitArrayWordsPerBit)); }

  // Return the array of bitmap words, or a specific word from it.
  bm_word_t* map() const           { return (bm_word_t*)this; }
  bm_word_t  map(idx_t word) const { return *((bm_word_t*)this +word); }

  // Return a pointer to the word containing the specified bit.
  bm_word_t* word_addr(idx_t bit) const { return map() + word_index(bit); }

  // Set a word to a specified value or to all ones; clear a word.
  void set_word  (idx_t word, bm_word_t val) { *((bm_word_t*)this +word) = val; }
  void set_word  (idx_t word)            { set_word(word, ~(uintptr_t)0); }
  void clear_word(idx_t word)            { *((bm_word_t*)this +word) = 0; }

  // Utilities for ranges of bits.  Ranges are half-open [beg, end).

  // Ranges within a single word.
  bm_word_t inverted_bit_mask_for_range(idx_t beg, idx_t end) const;
  void  set_range_within_word      (idx_t beg, idx_t end);
  void  clear_range_within_word    (idx_t beg, idx_t end);
  void  par_put_range_within_word  (idx_t beg, idx_t end, bool value);

  // Ranges spanning entire words.
  void      set_range_of_words         (idx_t beg, idx_t end);
  void      clear_range_of_words       (idx_t beg, idx_t end);
  void      set_large_range_of_words   (idx_t beg, idx_t end);
  void      clear_large_range_of_words (idx_t beg, idx_t end);

  // The index of the first full word in a range.
  idx_t word_index_round_up(idx_t bit) const;

  // Verification.
  inline void verify_index(idx_t index) const;

  inline void verify_range(idx_t beg_index, idx_t end_index) const; 


public:
  
  BitArray();
  ~BitArray();

  // Accessing
  idx_t size() const                    { return (idx_t)PageSize; }
  
  idx_t size_in_words() const           {
    idx_t val = PageSize >> (LogBitsPerWord + LogBitArrayWordsPerBit);
    idx_t val2 = word_index(size() + ((BitsPerWord - 1)<<LogBitArrayWordsPerBit));
    assert(val == val2, err_msg("different in size_in_words static val=%d, instance_val=%d", val, val2));
    return word_index(size() + ((BitsPerWord - 1)<<LogBitArrayWordsPerBit));
 }

  bool at(idx_t index) const {
    verify_index(index);
    return (*word_addr(index) & bit_mask(index)) != 0;
  }

  // Align bit index up or down to the next bitmap word boundary, or check
  // alignment.
  static idx_t word_align_up(idx_t bit) {
    return align_size_up(bit >> LogBitArrayWordsPerBit, BitsPerWord);
  }
  static idx_t word_align_down(idx_t bit) {
    return align_size_down(bit >> LogBitArrayWordsPerBit, BitsPerWord);
  }
  static bool is_word_aligned(idx_t bit) {
    return word_align_up(bit) == bit;
  }

  // Set or clear the specified bit.
  inline void set_bit(idx_t bit);
  inline void clear_bit(idx_t bit);

  void at_put(idx_t index, bool value);

  // Update a range of bits.  Ranges are half-open [beg, end).
  bool set_range   (idx_t beg, idx_t end);
  void clear_range (idx_t beg, idx_t end);
  void at_put_range(idx_t beg, idx_t end, bool value);

  // Clearing
  void clear_large();
  inline void clear();

  // Looking for 1's and 0's at indices equal to or greater than "l_index",
  // stopping if none has been found before "r_index", and returning
  // "r_index" (which must be at most "size") in that case.
  idx_t get_next_one_offset_inline (idx_t l_index, idx_t r_index) const;
  idx_t get_next_zero_offset_inline(idx_t l_index, idx_t r_index) const;

  // Non-inline versionsof the above.
  idx_t get_next_one_offset (idx_t l_index, idx_t r_index) const;
  idx_t get_next_zero_offset(idx_t l_index, idx_t r_index) const; 

  idx_t get_next_one_offset(idx_t offset) const {
    return get_next_one_offset(offset, size());
  }
  idx_t get_next_zero_offset(idx_t offset) const {
    return get_next_zero_offset(offset, size());
  }

  // Test if all bits are set or cleared
  bool is_full() const;
  bool is_empty() const;

};

#endif
