#include "storageManager.hpp"
#include "runtime/thread.hpp"
#include "memory/allocation.inline.hpp"
#include "memory/allocation.hpp"
#include "utilities/copy.hpp"
#include "gc_interface/collectedHeap.hpp"

LargePageDesc::LargePageDesc() : _addr(NULL), _size(0), _next(NULL) {

}

LargePageDesc::LargePageDesc(HeapWord* page_addr, size_t page_size) : _addr(page_addr), _size(page_size), _next(NULL) {

}

LargePageDesc::~LargePageDesc() {

}

StorageManager::StorageManager() :  _page_count(0) {//_free_pages(NULL), _holed_pages(NULL), _page_count(0) {
  ShouldNotReachHere();
}

StorageManager::StorageManager(Thread* t) : _thread(t), _page_count(0), _large_pages(NULL), _large_page_count(0) {
  //  _free_pages = (Stack<Page*,mtThread>*) new Stack<Page*,mtThread>();
  //_holes_pages = (Stack<Page*,mtThread>*) new Stack<Page*,mtThread>();
  _free_space = 0;
  _free_called = false;
  _total_allocated = 0;
}

StorageManager::~StorageManager(){
}


StorageManager::StorageManager(const StorageManager& other) {

}



void StorageManager::initialize(Thread* t) {
 
  assert(t == Thread::current(), err_msg("Wrong thread passed in for storage manager passed in = %p current = %p", t, Thread::current()));
  _thread = t;
  _free_space = 0;
  _free_called = false;
  _total_allocated = 0;

  /*
  _free_pages = new LinkedArray<Page*>(LinkedArrayElementCapacity);
  _holes_page = new LinkedArray<Page*>(LinkedArrayElementCapacity);

   */	
}

/*Added by Harry Xu on 4/15/16 
* This function will only be called from the GC 
* when it cleans up the holes
*/
void StorageManager::restore_useless_pages(){
  while(!_useless_pages.is_empty()){
    Page* p = _useless_pages.pop();
    _page_count++;
    Copy::zero_to_words((HeapWord*)p, (size_t)PageSize);
    p->initialize();
    _free_pages.push(p);
  }
  // missing handling for large_holes
}

void StorageManager::store(Page* aPage) {
  //  if (aPage->free_size() <= (size_t)UsefulPageThreshold) {
  if (DebugStorageManager) tty->print_cr("storage manager (%p) store page %p page_count = %d used=%d/%d free=%d hasHoles=%d", this, aPage, _page_count, aPage->dead_size(), PageSize, aPage->free_size(), aPage->has_holes());
  
  if(!UseYakHoleSummary){
    if(aPage->has_holes()){
      _useless_pages.push(aPage);
    } else{
      if(ReportYakReferenceStats){
	_free_space += PageSize;
      }
      //Universe::heap()->deallocate_page((HeapWord*)aPage, PageSize);
      _page_count++;
      _free_pages.push(aPage);
    }
  } else{
    if(aPage->is_reusable()){
      _page_count++;
      _free_pages.push(aPage);
    } else{
      _useless_pages.push(aPage);
    }
  }
  return;
    // }
    /*
  _page_count ++;

  if (aPage->has_holes()) {
    if (DebugStorageManager) tty->print_cr("storage_store holes_page %p page_count = %d used=%d/%d free=%d", aPage, _page_count, aPage->dead_size(), PageSize, aPage->free_size());
    _holes_pages.push(aPage);
  } else {
    if (DebugStorageManager) tty->print_cr("storage_store free_page %p page_count = %d used=%d/%d free=%d", aPage, _page_count, aPage->dead_size(), PageSize, aPage->free_size());
    _free_pages.push(aPage);
  }
    */

}

void StorageManager::store_large_page(HeapWord* addr, size_t size, bool large_hole) {
  //if(true){
  //  Universe::heap()->deallocate_page(addr, size);
  //  return;
  //}

  LargePageDesc* desc = new LargePageDesc(addr, size);
 
  if (large_hole) {
    if (_large_holes == NULL) {
      _large_holes = desc;
    } else { // just add at beginning
      desc->set_next(_large_holes);
      _large_holes = desc;
    }

  } else {
 
    if(ReportYakReferenceStats){
      _free_space += size;
    }

    if (_large_pages == NULL) {
      _large_pages = desc;
    } else { //insert and keep ascending order
      LargePageDesc* current = _large_pages;
      LargePageDesc* pre = NULL;
      while (current != NULL) {
        if (current->get_size() < size) {
          pre = current;
          current = current->get_next();
        } else {
          break;
        }
      }
      
      if (pre != NULL) {
        assert (pre->get_size() < size, "must be");
        pre->set_next(desc);
        desc->set_next(current);
      } else {
        desc->set_next(_large_pages);
        _large_pages = desc;
      }
    }
    
    
    _large_page_count++;
    
    if (DebugStorageManager) tty->print_cr("storage_store large_page %p size=%d large_page_count = %d", addr, size, _large_page_count);
  } // end of not large hole
}


Page* StorageManager::give() {
  /*  
  if (!_holes_pages.is_empty()) {
    _page_count --;
    Page* result =  _holes_pages.pop();
    if (DebugStorageManager) tty->print_cr("storage_give holes_page %p page_count = %d used=%d/%d free=%d", result, _page_count, result->dead_size(), PageSize, result->free_size());
    assert (result != NULL, "must be");
    return result;
  }
  */
  //if(true) return NULL;

  if(ReportYakReferenceStats){
    _free_called = false;
    _total_allocated += PageSize;
  }
  if(!_free_pages.is_empty())  {
    _page_count --;
    if(ReportYakReferenceStats){
      _free_space -= PageSize;
    }

    Page* result = _free_pages.pop();
    if (DebugStorageManager) tty->print_cr("storage manager (%p) gives free_page %p page_count = %d used=%d/%d free=%d", this, result, _page_count, result->dead_size(), PageSize, result->free_size());
    assert (result != NULL, "must be");
    return result;
  }
  
  assert (_page_count == 0, "must be");
  return NULL;
}

HeapWord* StorageManager::give_large_page(size_t size_requested) {
  if (_large_page_count ==0 ) return NULL;

  if(ReportYakReferenceStats) {
    _total_allocated += size_requested;
    _free_called = false;
  }

  // walk thru the list and pick the best fit large page
  assert (_large_pages != NULL, "must be not null");
  LargePageDesc* current = _large_pages;
  LargePageDesc* pre = NULL;
  while (current != NULL) {
    if (current->get_size() >= size_requested) { // found    
      break;
    } else { //move on
      pre = current;
      current= current->get_next();
    }
  }

  if (current == NULL) return NULL;
  if(ReportYakReferenceStats){
    _free_space -= size_requested;
  }
  _large_page_count --;
  assert (current != NULL, " must be");
  assert (current->get_size() >= size_requested, "must be large enough");
  
  HeapWord* result = current->get_addr();
  size_t size = current->get_size();


  //fix the link
  if (pre == NULL) {
    _large_pages = _large_pages->get_next();
  } else {
    assert (pre->get_next() == current, "sanity");
    pre->set_next(current->get_next());
  }

  delete current;

  if (DebugStorageManager) tty->print_cr("storage_give large_page %p size=%d large_page_count = %d", result, size, _large_page_count);
  return result;
}

bool StorageManager::active() {
  return (_page_count > 0 || _large_page_count > 0);
}

bool StorageManager::has_page() {
  return _page_count > 0;
}

bool StorageManager::has_large_page() {
  return _large_page_count > 0;
}

void StorageManager::break_large_page(LargePageDesc* large_page) {
  //TODO
}

void StorageManager::break_one_large_page() {
  //TODO
}

void StorageManager::break_all_large_pages() {
  //TODO
}

//TODO
void StorageManager::freeAll() {
  /* Reseting from here
  for (int i = 0; i < _pages->length(); i++) {
    Page* p = _pages->pop();
    p->clear();
    free (p);
  }

  _pages->clear();

  to here*/
  //delete _pages?
    
}
