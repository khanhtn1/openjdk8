/* This is the thread-local manager that is in charge of region
 * hierarchy
 */
#ifndef SRC_SHARE_VM_ASRMM_REGIONMANAGER_HPP
#define SRC_SHARE_VM_ASRMM_REGIONMANAGER_HPP


#include "region.hpp"
#include "page.hpp"
#include "storageManager.hpp"
#include "regionTable.hpp"

class RegionManager : public CHeapObj<mtThread> {
  friend class VMStructs;
  
private:
  int _thread_id; 
  int _current_region_id; // also the total region counts so far
  int _iteration_count; // increases when see iteration_start(), decreases when see iteration_end(); used to determine whether or not this manager is active or not
  // This can be removed, is_active() can just check if _current_region is not NULL or not  - I don't remove it because I used it as a return value for iteratioin_start and iteration_end

  Region* _current_region;
  Thread* _thread; // thread that owns the region manager :: Cant be removed: needs it to get back to RegionManager to get to storagemanager, unless we can move this somewhere else to get to this regionmanager

  StorageManager* _storage_manager;

  //Region** _regions; // all regions :: to be removed since I moved it to static table RegionTable
public:
  
  static bool needs_to_clear_the_world;
  RegionManager(Thread* t); // instead of passing a Thread*, pass ini thread_id instead: But I don't know the behavior when thread obj ends, can't remove because of storagemanager
  ~RegionManager();
  //copy constructor
  RegionManager(const RegionManager& other);
  
  // void initialize(Thread* t);
  static void clear_all_holes();

  bool is_active();

  void set_current_region(Region* region) { 
    _current_region = region; 
    _current_region_id = 0;
    if (region != NULL) {
      _current_region_id = region->get_region_id();
    }
  }
  
  int get_thread_id()				{	return _thread_id;		}

  void set_thread_id(int thread_id)  {  _thread_id = thread_id;  }

  int get_current_region_id();
  
  int create_region();

  int create_this_region(int region_id);

  int reclaim_region();

  int reclaim_this_region(int region_id);
  
  Region* current_region()			{       return _current_region;		}
  
  Region* get_region(int region_id);
  

  // allocation
  HeapWord* allocate(size_t size);


  //storage management
  StorageManager* get_storage_manager();
  void store_page(Page* page);
  void store_large_page(HeapWord* page_addr, size_t page_size, bool large_hole);
  Page* give_page();
  HeapWord* give_large_page(size_t size_requested);
  bool has_large_pages();

};

#endif // SRC_SHARE_VM_ASRMM_REGIONMANAGER_HPP
