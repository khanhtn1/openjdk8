#ifndef SHARE_VM_ASRMM_REGIONTABLE_HPP
#define SHARE_VM_ASRMM_REGIONTABLE_HPP

#include "region.hpp"

// All static to keep record of all regions ever created
// A quick way to store them

class RegionTable : AllStatic {
  friend class ASRMMHelper;
  friend class PSParallelCompact;
  friend class PSScavenge;
 private:
  static Region* region_matrix[max_jubyte+1][max_jubyte+1];

 public:
  

  static void add_region(Region* r, region_id_t region_id, thread_id_t thread_id) {
    assert(r != NULL, "can't add NULL region");
    assert(r->get_region_id() == region_id, "sanity check - region id");
    assert(r->get_thread_id() == thread_id, "sanity check - thread id");
    region_matrix[region_id][thread_id] = r;

  }

  static void add_region(Region* r) {
    // tty->print_cr("RegionTable:: adding [%d,%d]", r->get_region_id(),r->get_thread_id());
    assert(r != NULL, "can't add NULL region");
    // Region* old_r = region_matrix[r->get_region_id()][r->get_thread_id()];
    region_matrix[r->get_region_id()][r->get_thread_id()] = r;
  }

  static Region* fetch_region(region_id_t region_id, thread_id_t thread_id) { 
    
    assert(thread_id != 0 && region_id != 0, "sanity check");
    Region* result = region_matrix[region_id][thread_id];
    return result;
    
  }
  static Region* get_region(region_id_t region_id, thread_id_t thread_id) {
    // tty->print_cr("RegionTable::finding [%d,%d]", region_id,thread_id);

    assert(thread_id != 0 && region_id != 0, "sanity check");
    Region* result = region_matrix[region_id][thread_id];
    assert(result != NULL, err_msg("cant get NULL region [%d,%d]",region_id,thread_id));
    return result;
  }

  static void remove_region(region_id_t region_id, thread_id_t thread_id) {
    //tty->print_cr("RegionTable::removing [%d,%d]", region_id,thread_id);

    assert(thread_id != 0 && region_id != 0, "sanity check");
    assert(region_matrix[region_id][thread_id] != NULL,
        " can't remove NULL region");
    region_matrix[region_id][thread_id] = NULL;
  }

  static bool is_active_region(region_id_t region_id, thread_id_t thread_id) {
    return region_matrix[region_id][thread_id] != NULL;
  }
};

#endif // SHARE_VM_ASRMM_REGIONTABLE_HPP
