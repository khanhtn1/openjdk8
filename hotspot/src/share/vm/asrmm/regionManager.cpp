/*
 * regionManager.cpp
 *
 *  Created on: Nov 23, 2015
 *      Author: Khanh
 */

#include "regionManager.hpp"
#include "oops/oop.hpp"
#include "oops/oop.inline.hpp"
#include "runtime/thread.hpp"
#include "regionTable.hpp"

bool RegionManager::needs_to_clear_the_world = false;

RegionManager::RegionManager(Thread* t): _thread(t),_current_region(NULL), _current_region_id(0), _iteration_count(0) {

  assert (t == Thread::current(), err_msg("Wrong thread obj passed in, passed in=%p, current=%p ",t,Thread::current() ) ) ;
  //  _current_thread_id =java_lang_Thread::thread_id(t->threadObj());
  _thread_id =  t->get_id();
  _storage_manager = NULL;

}

RegionManager::~RegionManager() {

}

//copy contructor
RegionManager::RegionManager(const RegionManager& other) {
	 ShouldNotReachHere();
	 // I do NOT want this region manager to get copied somewhere else
}

/*void RegionManager::initialize(Thread* t) {
  _current_iteration_id = 0;
  assert (t == Thread::current(), err_msg("Wrong thread obj passed in, passed in=%p, current=%p ",t,Thread::current() ) ) ;
  _thread = t;
  _current_thread_id =  _thread->get_id();
  tty->print_cr("Thread %p and id = %d", t, _current_thread_id);
  _current_region= NULL; 
	
  }*/


bool RegionManager::is_active() {
  //  return _iteration_count >0; 
  return _current_region != NULL; // an attempt to remove _iteration_count
}

int RegionManager::create_region() {

  bool has_pages = false;
  bool has_large_pages = false;

  if (_storage_manager != NULL)  { 
    has_pages = _storage_manager->has_page();
    has_large_pages = _storage_manager->has_large_page();
  }

  _current_region_id++;
  _current_region =  new Region(_current_region_id,_thread_id, _current_region, _thread, has_pages, has_large_pages);


  assert (((_current_region_id & ~0xff) == 0), "region id overflow, more than 1 byte capacity");

  RegionTable::add_region(_current_region, _current_region_id,_thread_id);

  return ++_iteration_count;
  
}

int RegionManager::get_current_region_id() {
  return _current_region_id;
}

int RegionManager::create_this_region(int region_id) {
  bool has_pages = false;
  bool has_large_pages = false;

  if (_storage_manager != NULL)  {
    if(DebugYak){
      tty->print_cr("Wrong!!!Storage manager is NOT NULL in thread %d, region %d", _thread_id, region_id);
    }
  } else{
    if(region_id > 1){
      //let's get an existing storage manager
      Region* r = RegionTable::fetch_region(region_id - 1, _thread_id);
      if(r != NULL && r->is_retired()){
        StorageManager* m = r->get_manager()->get_storage_manager();
        if(DebugYak){ 
          if(m){
            tty->print_cr("Storage manager %p shared from thread %d, region %d, hasPage %d", m, _thread_id, region_id-1, m->has_page());  
          } else{
            tty->print_cr("Wrong!!! Storage manager NULL!");
          }
        } 
        _storage_manager = m;
      }
    }   
  }

  has_pages = get_storage_manager()->has_page();
  has_large_pages = get_storage_manager()->has_large_page();

  _current_region =  new Region(region_id,_thread_id, _current_region, _thread, has_pages, has_large_pages);  
  assert (((region_id & ~0xff) == 0), "region id overflow, more than 1 byte capacity");
  RegionTable::add_region(_current_region, region_id,_thread_id);
  _current_region_id = region_id;

  return ++_iteration_count;
}

int RegionManager::reclaim_region() {
  assert (_current_region != NULL, "NULL region that means the sequence of iteration_start and iteration_end is not correct");
  _current_region->retire();
  //for(int i = 2; i < 256; i++){
  //  Region* cur = RegionTable::fetch_region(_current_region_id, i);
  //  if(cur == NULL) break;
    //cur->set_is_retired();
  // }
  _current_region = _current_region->parent();
  return --_iteration_count;
}

//This can only be called from within the GC
void RegionManager::clear_all_holes(){
  for(int i = 1; i < 256; i++){
    for(int j = 1; j < 256; j++){
      Region* cur = RegionTable::fetch_region(j, i);
      if(cur != NULL && cur->is_retired()){
        tty->print_cr("retired %d, %d", i, j );
        cur->get_manager()->get_storage_manager()->restore_useless_pages();
        cur->clear_metadata();
        RegionTable::remove_region(j, i);
        delete cur;
      }
    }  
  }
}

int RegionManager::reclaim_this_region(int region_id) {
  
  assert (_current_region != NULL, "NULL region that means the sequence of create_region and delete_region is not correct");
  assert (_current_region->get_region_id() == region_id, "must be");
  _current_region->retire();
  _current_region = _current_region->parent();
  assert (_current_region == NULL, "must be?");
  return --_iteration_count;
}


Region* RegionManager::get_region(int region_id)  { 
  ShouldNotReachHere();
  assert (region_id > 0 && region_id <= _current_region_id, err_msg("region id %d is out of range: must <= %d", region_id, _current_region_id));

  //Region* result=  *(_regions + region_id);
  //assert (result  != NULL, "sanity check - cant be NULL pointer");
  //return result;
  return NULL;
}

/*bool RegionManager::find_region(Region* r) {
  //Search the hierarchy from the current region
  
  //Fast path:
  if (_current_region == r) return true;

  //Slow path
  Region* temp = _current_region->parent();
  while (temp != NULL) {
    if (temp == r) return true;
    temp = temp->parent();
  }
  return false;
  }
*/


HeapWord* RegionManager::allocate(size_t size) {
  
  
  HeapWord* obj =  _current_region->allocate(size);
  assert (obj != NULL, " can't be null" ); 
  
  ((oop)obj)->set_ids(_current_region_id, _thread_id);
  if (DebugMarkNativeOop) {
    tty->print_cr("setting ids for p=%p ids=[%d,%d]",obj,_current_region_id, _thread_id);
    tty->print_cr("obj's region signature [%d,%d]", ((oop)obj)->get_region_id(), ((oop)obj)->get_thread_id());
  }
  return obj;
}


StorageManager* RegionManager::get_storage_manager() {
  if (_storage_manager == NULL) {
    _storage_manager = new StorageManager(_thread);
  }
  return _storage_manager;
}

void RegionManager::store_page(Page* page) {
  get_storage_manager()->store(page);
}

void RegionManager::store_large_page(HeapWord* large_obj, size_t size, bool large_hole) {
  get_storage_manager()->store_large_page(large_obj, size, large_hole);
}

Page* RegionManager::give_page(){
  assert (_storage_manager != NULL, "Sanity check");
  if(DebugYak){
    tty->print_cr("trying to give: thread_id %d, region_id %d, storage manager %p",  _thread_id, _current_region_id, _storage_manager);
  }
  return _storage_manager->give();
}

HeapWord* RegionManager::give_large_page(size_t size_requested){
  assert (_storage_manager != NULL, "Sanity check");
  return _storage_manager->give_large_page(size_requested);
}

bool RegionManager::has_large_pages() {
  if (_storage_manager == NULL) return false;
  return (_storage_manager->has_large_page());
}
