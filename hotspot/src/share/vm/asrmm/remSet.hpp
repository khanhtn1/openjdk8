/*
 * remSet.hpp 
 * RemberSet implementation with a fixed number
 * [TotalRemSetBuckets] of buckets - thus no rehash, use standard
 * hashing function
 *
 */

#ifndef SRC_SHARE_VM_ASRMM_REMSET_HPP
#define SRC_SHARE_VM_ASRMM_REMSET_HPP

#include "utilities/stack.inline.hpp"
#include "memory/allocation.hpp"
#include "utilities/top.hpp"
//#include "asrmm/region.hpp"
#include "runtime/mutex.hpp"

//TODO: Simplify, Generalize and Improve the implemenation of this and support more functions such as removing
// i.e., create HashMap<K,V> class

class RemSet;
class Region;

class PairRS: public CHeapObj<mtThread> {

 public:
  oop _obj;
  void* _field_addr; // needs to be void* because of the use of template with T==oop and T=unsigned int*

  PairRS();
  PairRS(oop o, void* f);
  ~PairRS() {}; // TODO
};

//ChunkRS contains fixed number of references, if needs more then use _next to get another instance
/*class ChunkRS: public CHeapObj<mtThread> {

 public:
  int _current_index;
  int _capacity;
  PairRS* _array;
  ChunkRS* _next;

  ChunkRS(int capacity = RemSetChunkCapacity);
  ~ChunkRS();

  //void initialize(int capacity=RemSetChunkCapacity);
  bool push(void* referent_field, oop referent);

 int size() {
    return _current_index;
  }

  PairRS* get_array() {
    return _array;
  }

  ChunkRS* get_next() {
    return _next;
  }

  void print();

  };*/

// Mar 6: Replaces ChunkRS by Stack to have the iterator
// Can't change Entry* in Bucket into Stack because of _next and _ordered_next
class Entry: public CHeapObj<mtThread> {

 private:

 public:
  oop _key; // escaping object

  region_id_t _dest_region_id;
  //  ChunkRS* _current_chunk; // list of all references to this _key
  Stack<PairRS, mtThread> _references; // list of all references to this _key, changed to Stack so we can iterate over the list immediately, no need to code a different iterator for Chunk

  Entry* _next; //this is next Entry in the same Bucket
  Entry* _ordered_next; // this is next Entry when we do OrderedIterator (to visit esc objs with dest_region == HEAP first, then regions in according to the semilatice

  Entry(oop key);
  ~Entry();

  void push(void* referent_field, oop referent, region_id_t dest_region_id);

  void set_esc_obj(oop obj) {
    _key = obj;
  }

  oop get_esc_obj() {
    return _key;
  }

  region_id_t get_dest_region_id() {
    return _dest_region_id;
  }

  Entry* get_next() {
    return _next;
  }

  void set_next(Entry* entry) {
    _next = entry;
  }

  Entry* get_ordered_next() {
    return _ordered_next;
  }

  void set_ordered_next(Entry* entry) {
    _ordered_next = entry;
  }

  // Since the StackIterator does NOT have a copy constructor.
  // Let's just return a reference of the stack instead of a iterator (copy is undefined).
  Stack<PairRS, mtThread>* get_references() {
    return &_references;
  }

  // TODO to be deleted if it is OK.
  //StackIterator<PairRS, mtThread> get_references_iterator() {
  //  StackIterator<PairRS, mtThread> iter(_references);
  //  return iter;
  //}

  //verify references of this entry: if every reference is no longer, return false to remove this entry completely from the Bucket
  // otherwise, return false. However, during the checking, if a reference is no longer valid, get rid of it so that we don't have to 
  // update it:: it's gonna be ugly with the Stack 
  bool verify(); 

  bool is_valid();

  void merge_with(Entry* another);

  void print();
};

class Bucket: public CHeapObj<mtThread> {

private:
  int _size;


  Mutex _lock;

  Entry* find_entry_no_lock(oop esc_obj);

public:

  Entry* _head; // TODO make this private


  Bucket();
  ~Bucket();

  //    void initialize();

  Mutex* lock() {
    return &_lock;
  }

  Entry* find_entry(oop esc_obj);

  //return whether or not this is a new entry in the bucket
  bool push(oop esc_obj, void* referent_field, oop referent, region_id_t dest_region_id);

  bool add_and_update_entry(Entry* entry, oop esc_obj);

  Entry* get_head() {
    return _head;
  }

  int size() {
    return _size;
  }

  Entry* remove_entry(oop esc_obj);

  void print();

};


class KeyIterator: public CHeapObj<mtThread> {

private:
  int _current_bucket_index;
  int _total_buckets;

  Bucket** _buckets;
  Bucket* _current_bucket;
  Entry* _current_entry;
  
  Mutex* _lock;

public:

  bool has_next();
  oop next();

  KeyIterator(RemSet* set);
  ~KeyIterator();

};

// to be used by OrderedIterator
class LinkedBucket: public CHeapObj<mtThread> {
private:

  region_id_t _dest_region_id; // used to enforce the order 0 is for NULL, non-zero is region

  Entry* _head; // all entries in this LinkedBucket

  LinkedBucket* _next;

public:

  LinkedBucket(region_id_t dest_region_id, Entry* entry);

  region_id_t get_dest_region_id() {
    return _dest_region_id;
  }

  Entry* get_head() {
    return _head;
  }

  LinkedBucket* get_next() {
    return _next;
  }

  void set_next(LinkedBucket* next) {
    _next = next;
  }

  void add(Entry* entry);
};

class OrderedIterator: public CHeapObj<mtThread> {
private:

  LinkedBucket* _head_bucket; // this acts as current_bucket also
  Entry* _current_entry;
  LinkedBucket** addr;
  int size_temp_arr;
  void construct(RemSet* set);

public:
  OrderedIterator(RemSet* set);
  ~OrderedIterator();

  bool has_next();
  oop next();
  Entry* next_entry();

};

class EntryIterator: public CHeapObj<mtThread> {
private:
  int _current_bucket_index;
  int _total_buckets;

  Bucket** _buckets;
  Bucket* _current_bucket;
  Entry* _current_entry;

  Mutex* _lock;

public:

  EntryIterator(RemSet* set);
  ~EntryIterator();

  bool has_next();
  Entry* next();

};


class RemSet: public CHeapObj<mtThread> {
  friend class VMStructs;

private:

  int _num_buckets;
  int _size; // total number of entries;
  Bucket** _all_buckets;
  Mutex _lock; 

public:

  RemSet(int num_buckets = TotalRemSetBuckets);
  ~RemSet();

  Mutex* lock() {
    return &_lock;
  }

  // Not thread-safe.
  int put(oop esc_obj, void* referent_field, oop referent, region_id_t dest_region_id);

  // Used by re-hash (if key is changed).
  int put(oop esc_obj, Entry* entry);

  // Thread-safe.
  int put_thread_safe(oop esc_obj, void* referent_field, oop referent, region_id_t dest_region_id);

  Entry* remove(oop esc_obj);
  
  Entry* get(oop esc_obj);
  
  Bucket** get_buckets() {
    return _all_buckets;
  }

  int get_num_buckets() {
    return _num_buckets;
  }
  
  //iterators
  KeyIterator* get_key_iterator();
  OrderedIterator* get_ordered_iterator();
  EntryIterator* get_entry_iterator();

  int size() {
    return _size;
  }

  void clear() {
    // TODO clear the content, may release resource.
  }

  void print();
};

#endif // SRC_SHARE_VM_ASRMM_REMSET_HPP
