// bitArray.cpp


#include "bitArray.inline.hpp"
#include "utilities/copy.hpp"


void BitArray::set_range_within_word(idx_t beg, idx_t end) {
  // With a valid range (beg <= end), this test ensures that end != 0, as
  // required by inverted_bit_mask_for_range.  Also avoids an unnecessary write.
  if (beg != end) {
    bm_word_t mask = inverted_bit_mask_for_range(beg, end);
    *word_addr(beg) |= ~mask;
  }
}

void BitArray::clear_range_within_word(idx_t beg, idx_t end) {
  // With a valid range (beg <= end), this test ensures that end != 0, as
  // required by inverted_bit_mask_for_range.  Also avoids an unnecessary write.
  if (beg != end) {
    bm_word_t mask = inverted_bit_mask_for_range(beg, end);
    *word_addr(beg) &= mask;
  }
}

bool BitArray::set_range(idx_t beg, idx_t end) {

  bool verbose = false;

  if (at(beg)) return false;

  verify_range(beg, end);
  if (DebugBuildMapInPage && verbose ) 
    tty->print_cr("set_range %d - %d", beg, end);
  idx_t beg_full_word = word_index_round_up(beg);
 
  end = align_size_up(end, BitArrayWordsPerBit);
  idx_t end_full_word = word_index(end);

  if (DebugBuildMapInPage && verbose)
    tty->print_cr("full word %d - %d", beg_full_word, end_full_word);

  if (beg_full_word < end_full_word) {
    if (DebugBuildMapInPage && verbose)
      tty->print_cr("full word < %d - %d", beg_full_word, end_full_word);

    // The range includes at least one full word.
    if (DebugBuildMapInPage && verbose)
      tty->print_cr("set_range_within %d - %d", beg,  bit_index(beg_full_word));

    set_range_within_word(beg, bit_index(beg_full_word));
    set_range_of_words(beg_full_word, end_full_word);

    if (DebugBuildMapInPage && verbose)
      tty->print_cr("set_range_within %d - %d", bit_index(end_full_word),end);

    set_range_within_word(bit_index(end_full_word), end);
  } else {
    // The range spans at most 2 partial words.
    idx_t boundary = MIN2(bit_index(beg_full_word), end);
    boundary = MAX2(beg, boundary);
    if (DebugBuildMapInPage && verbose)
      tty->print_cr("full word >= %d - %d", beg_full_word, end_full_word);

    if (DebugBuildMapInPage && verbose)
      tty->print_cr("set_range_within %d - %d", beg, boundary);
    set_range_within_word(beg, boundary);

    if (DebugBuildMapInPage && verbose)
      tty->print_cr("set_range_within %d - %d", boundary, end);
    set_range_within_word(boundary, end);
  }
  return true;
}

void BitArray::clear_range(idx_t beg, idx_t end) {
  verify_range(beg, end);

  idx_t beg_full_word = word_index_round_up(beg);

  end = align_size_up(end, BitArrayWordsPerBit);
  idx_t end_full_word = word_index(end);

  if (beg_full_word < end_full_word) {
    // The range includes at least one full word.
    clear_range_within_word(beg, bit_index(beg_full_word));
    clear_range_of_words(beg_full_word, end_full_word);
    clear_range_within_word(bit_index(end_full_word), end);
  } else {
    // The range spans at most 2 partial words.
    idx_t boundary = MIN2(bit_index(beg_full_word), end);
    clear_range_within_word(beg, boundary);
    clear_range_within_word(boundary, end);
  }
}


void BitArray::at_put(idx_t offset, bool value) {
  if (value) {
    set_bit(offset);
  } else {
    clear_bit(offset);
  }
}

/*void BitArray::at_put_grow(idx_t offset, bool value) {
  if (offset >= size()) {
    resize(2 * MAX2(size(), offset));
  }
  at_put(offset, value);
  }*/


void BitArray::at_put_range(idx_t start_offset, idx_t end_offset, bool value) {
  if (value) {
    set_range(start_offset, end_offset);
  } else {
    clear_range(start_offset, end_offset);
  }
}


bool BitArray::is_full() const {
  bm_word_t* word = map();
  idx_t rest = (size() >> LogBitArrayWordsPerBit);
  for (; rest >= (idx_t) BitsPerWord; rest -= BitsPerWord) {
    if (*word != (bm_word_t) AllBits) return false;
    word++;
  }
  return rest == 0 || (*word | ~right_n_bits((int)rest)) == (bm_word_t) AllBits;
}


bool BitArray::is_empty() const {
  bm_word_t* word = map();
  idx_t rest = (size() >> LogBitArrayWordsPerBit);
  for (; rest >= (idx_t) BitsPerWord; rest -= BitsPerWord) {
    if (*word != (bm_word_t) NoBits) return false;
    word++;
  }
  return rest == 0 || (*word & right_n_bits((int)rest)) == (bm_word_t) NoBits;
}
