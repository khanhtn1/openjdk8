#include "remSet.hpp"
#include "memory/allocation.inline.hpp"
#include "oops/oop.inline.hpp"
#include "runtime/mutexLocker.hpp"
#include "region.hpp"

bool needLock = true;

// ===== Start of PairRS =====//

PairRS::PairRS(): _obj(NULL), _field_addr(NULL) {
}

PairRS::PairRS(oop o, void* f): _obj(o), _field_addr(f) {
}

// ===== End of PairRS =====//

// Mar 6: No longer use CHUNKRS

// ===== Start of ChunkRS =====//

/*
ChunkRS::ChunkRS(int capacity):_current_index(0), _capacity(capacity), _next(NULL) {
  _array = new PairRS[_capacity];
  if (DebugRemSet) tty->print_cr("ChunkRS(capacity)");
}

ChunkRS::~ChunkRS() {
  if (DebugRemSet) tty->print_cr("~ChunkRS()");
  delete[] _array; //FREE_C_HEAP_ARRAY(Pair, _array , mtThread);
}
*/

/*
void ChunkRS::initialize(int capacity) {
  _current_index = 0;
  _capacity = capacity;
  _next = NULL;
  _array = new Pair[_capacity];//NEW_C_HEAP_ARRAY(Pair, _capacity, mtThread);
}
*/

/*
bool ChunkRS::push(void* referent_field, oop referent) {
  if (_current_index  == _capacity)
    return false;
  
  _array[_current_index]._obj=referent;
  _array[_current_index]._field_addr = referent_field;
  _current_index++;
  
  return true;
}

void ChunkRS::print() {
  tty->print("[");
  for (int i =0; i < _current_index; i++) {
    tty->print(" %p-%p ",_array[i]._obj, _array[i]._field_addr); //TODO: pointer instead of array[]
  }
  tty->print("]");
}
*/
// ===== End of ChunkRS =====//


// ===== Start of Entry =====//

Entry::Entry(oop key):_key(key), _dest_region_id(255), _next(NULL), _ordered_next(NULL)  {

  if (DebugRemSet) tty->print_cr("Entry(oop %p)", key);
}

Entry::~Entry() {
  //TODO
  //tty->print_cr("~Entry()");
  
  /*  ChunkRS* chunk = _current_chunk;
  while (chunk != NULL) {
    ChunkRS* next = chunk->_next;
    delete chunk;//FREE_C_HEAP_OBJ(chunk, mtThread);
    chunk = next;
    }*/
}

/*
void Entry::initialize(oop key) {
  _key = key;
  _is_dest_region_heap = false;
  _dest_region = NULL;
  _next = NULL;
  _current_chunk = new ChunkRS();//NEW_C_HEAP_OBJ(ChunkRS, mtThread);
  _current_chunk->initialize();
}
*/

/*void Entry::push_region(Region* r) {
  if (DebugRemSet) tty->print_cr("Entry::push_region %p", r);

  if (r == NULL || _key->get_region() == NULL) {
    _is_dest_region_heap = true;
    _dest_region = NULL; // just to be consistent, can comment this out
    return;
  }

  // r is a valid Region
  if (_dest_region == NULL) { // equivalent to initialize _dest_region
    //tty->print_cr("prejoin init dest esc =%p,  r =%p, _dest_region =%p", _key,  r, _dest_region);
    _dest_region = Region::join_two_regions(r, _key->get_region()); // for easier: r is from left oop, _key is the right oop
    //tty->print_cr("postjoin init dest esc =%p, r =%p, _dest_region =%p", _key, r, _dest_region);
    if (_dest_region == NULL) {
      _is_dest_region_heap = true; // this will block further updating
    }
    return;
  }

  // _dest_region is not NULL and r is not NULL
  assert (_dest_region != NULL, "sanity check");
  assert (r != NULL, "sanity check");

  if (_dest_region == r) {
    return;
  }

  // call the join operation
  //tty->print_cr("prejoin esc =%p,  r =%p, _dest_region =%p", _key,  r, _dest_region);
  _dest_region = Region::join_two_regions(r, _dest_region); // for easier: r is from left oop, _dest_region is the right oop
  //tty->print_cr("postjoin esc =%p, r =%p, _dest_region =%p", _key, r, _dest_region);
  if (_dest_region == NULL) {
    _is_dest_region_heap = true; // this will block further updating
  }

}
*/

void Entry::push(void* referent_field, oop referent, region_id_t dest_region_id) {

  if (DebugRemSet) {
    tty->print_cr("Entry: push_reference esc_obj=%p, field=%p, ref=%p, dest_region_id=%d", _key, referent_field, referent, dest_region_id);
  }

  assert (Universe::heap()->is_in_reserved(referent_field), err_msg("field %p is outside of heap", referent_field));
  assert (Universe::heap()->is_in_reserved(referent), err_msg("referent %p is outside of heap", referent));
  _dest_region_id = MIN2(_dest_region_id, dest_region_id);

  /*if (_current_chunk->push(referent_field, referent) == false) {
    if (DebugRemSet) tty->print_cr("reach capacity of current chunk when pushing to entry esc_obj=%p, field=%p, ref=%p", _key, referent_field, referent);
    ChunkRS* chunk  =  new ChunkRS();//NEW_C_HEAP_OBJ(ChunkRS, mtThread);

    bool b = chunk->push(referent_field, referent);
    assert (b, "failed to add - not possible");
    chunk->_next = _current_chunk;
    _current_chunk = chunk;
    }*/
  
  _references.push(PairRS(referent, referent_field));

  
  ///if (DebugRemSet) tty->print_cr("done pushing to entry:  esc_obj=%p, field=%p, ref=%p, region=%p", _key, referent_field, referent, referent_region);
}

// return true if there is no valid reference in the stack
// return false there is at least one valid reference
// during traversal of references, get rid of invalid references
// This is NOT MT-safe.
bool Entry::verify() {
  
  Stack<PairRS, mtThread> new_references;
  
  bool invalid_key = true; // assume all references are invalid
  while (!_references.is_empty()) {
    PairRS pair = _references.pop();
    assert(Universe::heap()->is_in_reserved(pair._obj), err_msg("_obj %p should be in heap for _key %p",pair._obj,_key));
    assert(Universe::heap()->is_in_reserved(pair._field_addr), err_msg("_field_addr %p should be in heap for _key %p", pair._field_addr,_key)); 
    
    if (!Universe::heap()->is_in_reserved(pair._obj) || !Universe::heap()->is_in_reserved(pair._field_addr)) continue;
    
    oop must_be_key_oop = oopDesc::load_heap_oop((oop*)pair._field_addr);
    if (must_be_key_oop == _key) {
      new_references.push(pair);
      invalid_key = false; // at least one reference is still good
    } else { // just verbose
      if (DebugRemSet) tty->print_cr("getting rid of invalid reference %p-%p for key %p", pair._obj, pair._field_addr, _key);
    }
  }

  //transfer content because we cant do _references=new_references;
  while (!new_references.is_empty()) {
    _references.push(new_references.pop());
  }
  return invalid_key;
  
}

bool Entry::is_valid() {
  if (_references.is_empty()) return false;

  return !verify();
}

void Entry::merge_with(Entry* another) {
  
  Stack<PairRS, mtThread>* references = another->get_references();
  
  while (!references->is_empty()) {
    PairRS pair = references->pop();
    _references.push(pair);
  }


  //because this is the merge, I want to preserve the invariance that
  //of all references point to the correct esc_obj
  //therefore I have to update
  oop old_esc_obj = another->get_esc_obj();
  oop new_esc_obj = get_esc_obj();
  StackIterator<PairRS,mtThread> it(_references);
  while (!it.is_empty()) {
    PairRS pair = it.next();
    oop esc = oopDesc::load_heap_oop((oop*)pair._field_addr);
    if (esc == old_esc_obj || esc == new_esc_obj) {
      references->push(PairRS(pair._obj, pair._field_addr));
    }
  }
}


void Entry::print() {
  tty->print("Entry: esc_obj=%p, _dest_region_id=%d ", _key, _dest_region_id);
  tty->print(" [");
  StackIterator<PairRS, mtThread> iter(_references);
  while (!iter.is_empty()) {
    PairRS pair = iter.next();
    tty->print(" %p-%p, ", pair._obj, pair._field_addr);
  }
  tty->print_cr("]");
  /*ChunkRS* t = _current_chunk;
  while (t!= NULL) {
    t->print();
    t= t->_next;
  }
  tty->print_cr(" ");
  */

}

// ===== End of Entry =====//


// ===== Start of Bucket =====//

Bucket::Bucket(): _size(0), _head(NULL), _lock(Mutex::leaf, "Bucket lock", true){
  if (DebugRemSet) tty->print_cr("Bucket()");  
}


Bucket::~Bucket() {
  if (DebugRemSet) tty->print_cr("~Bucket");

  Entry* entry = _head;
  while (entry != NULL) {
    Entry* next = entry->_next;
    delete entry;//FREE_C_HEAP_OBJ(entry, mtThread);
    entry = next;
  }
  
}

/*
void Bucket::initialize() {
  _size = 0;
  _head = NULL;
}
*/

Entry* Bucket::find_entry(oop esc_obj) {

  // Mar 20, since we have remove(), _head is not always not NULL
  // assert(_head != NULL, "sanity check");
  //  if(needLock) {
    MutexLocker ml (lock());
    //}
  return find_entry_no_lock(esc_obj);

}





Entry* Bucket::find_entry_no_lock(oop esc_obj) {
  
  // Mar 20, since we have remove(), _head is not always not NULL
  // assert(_head != NULL, "sanity check");

  Entry* elem = _head;
  while (elem != NULL){
    if (elem->_key == esc_obj)
      return elem;
    elem = elem->_next;
  }
  return NULL;

}

bool Bucket::push(oop esc_obj, void* referent_field, oop referent, region_id_t dest_region_id) {

  // Khanh -- Mar 23: this check is at the wrong place because our recording happens BEFORE the actual store_heap_oop
  //oop must_be_esc_obj = oopDesc::load_heap_oop((oop*)referent_field);
  assert (/*must_be_esc_obj*/
	  oopDesc::load_heap_oop((oop*)referent_field) == esc_obj, 
	  err_msg("esc_obj=%p, loaded=%p, referent=%p, field_addr=%p, dest_region=%d", 
		  esc_obj,/* must_be_esc_obj*/oopDesc::load_heap_oop((oop*)referent_field), referent, referent_field, dest_region_id));

  //if(needLock){
     MutexLockerEx ml (&_lock, Mutex::_no_safepoint_check_flag );
     //}
  PRINT_YAK_DEBUG(DebugYakRemSet, "esc_obj: %p, region: %d, thread: %d, ref_obj: %p, region: %d, thread: %d",
      esc_obj, esc_obj->get_region_id(), esc_obj->get_thread_id(),
      referent, referent->get_region_id(), referent->get_thread_id());
  if (_size == 0) {
    Entry* e = new Entry(esc_obj);//NEW_C_HAAP_OBJ(Entry, mtThread);
    e->push(referent_field, referent, dest_region_id);
    _head = e;
    _size=1;
    return true;
  }

  

  Entry* e = find_entry_no_lock(esc_obj);
  bool new_entry = false;
  if (e == NULL) {
    e = new Entry(esc_obj);//NEW_C_HEAP_OBJ(Entry, mtThread);
    e->_next = _head;
    _head = e;
    _size++;
    new_entry = true;
  } 

  e->push(referent_field, referent, dest_region_id);
  return new_entry;
}

bool Bucket::add_and_update_entry(Entry* entry, oop esc_obj) {

  //if(needLock) {
     MutexLockerEx ml (&_lock, Mutex::_no_safepoint_check_flag);
     //}
  if (DebugRemSet)tty->print_cr("Bucket %p add_and_update_entry for new_oop=%p, old_oop=%p, size=%d", this,esc_obj, entry->get_esc_obj(),_size);

  if (_size == 0) {
    entry->set_esc_obj(esc_obj);
    assert (entry->_next == NULL, "entry still has attachment from remove?");
    _head = entry;
    _size = 1;
    return true;
  }

  Entry* e = find_entry_no_lock(esc_obj);
  bool new_entry = false;
  if (e == NULL) {
    //    this is expected
    entry->set_esc_obj(esc_obj);
    assert (entry->_next == NULL, "entry still has attachment from remove?");
    entry->_next = _head;
    _head = entry;
    _size++;
    new_entry = true;
  } else {
    //very interesting
    
    tty->print_cr("Bucket %p the new key %p is not new for this bucket - old key is %p entry's oop=%p == MERGING", this, esc_obj, e->get_esc_obj(),entry->get_esc_obj());
    entry->print();
    tty->print("INTO ");
    e->print();
    
    e->merge_with(entry);
    tty->print("Finally= "); 
    e->print();
    

  }
  
  return new_entry;

}

Entry* Bucket::remove_entry(oop esc_obj) {
  
  //if(needLock) {
    MutexLockerEx ml (&_lock, Mutex::_no_safepoint_check_flag);
    //}
  assert(_head != NULL, "sanity check when removing ");

  Entry* elem = _head;
  Entry* pre = NULL;
  while (elem != NULL){
    if (elem->_key == esc_obj) {
      if (pre == NULL) {
        _head = elem->get_next();
      } else {
        pre->set_next(elem->get_next());
      }
      elem->set_next(NULL);
      
      
      //bool invalid_key = elem->verify();
      //if (invalid_key) {
      //	tty->print_cr("TODO: Bucket::remove_entry this entry is invalid, should be removed completely instead");
      //	elem->print();
      //}
      
      return elem;
    }
    pre = elem;
    elem = elem->_next;
  }

  ShouldNotReachHere();
  return NULL;
}

void Bucket::print() {


  // if(needLock){
    MutexLockerEx ml (&_lock, Mutex::_no_safepoint_check_flag);
    // }
  tty->print (" size =%d ", _size);
  Entry* elem = _head;
  while (elem != NULL){
    elem->print();
    elem = elem->_next;
  }
}

// ===== End of Bucket =====//


// ===== Start of KeyIterator =====//

KeyIterator::KeyIterator(RemSet* set) {
 
  assert (set->size() > 0, "weird to call iterator on set of 0");

  //if(needLock){
    _lock = set->lock();
    _lock->lock();
    //}
  _buckets = set->get_buckets();
  _total_buckets = set->get_num_buckets();
  _current_bucket_index = 0;

  _current_bucket = NULL;
  
  while(_current_bucket_index < _total_buckets) {
    _current_bucket = *(_buckets+_current_bucket_index);
    if (_current_bucket != NULL) break;
    _current_bucket_index++;
  }
  
  if (_current_bucket == NULL)  {
    assert (_current_bucket_index == _total_buckets, "must be");
    assert (set->size() == 0, "must be 0");
  } else {
    _current_entry = _current_bucket->_head;
    
    assert(_current_entry != NULL , "sanity check");
  }
}

bool KeyIterator::has_next() {
  return _current_entry != NULL;
  // return _current_bucket_index < _total_buckets;
}

oop KeyIterator::next() {
//  tty->print_cr("KeyIterator Next start, %p", _current_entry->_key);
  oop result =  _current_entry->_key;

  _current_entry = _current_entry->_next;
  if (!_current_entry) {
    while (++_current_bucket_index < _total_buckets) {
      _current_bucket = *(_buckets+_current_bucket_index);
      if (_current_bucket) break;
    }
    if (_current_bucket_index < _total_buckets) _current_entry = _current_bucket->_head;
    
  }

//  tty->print_cr("KeyIterator Next end");
  return result;
}

KeyIterator::~KeyIterator() {
  //if(needLock) {
    _lock->unlock();
    //}
}

// ===== End of KeyIterator =====//


// ===== Start of LinkedBucket =====//
LinkedBucket::LinkedBucket(region_id_t dest_region_id, Entry* entry) : _dest_region_id(dest_region_id), _head(entry), _next(NULL) {

}


void LinkedBucket::add(Entry* entry) {
  Entry* temp = _head;
  _head = entry;
  _head->set_ordered_next(temp);
}


// ===== End of LinkedBucket =====//


// ===== Start of OrderedIterator =====//

OrderedIterator::OrderedIterator(RemSet* set) : _head_bucket(NULL) {
  
  construct(set);

}

void OrderedIterator::construct(RemSet* set) {
  size_temp_arr = 256;
  // temp array for fast accessing
  // LinkedBucket** map =  NEW_C_HEAP_ARRAY(LinkedBucket*, size_temp_arr, mtThread);
  //addr = map;
  ResourceMark rm;
  LinkedBucket** map =  (LinkedBucket**) malloc(sizeof(LinkedBucket*) << 8);
  // NEW_RESOURCE_ARRAY(LinkedBucket*,size_temp_arr); // 255 is max region id
  addr = map;
  memset(map,0, sizeof(LinkedBucket*) << 8);

  // iterate thru all entries
  EntryIterator* it = set->get_entry_iterator();

  Entry* entry;
  int max_id = 0;
  
  while(it->has_next())  {
    entry = it->next();
    int id = (int)entry->get_dest_region_id();
    
    if (id > max_id) max_id = id;

    assert (id < size_temp_arr, "need larger temp array then "); 
    LinkedBucket* lb = *(map+id);
    if (lb == NULL) {
      lb = new LinkedBucket(id,entry);
      *(map+id) = lb;
    } else {
      lb->add(entry);
    }
  }
  delete it;

  //  tty->print_cr("Max id = %d",max_id);
  // loop thru the map and link all buckets together
  for (int i = max_id; i >= 0; --i) {
    LinkedBucket* lb = *(map+i);
    if (lb != NULL) {
      LinkedBucket* temp = _head_bucket;
      _head_bucket = lb;
      _head_bucket->set_next(temp);
    }
  }
      

  // done
  if (_head_bucket != NULL){
    _current_entry = _head_bucket->get_head();
  }
} 

bool OrderedIterator::has_next() {
  return _head_bucket != NULL;
}

oop OrderedIterator::next() {
  oop result = _current_entry->get_esc_obj();
  
  _current_entry = _current_entry->get_ordered_next();
  
  if (_current_entry == NULL) {
    _head_bucket = _head_bucket->get_next(); // change to next LinkedBucket
    if (_head_bucket != NULL) {
      _current_entry = _head_bucket->get_head();
    }
  }

  return result;
}

Entry* OrderedIterator::next_entry() {
  Entry* result = _current_entry;

  _current_entry = _current_entry->get_ordered_next();

  if (_current_entry == NULL) {
    _head_bucket = _head_bucket->get_next(); // change to next LinkedBucket
    if (_head_bucket != NULL) {
      _current_entry = _head_bucket->get_head();
    }
  }

  return result;
}

OrderedIterator::~OrderedIterator(){
  // TODO to prevent memory leaks.
  free(addr);
  //FREE_C_HEAP_ARRAY(LinkedBucket*, addr, mtThread);
}


// ===== End of OrderedIterator =====//

// ===== Start of EntryIterator =====//

EntryIterator::EntryIterator(RemSet* set) {

  //if(needLock){
    _lock = set->lock();
    _lock->lock();
    //}
  _buckets = set->get_buckets();
  _total_buckets = set->get_num_buckets();
  _current_bucket_index = 0;

  while(true) {
      _current_bucket = *(_buckets+_current_bucket_index);
      if (_current_bucket != NULL && _current_bucket->_head != NULL) 
        break;
      _current_bucket_index++;
      if (_current_bucket_index == _total_buckets)
        break;
  }
  
  if (_current_bucket_index < _total_buckets) {
    assert(_current_bucket != NULL , "sanity check");
    
    _current_entry = _current_bucket->_head;
    
    assert(_current_entry != NULL , "sanity check");
  } else {
    _current_bucket = NULL;
    _current_entry = NULL;
  }
}
 
bool EntryIterator::has_next() {
  return _current_entry != NULL;
}

Entry* EntryIterator::next() {
  Entry* result = _current_entry;
  
  _current_entry = _current_entry->get_next();
  
  if (_current_entry == NULL) {
    while (++_current_bucket_index < _total_buckets) {
      _current_bucket = *(_buckets+_current_bucket_index);
      if (_current_bucket != NULL && _current_bucket->_head != NULL) 
        break;
    }
    if (_current_bucket_index < _total_buckets) 
      _current_entry = _current_bucket->_head;

  }

  //bool invalid_key = result->verify(); 
  //if (invalid_key) {
  // tty->print_cr("EntryIterator::next TODO entry is no longer valid, should remove this");
  //  result->verify();
  //}
  return result;  
}

EntryIterator::~EntryIterator() {
  //if(needLock){
   _lock->unlock();
   //}
}

// ===== End of EntryIterator =====//


// ===== Start of RemSet =====//

RemSet::RemSet(int numBuckets) :  _num_buckets(numBuckets), _size(0), _lock(Mutex::leaf, "Remset lock", true)  {
  
  size_t t =sizeof(Bucket*) * _num_buckets;
  _all_buckets =  (Bucket**)malloc(t); // NEW_C_HEAP_ARRAY(Bucket*,_num_buckets,mtThread);
  memset(_all_buckets,0, t);

}


RemSet::~RemSet()  {
  for (int i = 0; i < _num_buckets; i++) {
    if (*(_all_buckets + i) != NULL) {
      delete(*(_all_buckets+i));//FREE_C_HEAP_OBJ(_map[i], mtThread);
    }
  }
  //delete[](_all_buckets);//
  free(_all_buckets);
  //  FREE_C_HEAP_ARRAY(Bucket*, _all_buckets, mtThread);

}

unsigned hash(oop obj) {
  // copied this from utilities/resourceHash.hpp
  unsigned hash = (unsigned)((uintptr_t)obj);
  return hash ^ (hash >> 3); // just in case we're dealing with aligned ptrs
}

//return the size of the remset: i.e., number of entries
int RemSet::put(oop esc_obj, void* referent_field, oop referent, region_id_t dest_region_id) {
  unsigned index = (hash(esc_obj) & (_num_buckets-1)); 
  assert(index < (unsigned)_num_buckets, "must be");
  if (DebugRemSet) {
    tty->print_cr("put to bucket %d:  esc_obj=%p, field=%p, ref=%p, esc_type: %s, ref_type: %s", index, esc_obj, referent_field, referent, esc_obj->klass()->internal_name(), referent->klass()->internal_name());
  }
//  if (DebugRemSet) tty->print_cr("put to bucket %d:  esc_obj=%p, field=%p, ref=%p", index, esc_obj, referent_field, referent);
  Bucket* b = *(_all_buckets +index);
  if (b == NULL) {
    b = new Bucket();//NEW_C_HEAP_OBJ(Bucket, mtThread);
    *(_all_buckets+index) = b;
  } 

  assert(referent->is_oop(), err_msg("oop is not oop, %p", referent));
  bool new_entry = b->push(esc_obj, referent_field, referent, dest_region_id);

  if (new_entry) _size++;

  if (DebugRemSet) b->print();
  
  return _size;

}

// esc_obj is the new key, entry is the old data, we need to replace the entry->key with esc_obj
int RemSet::put(oop esc_obj, Entry* entry) {

  unsigned index = (hash(esc_obj) & (_num_buckets-1));
  assert (index < (unsigned)_num_buckets, " must be");
  if (DebugRemSet) tty->print_cr("put to bucket %d:  esc_obj=%p, whole entry", index, esc_obj);
  Bucket* b = *(_all_buckets +index);
  if (b == NULL) {
    b = new Bucket();//NEW_C_HEAP_OBJ(Bucket, mtThread);
    *(_all_buckets+index) = b;
  }

  // add entry after replacing the key in entry
  bool new_entry = b->add_and_update_entry(entry, esc_obj);
  
  if (new_entry) _size++;
 
  if (DebugRemSet) b->print();

  return _size;
}

int RemSet::put_thread_safe(oop esc_obj, void* referent_field, oop referent, region_id_t dest_region_id) {
  assert (false, " TO be implemented");
  return 0;
}


Entry* RemSet::remove(oop esc_obj) {

  unsigned index = (hash(esc_obj) & (_num_buckets-1));
  assert (index < (unsigned)_num_buckets, "must be");
  if (DebugRemSet) tty->print_cr("remove from bucket %d:  esc_obj=%p", index, esc_obj);
  Bucket* b = *(_all_buckets +index);
  
  assert (b !=NULL, "sanity check - bucket is NULL when removing");

  Entry* result = b->remove_entry(esc_obj);

  _size--;

  if (DebugRemSet) b->print();

  return result;

} 

Entry* RemSet::get(oop esc_obj) {
  unsigned index = (hash(esc_obj) & (_num_buckets-1));
  assert (index < (unsigned)_num_buckets, " must be ");
  Bucket* b = *(_all_buckets +index);
  if (b == NULL) return NULL;
  
  Entry* result = b->find_entry(esc_obj);
  return result;
}

KeyIterator* RemSet::get_key_iterator() {
  return new KeyIterator(this);
}

OrderedIterator* RemSet::get_ordered_iterator() {
  return new OrderedIterator(this);
}

EntryIterator* RemSet::get_entry_iterator() {
  return new EntryIterator(this);
}

void RemSet::print() {
  for (int i = 0; i < _num_buckets; i++) {
    Bucket* b = *(_all_buckets+i);
    if (b != NULL) {
      tty->print_cr("++++++++++++++++++Bucket %d ++++++++++++++++++++++++++++++++",i);
      b->print();
      tty->print_cr("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }
  }
}
// ===== End of RemSet =====//
