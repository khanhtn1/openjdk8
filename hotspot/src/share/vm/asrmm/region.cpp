/*
 *
 */
#include "remSet.hpp"
#include "region.hpp"
#include "oops/oop.hpp"
#include "runtime/thread.hpp"
#include "asrmm/asrmmHelper.hpp"
#include "memory/gcLocker.hpp"
#include "memory/gcLocker.inline.hpp"


RegionSignature RegionSignature::heap_signature((region_id_t)0,(thread_id_t)0);

RegionSignature::RegionSignature(oop obj) {
  if (!obj->is_in_region_physically()) {
    // header must be 0
    assert (obj->get_region_id() == 0, "region_id must be 0 for oop not physically in region");
    assert (obj->get_thread_id() == 0, "thread_id must be 0 for oop not physically in region");
  
    _region_id = 0;
    _thread_id = 0;
  } else {
     int region = obj->get_region_id();

     assert ((region & ~0xff) ==0, "region is more than 1 byte capacity");

     if (region == 0) {
       _region_id = 0;
       _thread_id = 0;
     } else {
       _region_id = region & 0xff;
       int thread = obj->get_thread_id();
       assert ((thread & ~0xff) ==0, "thread is more than 1 byte capacity");
       _thread_id = thread & 0xff;

       assert (_region_id >=0, "region_id cant be negative");
       assert (_thread_id >=0, "thread_id cant be negative");

     }
   }
 }

long Region::TOTAL_REFERENCES = 0;
long Region::REFERENCES_FROM_HEAP_TO_REGION = 0;
long Region::OTHER_REFERENCES = 0;
long Region::FREE_SPACE = 0;
long Region::DEALLOCATION_TIME = 0;
long Region::OBJECT_MOVE_TIME_IN_GC = 0;
long Region::OBJECTS_COPIED = 0;
volatile long Region::ALLOCATED_BYTES = 0;

 //TODO: remove Thread* and replace by RegionManager*
Region::Region(int region_id,  int thread_id, Region* parent, Thread* thread, bool has_page, bool has_large_page) :
_region_id(region_id), _thread_id(thread_id), _is_retired(false), _parent(parent), _rem_set(NULL), _heap_rem_set(NULL), _another_heap_rem_set(NULL), _has_page_storage(has_page), _has_large_page_storage(has_large_page), _lock_esc_stack(Mutex::leaf, "Esc stack lock", true) {

   assert(thread == Thread::current(), err_msg("Wrong thread passed in for Region, passed in=%p current=%p", thread, Thread::current()) ) ;

   //   _lock_esc_stack(Mutex::leaf, "Esc stack lock", true); // Mutex (int rank, const char *name, bool allow_vm_block=false);

   _manager = thread->region_manager();

   _current_page = create_page();

   _num_holes = -1;

   _num_objects_created = 0;
 }

 Region::~Region() {

   tty->print_cr("Region %p destructor called [%d, %d]",this, _region_id, _thread_id);
 }

Region::Region(const Region & other): _lock_esc_stack(Mutex::leaf, "Esc stack lock", true) {
   ShouldNotReachHere();
 }

 Region* Region::join_two_regions(Region* r1, Region* r2) {
   //find Lowest Common Ancestor of the two DIFFERENT regions
   ShouldNotReachHere();
   /*  tty->print_cr("Calling join operation on r1=%p and r2=%p", r1,r2);
   if (r1 == NULL || r2 == NULL) return NULL;

   assert (r1 != r2 , "Should not call on 2 same region");

   assert (r1->get_thread() != NULL, "r1 is owned by NULL thread");
   assert (r2->get_thread() != NULL, "r2 is owned by NULL thread");

   if (r1->get_thread() != r2->get_thread()) {
     tty->print_cr("Concurrent regions - return NULL");
     return NULL; // r1 and r2 are concurrent regions, join of it is heap which is NULL

   }

   // same thread, now decide which one is higher in the hierarchy (closer to heap)
   if (r1->get_region_id() < r2->get_region_id()) {
     tty->print_cr("r1=%p is higher (%d) than r2=%p (%d)", r1,r1->get_region_id(),r2, r2->get_region_id()); 
     return r1;
   }
   tty->print_cr("r1=%p is lower (%d) than r2=%p (%d)", r1,r1->get_region_id(),r2, r2->get_region_id());
   return r2;
   */
 }

 RegionSignature Region::join_two_regions(RegionSignature left, RegionSignature right) {

   region_id_t l_region = left.get_region_id();
   region_id_t r_region = right.get_region_id();

   if (l_region == 0 || r_region == 0)  {
     // return HEAP
     return RegionSignature::heap_signature;
   }

   thread_id_t l_thread = left.get_thread_id();
   thread_id_t r_thread = right.get_thread_id();


   assert (l_thread >= 0, "l_thread must be non-negative since it's in our region");
   assert (r_thread >= 0, "r_thread must be non-negative since it's in our region");

   if (l_thread != r_thread) { // concurrent regions
     return RegionSignature::heap_signature;
   }

   // now return whichever region is higher in the hierarchy 
   if (l_region < r_region) return left;

   return right;
 }



 RegionSignature Region::join_two_regions(oop left, oop right) {

   RegionSignature sig_left(left);
   RegionSignature sig_right(right);

   return Region::join_two_regions(sig_left, sig_right);
 }


void Region::clear_metadata() {

  _hole_stack.clear(true);

  if(_heap_rem_set!= NULL){
    delete _heap_rem_set;
  }
  _heap_rem_set = NULL;
  
  if(_rem_set != NULL){
    delete _rem_set;
  }
  _rem_set = NULL;

  if (_another_heap_rem_set != NULL ) {
    delete _another_heap_rem_set;
  }
  _another_heap_rem_set = NULL;

}



 /*void Region::initialize(int iter_id,  int thread_id, Region* parent, Thread* thread) {

   _iteration_id = iter_id;
   _thread_id = thread_id;
   _is_retired = false;
   _parent = parent;
   assert(thread == Thread::current(), err_msg("Wrong thread passed in for Region, passed in=%p current=%p", thread, Thread::current()) ) ;
   _thread = thread;

   //_ask_storage_manager = _thread->storage_manager().active();

   _pages = new (ResourceObj::C_HEAP, mtThread) FlexibleArray<Page*>();
   _pages->initialize();

   _current_page = create_page();


   }*/


 RemSet* Region::another_heap_rem_set() {
   if (_another_heap_rem_set == NULL) {
     _another_heap_rem_set = new RemSet();
   }
   return _another_heap_rem_set;
 }

 void Region::swap_heap_rem_sets() {

   // Apr 13 2016: Is this correct??? 
   if (_heap_rem_set != NULL) {
     delete _heap_rem_set;
   }

   _heap_rem_set = _another_heap_rem_set;
   
   
   /*if (_another_heap_rem_set == NULL) {
     tty->print_cr("********************** [%d %d] anotherheapremset %p is NULL", get_region_id(),get_thread_id(),_another_heap_rem_set);
   } else {
     tty->print_cr("********************** [%d %d] anotherheapremset %p is not NULL size =%d",  get_region_id(),get_thread_id(), _another_heap_rem_set, _another_heap_rem_set->size());
   }
   if (_heap_rem_set == NULL)
     tty->print_cr("********************** [%d %d] heapremset %p is NULL", get_region_id(),get_thread_id(),_heap_rem_set);
   else
     tty->print_cr("********************** [%d %d] heapremset %p is not NULL size =%d", get_region_id(),get_thread_id(),_heap_rem_set,_heap_rem_set->size());
   */
   // RemSet* temp = _heap_rem_set;
   //_heap_rem_set = _another_heap_rem_set;
   
   //_another_heap_rem_set = temp;
   
   /*if (temp == NULL)
     tty->print_cr("********************** [%d %d] temp %p is NULL", get_region_id(),get_thread_id(),temp);
   else
     tty->print_cr("********************** [%d %d] temp %p is not NULL size =%d", get_region_id(),get_thread_id(),temp,temp->size());

   if (temp != NULL) temp->clear(); //XXX - delete RemSet?
   */
   
   /*if (_another_heap_rem_set == NULL) {
     tty->print_cr("******************************************** [%d %d] anotherheapremset %p is NULL", get_region_id(),get_thread_id(),_another_heap_rem_set);
   } else {
     tty->print_cr("******************************************** [%d %d] anotherheapremset %p is not NULL size =%d", get_region_id(),get_thread_id(),_another_heap_rem_set, _another_heap_rem_set->size());
   }
   if (_heap_rem_set == NULL) {
     tty->print_cr("******************************************** [%d %d] heapremset %p is NULL", get_region_id(),get_thread_id(),_heap_rem_set);
   } else {
     tty->print_cr("******************************************** [%d %d] heapremset %p is not NULL size =%d",  get_region_id(),get_thread_id(),_heap_rem_set, _heap_rem_set->size());
   }
   if (temp == NULL)
     tty->print_cr("******************************************** [%d %d] temp %p is NULL", get_region_id(),get_thread_id(),temp);
   else
     tty->print_cr("******************************************** [%d %d] temp %p is not NULL size =%d", get_region_id(),get_thread_id(),temp,temp->size());

   */

 }

Stack<oop, mtThread>* Region::esc_stack() {
  // Just to be safe - maybe we can remove this
  MutexLockerEx x(&_lock_esc_stack, Mutex::_no_safepoint_check_flag); 
  return &_esc_stack;
}

void Region::esc_stack_push_thread_safe(oop esc_obj) {
  MutexLockerEx x(&_lock_esc_stack, Mutex::_no_safepoint_check_flag);
  StackIterator<oop,mtThread> si(_esc_stack);
  while (!si.is_empty()){
    oop v = si.next();
    if (v == esc_obj) return;
  }
  
  _esc_stack.push(esc_obj);
  
}

bool Region::mark_and_push_hole(oop obj) {
  //  Don't double mark 
  assert (obj != NULL, "sanity");

  size_t size = obj->size();
  if (size < (size_t)LargeObjectThreshold) {
    Page* page = Page::get_page(obj);
    assert ((void*)obj != (void*)page, err_msg("sanity check obj=%p size=%d page=%p", obj, size, page));
    //page->mark_hole(obj, obj->size());
    /*insert holes into a summary represented by a list*/
    if(UseYakHoleSummary){
      HoleSummary* summary = page->get_hole_summary();
      summary->insert((HeapWord*)obj, size);
    }
    page->set_has_holes(true);
  } else {
    PRINT_YAK_DEBUG(true, "Warning: Large Object %p being marked as hole region [%d,%d] - klass %s ", obj, _region_id, _thread_id,  obj->klass()->internal_name());
    // Apr 19 Missing handling logic for large obj as hole 
    // assert (false, "violated assumption, no large object should be hole");
  }
  
    // Apr 1 For performance: dont push typeArray into _holes_stack because you wont follow content
    // use the following 2 statements:: is it safe to do so???

    // Klass* k = obj->klass();
    // if (!k->oop_is_typeArray())

  _hole_stack.push(obj);

  PRINT_YAK_DEBUG(DebugYakRetire,
                  "Mark obj as a hole no. %d in region[%d][%d] %p, %s", _hole_stack.size(), _region_id, _thread_id,
      obj, obj->klass()->internal_name());

  return true;
}


Page* Region::create_page() {
  Page* p = NULL;
  
  if (_has_page_storage) { // storageManager has stuff to offer
    p = _manager->give_page();
    //    if (p != NULL) tty->print_cr("giving page %p used=%d/%d free=%d", p, p->dead_size(),PageSize,p->free_size());
    // p can be NULL if storage Manager has nothing left to give
  }
 
  if (p == NULL) {//there is no page in storage
    _has_page_storage = false;
    p = Page::brand_new_page();//malloc memory for new page
    assert (p != NULL, " brand_new_page() is not allowed to give me NULL");
  }

  _pages.push(p);//add this new page to the array of pages for this region

  return p;
}

HeapWord* Region::create_large_page(size_t size) {
  
  HeapWord* p = NULL;
  
  if (_has_large_page_storage) {
    p = _manager->give_large_page(size);
  }
  
  if (p == NULL) {
    if (!_manager->has_large_pages()) _has_large_page_storage = false;
    p = Universe::heap()->allocate_new_page(size);
    assert (p != NULL, "heap cannot give me NULL unless we are OOM for real");
  }
  
  _large_objects_stack.push((oop)p);  
  return p;

}



void Region::retire() {
  assert(!UseCompressedOops, "Compressed Oop should be disabled!");
  if(DebugYak){
    tty->print_cr("Reclaiming region %p [%d,%d]",this,_region_id,_thread_id);
  }
  
  if(TimeRegionDeallocation){
    _timer.start();
  }

  // Walk through the rem set:
  //   1) Promote the object to another region.
  //   2) Mark the heap object as holes.
  //   3) Update (heap) rem set.
  //   4) Clear the rem set.
  //
  // XXX: large objects in regions : Apr 13 working on

  if (_rem_set != NULL) {
    OrderedIterator* it = _rem_set->get_ordered_iterator();
    while (it->has_next()) {
      Entry* entry = it->next_entry();
      // is_valid is NOT MT-safe.
      // TODO: Will this cause problem?
      if (!entry->is_valid()) {
        continue;
      }
      
      oop obj = entry->get_esc_obj();
      assert(!oopDesc::is_null(obj), "In remset, no pointee should be NULL!");

      region_id_t dest_region_id = entry->get_dest_region_id();

      if((size_t)(obj->size()) >= (size_t)LargeObjectThreshold) {
        PRINT_YAK_DEBUG(true, "Warning: Large Object %p being processed in remset region [%d,%d] dest_region=%d - klass %s ",
            obj, _region_id, _thread_id, dest_region_id,  obj->klass()->internal_name());
      }

      if (DebugRemSetRetire) {
        tty->print_cr("oop from remset =%p - dest_region_d=%d",obj,dest_region_id);
        //entry->print();
      }

      // 1) obj is a heap obj, REVISIT.
      //   1.1) if src_obj is a heap object, (do nothing)
      //   1.2) if src_obj is a region object (update the heap remset for current region).
      // 2) obj is a region obj,
      //   2.1) obj is forwarded, REVISIT (update the references).
      //   2.2) obj is not forwarded, FIRST-VISIT
      //     2.2.1) dest_region_id is heap, (set the region id)
      //       2.2.1.1) the src_obj is heap object, (do nothing).
      //       2.2.1.2) the src_obj is region object, (process all reachable objects from obj,
      //                                     update the heap remset).
      //     2.2.2) dest_region_id is another region, (process all reachable objects from obj,
      //                                     update the references).
      // Key: UPWARD reference needs not be remembered.

      // 1 obj is a heap obj.
      StackIterator<PairRS, mtThread> ref_it(*(entry->get_references()));
      if (obj->get_region_id() == 0) {
        while (!ref_it.is_empty()) {
          PairRS pair = ref_it.next();
          oop src_obj = pair._obj;

          // 1.2
          if (!ASRMMHelper::is_logic_in_heap(src_obj)) {
            // Verify if the reference is still valid or not.
            // TODO: delete the reference.
            oop pointee = oopDesc::load_heap_oop((oop*)(pair._field_addr));
            assert (pointee == obj, "it should be equal - case 1");
            if (pointee != obj) {
              continue;
            }
            another_heap_rem_set()->put(obj, pair._field_addr, src_obj, 0);
          }
        }
      } 

      // 2.1 obj is forwarded (revisit).
      else if (obj->is_forwarded()) {
        oop new_obj = obj->forwardee();
        assert (new_obj->is_oop(), err_msg("obj=%p is forwarded to forwardee=%p not oop",obj,new_obj));

        while (!ref_it.is_empty()) {
          PairRS pair = ref_it.next();
          // Verify if the reference is still valid or not.
          // TODO: delete the reference.
          oop pointee = oopDesc::load_heap_oop((oop*)(pair._field_addr));
          assert (pointee == obj, "it should be equal - case 2.1");
          if (pointee != obj) {
            continue;
          }
          assert(Universe::heap()->is_in_reserved(pair._field_addr), //&&
                 //    (!Universe::heap()->is_in_region(pair._field_addr) || pair._obj->get_region_id() == 0),
                 err_msg("try to write something outside the heap: %p in_reserved=%d in_region=%d, pair_obj_region_id=%d", pair._field_addr, Universe::heap()->is_in_reserved(pair._field_addr),
                         Universe::heap()->is_in_region(pair._field_addr), pair._obj->get_region_id()));
          oopDesc::store_heap_oop((oop*)pair._field_addr, new_obj);
        }
      }

      // 2.2 obj is not forwarded (first visit).
      else {
        // 2.2.1 dest_region_id is heap.
        if (dest_region_id == 0) {
//          obj->set_region_id(0); // Looks like no need. ??

          ASRMMHelper::process_all_reachable_obj_in_region(obj,
              _region_id, 0, _thread_id);
          while (!ref_it.is_empty()) {
            PairRS pair = ref_it.next();
            // Verify if the reference is still valid or not.
            // TODO: delete the reference.
            oop src_obj = pair._obj;
            oop pointee = oopDesc::load_heap_oop((oop*)(pair._field_addr));
            //assert (pointee == obj, "it should be equal - case 2.2.1");
            if (pointee != obj) {
              continue;
            }
            // 2.2.1.2
            if (!ASRMMHelper::is_logic_in_heap(src_obj)) {
              another_heap_rem_set()->put(obj, pair._field_addr, src_obj, 0);
            }
          }
        }
        // 2.2.2 dest_region_id is another region.
        else {
          oop new_obj = ASRMMHelper::process_all_reachable_obj_in_region(obj,
              _region_id, dest_region_id, _thread_id);
          assert(new_obj->is_oop(), err_msg("moved obj=%p to new region but new_obj=%p is not oop", obj, new_obj));
          assert(new_obj != obj && obj->is_forwarded(), err_msg("obj %p should be moved to a parent region!", obj));
          while (!ref_it.is_empty()) {
            PairRS pair = ref_it.next();
            // Verify if the reference is still valid or not.
            // TODO: delete the reference.
            //oop src_obj = pair._obj;
            oop pointee = oopDesc::load_heap_oop((oop*)(pair._field_addr));
            assert (pointee == obj, "it should be equal - case 2.2.2");
            if (pointee != obj) {
              continue;
            }
            assert(Universe::heap()->is_in_reserved(pair._field_addr),
                   // &&
                   //(!Universe::heap()->is_in_region(pair._field_addr) || pair._obj->get_region_id() == 0),
                   err_msg("try to write something outside the heap: %p in_reserved=%d in_region=%d, pair_obj_region_id=%d", pair._field_addr, Universe::heap()->is_in_reserved(pair._field_addr), 
                           Universe::heap()->is_in_region(pair._field_addr), pair._obj->get_region_id()));
            oopDesc::store_heap_oop((oop*)pair._field_addr, new_obj);
          }
        }
      }
    } // while end for all the rem set.
    delete it;
    // FIXME: We do not reset the rem set! UGLY!!!
  } // if(_rem_set != NULL)

  while (!_esc_stack.is_empty()) {
    ShouldNotReachHere(); // Apr 13 read barrier is no longer needed
    oop obj = _esc_stack.pop();
    if (obj->get_region_id() != 0) {
      ASRMMHelper::process_all_reachable_obj_in_region(obj,
          _region_id, 0, _thread_id);
    }
  }


  // XXX: Swap two heap remsets, so this is safe for GC interrupts!! 
  swap_heap_rem_sets();


  // 2 -- then give all pages to storage manager  
  
  //tty->print_cr("giving back %d pages for current region %p[%d,%d]", _pages.size(),this,_region_id,_thread_id);
  RegionManager* region_manager = _manager;

  while (!_pages.is_empty()) {
    Page* page = _pages.pop();
    page->reset(); // reset top 
    //tty->print_cr("storing page %p used=%d/%d free=%d", page, page->dead_size(),PageSize,page->free_size());
    region_manager->store_page(page);
    // storage_manager will handle classification of pages
  }

  // 3 -- large objects
  while (!_large_objects_stack.is_empty()) {
    oop large_obj = _large_objects_stack.pop();
    bool large_hole = false;
    if (_rem_set != NULL) {
      // Apr 19: no handling of Large Hole
      //assert (_rem_set->get(large_obj) == NULL, "must be null");
      if (_rem_set->get(large_obj) != NULL)
        large_hole = true;
    }
    size_t page_size = align_size_up(large_obj->size(),(size_t)PageSize);
    region_manager->store_large_page((HeapWord*)large_obj, page_size, large_hole);
  }


  _num_holes = _hole_stack.size();

  // 4 -- Finally
  _is_retired = true;
  _pages.clear(true); // clean the space used for _pages
  _large_objects_stack.clear(true);

  //clean up
  if (_rem_set != NULL) delete _rem_set;
  _rem_set = NULL;

  if(TimeRegionDeallocation){
    _timer.stop();
  }
  
}

void Region:: set_is_retired(){

  while (!_pages.is_empty()) {
    Page* page = _pages.pop();
    page->reset(); // reset top
    _manager->store_page(page);
    // storage_manager will handle classification of pages
  }

  // 3 -- large objects
  while (!_large_objects_stack.is_empty()) {
    oop large_obj = _large_objects_stack.pop();
    bool large_hole = false;
    if (_rem_set != NULL) {
      // Apr 19: no handling of Large Hole
      //assert (_rem_set->get(large_obj) == NULL, "must be null");
      if (_rem_set->get(large_obj) != NULL)
        large_hole = true;
    }
    size_t page_size = align_size_up(large_obj->size(),(size_t)PageSize);
    _manager->store_large_page((HeapWord*)large_obj, page_size, large_hole);
  }
  _is_retired = true;
}



Region* Region::joins(Region* r)  {
  ShouldNotReachHere();
  /*
  if (r == NULL) return NULL;
  
  assert (get_thread() != NULL, "this is owned by NULL thread");
  assert (r->get_thread() != NULL, "r is owned by NULL thread");

  if (get_thread() != r->get_thread()) return NULL; // r1 and r2 are concurrent regions, join of it is heap which is NULL

  // same thread, now decide which one is higher in the hierarchy (closer to heap)
  if (get_region_id() < r->get_region_id()) return this;

  return r;
  */
}



