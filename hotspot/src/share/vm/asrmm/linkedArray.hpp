/*
 * LinkedArray is an array with each element is a fixed-size array of size _capacity
 * Each element then can link to another element
 *
 * Not thread-safe
 */


#ifndef SHARE_VM_ASRMM_LINKEDARRAY_HPP
#define SHARE_VM_ASRMM_LINKEDARRAY_HPP

#include "utilities/debug.hpp"
#include "utilities/globalDefinitions.hpp"
#include "memory/allocation.hpp"
#include "memory/allocation.inline.hpp"



template<class E> class ArrayElement : public CHeapObj<mtThread> {
 
private:
  int    _len;			// current length
  int    _capacity;		// maximum length
  E*     _data;			// data array

  ArrayElement<E>* _next;
  ArrayElement<E>* _prev;

public:
  ArrayElement(int capacity) : _len(0), _capacity(capacity), _prev(NULL), _next(NULL) {
    _data = (E*)malloc(_capacity * sizeof(E));//NEW_C_HEAP_ARRAY(E, _capacity, mtThread);
    for (int i =0; i < _capacity; i++) ::new ((void*)&_data[i]) E();
  }
  
  ~ArrayElement() {
    for (int i =0; i < _capacity; i++){
      if(_data[i]!=NULL) delete _data[i];
    }
    free(_data);
  }

  void set_next(ArrayElement* next)		{  _next = next; }

  ArrayElement* get_next()			{  return _next;   }

  void set_prev(ArrayElement* prev)		{  _prev = prev;    }

  ArrayElement* get_prev()			{  return _prev;    }

  
  bool valid_to_pop()				{  return (_len >0 && _len <=_capacity);   }

  bool push(const E& e) {
    if (_len >= _capacity) return false;
    *(_data + _len) = e;
    _len++;
    return true;
  }

  E pop() {
    --_len;
    return  *(_data + _len);
  }

};

template<class E> class LinkedArray : public CHeapObj<mtThread>  {


private:
  int _ecap;

  ArrayElement<E>*	_current_elem;		// current element 


public:
  
  LinkedArray(int element_capacity) : _ecap(element_capacity) {
    
    _current_elem = new ArrayElement<E>(element_capacity);
    
    _current_elem->set_next(NULL);
    _current_elem->set_prev(NULL);

  }

  ~LinkedArray();

  void push(const E& e) {
    
    bool b = _current_elem->push(e);
    if (!b) {
      ArrayElement<E>* elem = new ArrayElement<E>(_ecap);
      _current_elem->set_next(elem);
      elem->set_prev(_current_elem);
      _current_elem = elem;
    }

    b = _current_elem->push(e);

    assert (b, "Cant fail: coulnt push");

  }

  E pop() {
    
    assert (_current_elem != NULL, "Sanity check");

    if (_current_elem->valid_to_pop()) {
      return _current_elem->pop();
    }
    
    _current_elem = _current_elem->get_prev();
    assert (_current_elem != NULL, "Sanity check");

    assert (_current_elem->valid_to_pop(), "cant be invalid");
    return _current_elem->pop();
  }
};


#endif
