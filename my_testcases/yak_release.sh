#!/bin/bash

 $YAK_RELEASE/java -Xmx4g -Xregion -XX:-UseCompressedClassPointers -XX:-UseCompressedOops \
  -XX:-DebugPageAllocation -XX:-AllocReferenceObjectInRegion \
  -XX:-AllocInstanceMirrorObjectInRegion -XX:-DebugPSScavenge \
  -XX:+DebugYak \
  -XX:-DebugYakMinorGC \
  -XX:-DebugYakFullGC \
  -XX:-DebugYakRetire \
     
  $1 $2 $3 $4 $5
  #-XX:+TraceScavenge \
  #-XX:ParallelGCThreads=12 \
  #-XX:+DebugPSPromotionManager \
#  -XX:CICompilerCount=1 \
  #-XX:CICompilerCount=1 \
  #-Xint \
