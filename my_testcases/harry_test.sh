#!/bin/bash

#gdb --args
 $MY_JAVA/java -Xmx4g -Xregion -XX:-UseCompressedClassPointers -XX:-UseCompressedOops \
  -XX:-DebugPageAllocation -XX:-AllocReferenceObjectInRegion \
  -XX:-AllocInstanceMirrorObjectInRegion -XX:-DebugPSScavenge \
  -XX:-DebugYak \
  -XX:+DebugLargeObjectAllocationInRegion \
  -XX:+DebugYakMinorGC \
  -XX:-DebugYakFullGC \
  -XX:+DebugYakRetire \
  -XX:CICompilerCount=1 \
  -XX:+DebugStorageManager \
  -XX:-TraceSafepoint \
  -XX:-CICompileOSR \
  -XX:SuppressErrorAt=/runtime.cpp:139 \
  -XX:SuppressErrorAt=/javaClasses.cpp:1598 \
  -XX:SuppressErrorAt=/runtime.cpp:1167 \
  -XX:SuppressErrorAt=/c1_Runtime1.cpp:161 \
  -XX:SuppressErrorAt=/c1_LIRGenerator.cpp:1466 \
  -XX:-Verbose \
  $1 $2 $3 $4 $5 $6
  #-XX:+TraceScavenge \
  #-XX:ParallelGCThreads=12 \
  #-XX:+DebugPSPromotionManager \
#  -XX:CICompilerCount=1 \
  #-XX:CICompilerCount=1 \
  #-Xint \
