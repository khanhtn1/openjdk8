#!/bin/bash

gdb --args $MY_JAVA/java -Xmx3g -Xregion -XX:-UseCompressedClassPointers -XX:-UseCompressedOops -XX:-CICompileOSR \
  -XX:-DebugPageAllocation -XX:-AllocReferenceObjectInRegion \
  -XX:-AllocInstanceMirrorObjectInRegion -XX:-DebugPSScavenge \
  -XX:ParallelGCThreads=1 \
  -XX:+DebugYakMinorGC \
  -XX:+DebugYakFullGC \
  -XX:-TraceSafepoint \
  -XX:-Verbose \
  -Xint \
  $1 $2 $3
  #-XX:+TraceScavenge \
  #-XX:+DebugPSPromotionManager \
  #-XX:CICompilerCount=1 \
  #-XX:-DebugYak \
