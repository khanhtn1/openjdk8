#!/bin/bash

if [ $# == 0 ];
then
    echo 'make clean CONF=linux-x86_64-normal-server-fastdebug'
    make clean CONF=linux-x86_64-normal-server-fastdebug

    echo 'make all CONF=linux-x86_64-normal-server-fastdebug'
    make all CONF=linux-x86_64-normal-server-fastdebug
    
    echo 'make clean CONF=linux-x86_64-normal-server-release'
    make clean CONF=linux-x86_64-normal-server-release

    echo 'make all CONF=linux-x86_64-normal-server-release'
    make all CONF=linux-x86_64-normal-server-release
else
    if [ $1 == "1" ];
    then
        echo 'make clean CONF=linux-x86_64-normal-server-fastdebug'
        make clean CONF=linux-x86_64-normal-server-fastdebug
    
	echo 'make all CONF=linux-x86_64-normal-server-fastdebug'
	make all CONF=linux-x86_64-normal-server-fastdebug
    else 
	if [ $1 == "2" ];
	then
            echo 'make clean CONF=linux-x86_64-normal-server-release'
            make clean CONF=linux-x86_64-normal-server-release

	    echo 'make all CONF=linux-x86_64-normal-server-release'
	    make all CONF=linux-x86_64-normal-server-release
	else 
	    if [ $1 == "3" ];
	    then
                echo 'make clean CONF=linux-x86_64-normal-server-slowdebug'
                make clean CONF=linux-x86_64-normal-server-slowdebug

		echo 'make all CONF=linux-x86_64-normal-server-slowdebug'
		make all CONF=linux-x86_64-normal-server-slowdebug
	    fi
    
	fi
    
    fi
fi

#echo 'make all CONF=linux-x86_64-normal-server-fastdebug'
#make all CONF=linux-x86_64-normal-server-fastdebug

#echo 'make all CONF=linux-x86_64-normal-server-release'
#make all CONF=linux-x86_64-normal-server-release
